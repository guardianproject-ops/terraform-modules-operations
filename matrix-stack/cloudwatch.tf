module "label_alarms" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["alarms"]
  context    = module.this.context
}

resource "aws_sns_topic" "alarms_pagerduty" {
  name  = module.label_alarms.id
  count = var.pagerduty_enabled ? 1 : 0
}

locals {
  sns_topics_alarms = {
    "pagerduty" : {
      "topic_arn" : var.pagerduty_enabled ? aws_sns_topic.alarms_pagerduty[0].arn : null
      "topic_name" : var.pagerduty_enabled ? module.label_alarms.id : null
    }
  }
  sns_topic_arns_alarms = [for k, topic in local.sns_topics_alarms : topic.topic_arn if topic.topic_arn != null]
}

module "pagerduty" {
  source                           = "../pagerduty-service"
  enabled                          = var.pagerduty_enabled
  pagerduty_service_name           = var.pagerduty_service_name
  pagerduty_escalation_policy_name = var.pagerduty_escalation_policy_name
  suppress_alerts                  = var.pagerduty_suppress_alerts
  cloudwatch_sns_topics            = local.sns_topics_alarms
  context                          = module.this.context
}

data "aws_iam_policy_document" "grafana_cloudwatch_logs_policy" {
  # ref: https://grafana.com/docs/grafana/latest/datasources/aws-cloudwatch/
  statement {
    sid    = "AllowReadingMetricsFromCloudWatch"
    effect = "Allow"
    actions = [
      "cloudwatch:DescribeAlarmsForMetric",
      "cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms",
      "cloudwatch:ListMetrics",
      "cloudwatch:GetMetricData",
      "cloudwatch:GetInsightRuleReport",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowReadingLogsFromCloudWatch"
    effect = "Allow"
    actions = [
      "logs:DescribeLogGroups",
      "logs:GetLogGroupFields",
      "logs:StartQuery",
      "logs:StopQuery",
      "logs:GetQueryResults",
      "logs:GetLogEvents",
    ]
    resources = ["*"]
  }

  statement {
    sid = "UseKMSKey"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = [
      aws_kms_key.rds.arn
    ]
  }

  statement {
    sid    = "AllowReadingTagsInstancesRegionsFromEC2"
    effect = "Allow"
    actions = [
      "ec2:DescribeTags",
      "ec2:DescribeInstances",
      "ec2:DescribeRegions",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowReadingResourcesForTags"
    effect = "Allow"
    actions = [
      "tag:GetResources",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "cross_account_cloudwatch_logs_policy" {
  name        = "${module.this.id}-CrossAccountCloudWatchLogsPolicy"
  description = "Policy to allow access to CloudWatch logs from the operations account"
  policy      = data.aws_iam_policy_document.grafana_cloudwatch_logs_policy.json
}

data "aws_iam_policy_document" "cross_account_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.operations_account_id}:root"]
    }
    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"
      values   = [random_password.cloudwatch_assume_role_external_id.result]
    }
  }
}

resource "random_password" "cloudwatch_assume_role_external_id" {
  length  = 32
  special = false
}

resource "aws_iam_role" "cross_account_role_cloudwatch_logs" {
  name               = "${module.this.id}-CrossAccountCloudWatchLogsRole"
  assume_role_policy = data.aws_iam_policy_document.cross_account_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "attach_cloudwatch_logs_policy" {
  role       = aws_iam_role.cross_account_role_cloudwatch_logs.name
  policy_arn = aws_iam_policy.cross_account_cloudwatch_logs_policy.arn
}

resource "aws_ssm_parameter" "gpops_role_arn" {
  name = "/cloudwatch-cross-account-roles/${module.this.id}/role-arn"
  type = "String"
  value = jsonencode({
    "role_arn" : aws_iam_role.cross_account_role_cloudwatch_logs.arn,
    "deployment_id" : module.this.id,
    "external_id" : random_password.cloudwatch_assume_role_external_id.result
    "default_region" : data.aws_region.current.name
  })
  provider = aws.gp_operations
}
