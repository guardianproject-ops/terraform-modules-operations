module "label_ec2" {
  source  = "cloudposse/label/null"
  version = "0.25.0"
  context = module.this.context
}

module "label_ec2_main" {
  source  = "cloudposse/label/null"
  version = "0.25.0"
  name    = "main"
  context = module.label_ec2.context
}

module "main_instance_security_group" {
  source           = "cloudposse/security-group/aws"
  version          = "2.2.0"
  count            = module.label_ec2_main.enabled ? 1 : 0
  vpc_id           = local.vpc_id
  attributes       = ["main"]
  allow_all_egress = true
  rules = [
    {
      key         = "MATRIX_CLIENT"
      type        = "ingress",
      from_port   = "80",
      to_port     = "80",
      protocol    = "tcp",
      cidr_blocks = local.public_subnet_cidrs
    },
  ]
  context = module.label_ec2_main.context
}

module "label_log_group_main" {
  source    = "cloudposse/label/null"
  version   = "0.25.0"
  delimiter = "/"
  name      = "main"
  context   = module.this.context
}

resource "aws_cloudwatch_log_group" "main" {
  count             = module.label_ec2_main.enabled ? 1 : 0
  name              = "/${module.label_log_group_main.id}"
  retention_in_days = var.log_group_retention_in_days
  tags              = module.this.tags
}

module "main_instance" {
  source                        = "./ec2"
  context                       = module.label_ec2_main.context
  count                         = module.label_ec2_main.enabled ? 1 : 0
  kms_key_id                    = var.kms_key_arn
  attach_bucket                 = true
  bucket_name                   = local.synapse_media_bucket_name
  bucket_kms_key_arn            = aws_kms_key.media_bucket.arn
  ec2_instance_type             = var.main_instance_type
  ebs_volume_disk_allocation_gb = var.main_ebs_volume_disk_allocation_gb
  ec2_disk_allocation_gb        = var.main_ec2_disk_allocation_gb
  ec2_root_disk_volume_type     = var.ec2_root_disk_volume_type
  subnet_id                     = local.ec2_private_subnet_main_id
  security_group_ids            = [module.main_instance_security_group[0].id]
  deletion_protection_enabled   = local.deletion_protection_enabled
  log_groups_root               = "/${module.label_log_group_main.id}"
}

