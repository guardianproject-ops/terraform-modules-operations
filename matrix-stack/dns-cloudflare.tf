resource "cloudflare_record" "matrix" {
  count   = module.this.enabled ? 1 : 0
  zone_id = var.cloudflare_zone_id
  name    = var.matrix_server_domain
  content = module.alb[0].alb_dns_name
  type    = "CNAME"
  ttl     = 1
  proxied = true
  comment = "Created by Terraform as a part of ${module.this.id}"
}

resource "cloudflare_record" "legacy_push" {
  count   = module.this.enabled && var.matrix_push_domain != null ? 1 : 0
  zone_id = var.cloudflare_zone_id
  name    = var.matrix_push_domain
  content = module.alb[0].alb_dns_name
  type    = "CNAME"
  ttl     = 1
  proxied = false
  comment = "Created by Terraform as a part of ${module.this.id}"
}
