variable "kms_key_arn" {
  type        = string
  description = "The kms key ARN used for various purposes throughout the deployment"
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR of the VPC"
}

variable "ec2_subnets_cidr" {
  type        = string
  description = "The CIDR of the subnets in the VPC, will be subdivided into public and private segments."
}

variable "rds_subnets_cidr" {
  type        = string
  description = "The CIDR of the subnets in the VPC that is for RDS instances"
}


variable "s3_media_force_destroy" {
  description = "(Optional, Default:false ) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  type        = bool
  default     = false
}

variable "s3_media_expiration_enabled" {
  type        = bool
  description = "Whether objects should be expunged automatically, see expiration_days"
  default     = true
}

variable "s3_media_expiration_days" {
  description = "Number of days after which to expunge the objects."
  default     = 90
  type        = number
}

variable "matrix_server_domain" { type = string }
variable "matrix_push_domain" {
  type        = string
  default     = null
  description = "Legacy option. Do not use on new deployments."
}

variable "cloudflare_edit_token" { type = string }
variable "cloudflare_zone_id" { type = string }


variable "use_external_rds" {
  type        = bool
  default     = false
  description = <<EOT
When true this module will not create or manage an RDS instance, when false it will create an RDS instance and export the vars for it.
If using an external RDS the VPC will nevertheless be created with a RDS subnet, but you can ignore it.
EOT
}
variable "rds_admin_password" {
  type    = string
  default = null
}
variable "rds_db_identifier" {
  type    = string
  default = null
}
variable "rds_allocated_storage_gb" { type = number }
variable "rds_max_allocated_storage_gb" { type = number }
variable "rds_instance_class" { type = string }
variable "rds_admin_user" {
  type    = string
  default = "root"
}
variable "rds_admin_db_name" {
  type    = string
  default = "root"
}

variable "main_ec2_disk_allocation_gb" {
  default = null
  type    = number
}

variable "main_ebs_volume_disk_allocation_gb" {
  default = null
  type    = number
}

variable "main_instance_type" {
  default = null
  type    = string
}

variable "log_group_retention_in_days" {
  default     = 30
  type        = number
  description = <<EOT
The number in days that cloudwatch logs will be retained.
EOT
}

variable "tailscale_client_id" {
  type        = string
  description = <<EOT
The Tailscale client ID, used to create a pre-auth key for the created instances.
EOT
}
variable "tailscale_client_secret" {
  type        = string
  description = <<EOT
The Tailscale client secret, used to create a pre-auth key for the created instances.
EOT
}

variable "tailscale_tailnet" {
  type        = string
  description = <<EOT
The Tailscale tailnet to create the key in.
EOT
}

variable "tailscale_main_tags" {
  type        = list(string)
  description = <<EOT
The tailscale acl tags (must start with 'tag:') to apply to the created instance.
EOT
}

variable "pagerduty_enabled" {
  type    = bool
  default = true
}

variable "pagerduty_escalation_policy_name" {
  description = "The name of the pager duty escalation policy to use for this service"
  type        = string
}

variable "pagerduty_service_name" {
  description = "The name of the service. Recomended is the domain name of the service. e.g., neo.keanu.im"
  type        = string
}

variable "pagerduty_suppress_alerts" {
  description = "Controls whether all alerts are suppressed or not (does not create an incident nor page anyone)"
  type        = bool
  default     = false
}

variable "alb_alarms_evaluation_periods" {
  type        = number
  description = "Number of periods to evaluate for the alarm"
  default     = 1
}

variable "alb_alarms_period" {
  type        = number
  description = "Duration in seconds to evaluate for the alarm"
  default     = 300
}

variable "alb_alarms_target_3xx_count_threshold" {
  type        = number
  description = "The maximum count of 3XX requests over a period. A negative value will disable the alert"
  default     = -1
}

variable "alb_alarms_target_4xx_count_threshold" {
  type        = number
  description = "The maximum count of 4XX requests over a period. A negative value will disable the alert"
  default     = -1
}

variable "alb_alarms_target_5xx_count_threshold" {
  type        = number
  description = "The maximum count of 5XX requests over a period. A negative value will disable the alert"
  default     = 150
}

variable "alb_alarms_elb_5xx_count_threshold" {
  type        = number
  description = "The maximum count of ELB 5XX requests over a period. A negative value will disable the alert"
  default     = 100
}

variable "alb_alarms_target_response_time_threshold" {
  type        = number
  description = "The maximum average target response time (in seconds) over a period. A negative value will disable the alert"
  default     = 10
}


/*
variable "sts_external_secret_operations_account" {
  value       = string
  description = "A shared secret that must be provided by the operations account when assuming roles to access resources in this deployment"
}
*/


variable "grafana_url" {
  type        = string
  description = "The root URL of a Grafana server in which to create alerts."
}

variable "grafana_auth" {
  type        = string
  description = "API token, basic auth in the `username:password` format"
}


variable "grafana_cloudflare_service_token_client_secret" {
  type        = string
  description = "The cloudflare access service token client secret to use to access grafana"
}


variable "grafana_cloudflare_service_token_client_id" {
  type        = string
  description = "The cloudflare access service token client id to use to access grafana"
}


variable "rds_snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05"
  type        = string
  default     = null
}


variable "rds_allow_major_version_upgrade" {
  type    = bool
  default = false
}
variable "rds_engine_version" {
  type = string
}
variable "rds_family" {
  type = string
}

variable "rds_major_engine_version" {
  type = string
}

variable "rds_deletion_protection_enabled" {
  type    = bool
  default = true
}


variable "rds_iops" {
  default = null
  type    = number

}
variable "rds_storage_throughput" {
  default = null
  type    = number
}

variable "rds_db_subnet_group_name" {
  default = null
  type    = string
}

variable "rds_db_subnet_group_use_name_prefix" {
  type    = bool
  default = true
}

variable "rds_create_db_subnet_group" {
  type    = bool
  default = true
}

variable "backup_storage_retention_days" {
  default = 30
  type    = number
}

variable "ec2_root_disk_volume_type" {
  type    = string
  default = "gp3"
}

variable "enable_mirrors" {
  description = "If true, will deploy cloudfront distributions to mirror the matrix server"
  type        = bool
  default     = false
}

variable "mirrors" {
  description = "A simple map of names representing the mirrors, the names themselves are not important, but they must be unique for mirror rotation"
  type = map(object({
    name = string
  }))
  default = null
}
