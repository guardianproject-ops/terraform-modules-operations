module "label_certs" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["certs"]
  context    = module.this.context
}

module "acm_certs" {
  count = local.enabled ? 1 : 0
  #source                = "git::https://gitlab.com/guardianproject-ops/terraform-modules-operations//acm-cloudflare-certs?ref=master"
  source                = "../acm-cloudflare-certs"
  context               = module.label_certs.context
  cloudflare_edit_token = var.cloudflare_edit_token
  cloudflare_zone_id    = var.cloudflare_zone_id

  certs = {
    "${var.matrix_server_domain}" = {
      common_name = var.matrix_server_domain
      dns_names   = var.matrix_push_domain == null ? [var.matrix_server_domain] : [var.matrix_server_domain, var.matrix_push_domain]
    }
  }

  providers = {
    aws.us_east_1 = aws.us_east_1
    aws.main      = aws
  }
}
