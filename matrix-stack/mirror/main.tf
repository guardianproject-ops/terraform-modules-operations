locals {
  matrix_origin_id = "matrix-${var.origin_domain}"
}

resource "aws_cloudfront_distribution" "this" {
  comment         = "CDN/Mirror for ${var.origin_domain}"
  enabled         = true
  is_ipv6_enabled = true

  lifecycle {
    prevent_destroy = true
  }

  origin {
    domain_name         = var.origin_domain
    origin_id           = local.matrix_origin_id
    connection_attempts = 3
    connection_timeout  = 10
    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 60
      origin_protocol_policy   = "https-only"
      origin_ssl_protocols     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  default_cache_behavior {
    allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods           = ["GET", "HEAD"]
    target_origin_id         = local.matrix_origin_id
    viewer_protocol_policy   = "redirect-to-https"
    min_ttl                  = 0
    default_ttl              = 0                                      # 3600
    max_ttl                  = 0                                      # 86400
    cache_policy_id          = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad" # CachingDisabled
    origin_request_policy_id = "b689b0a8-53d0-40ab-baf2-68738e2966ac" # AllViewerExceptHostHeader
    compress                 = true
  }

  dynamic "custom_error_response" {
    for_each = [
      400,
      403,
      405,
      414,
      416
    ]
    content {
      error_caching_min_ttl = 10
      error_code            = custom_error_response.value
    }
  }

  price_class = "PriceClass_100"
  viewer_certificate {
    cloudfront_default_certificate = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = module.this.tags
}
