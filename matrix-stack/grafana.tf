resource "grafana_data_source" "cloudwatch" {
  type = "cloudwatch"
  name = module.this.id
  json_data_encoded = jsonencode({
    defaultRegion = data.aws_region.current.name
    authType      = "default"
    assumeRoleArn = aws_iam_role.cross_account_role_cloudwatch_logs.arn,
    externalId    = random_password.cloudwatch_assume_role_external_id.result
  })
}
