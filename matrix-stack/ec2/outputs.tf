output "instance_id" {
  value = aws_instance.default[0].id
}

output "instance_arn" {
  value = aws_instance.default[0].arn
}

output "iam_role_arn" {
  value       = module.this.enabled ? module.instance_role_profile[0].iam_role_arn : null
  description = "The ARN for the role attached to the instance profile"
}

output "iam_role_name" {
  value       = module.this.enabled ? module.instance_role_profile[0].iam_role_name : null
  description = "The name of the role attached to the instance profile"
}

output "instance_profile_name" {
  value       = module.this.enabled ? module.instance_role_profile[0].instance_profile_name : null
  description = "The name for the IAM instance profile with the attached policies (bucket access and SSM)"
}

output "data_volume_arn" {
  value = aws_ebs_volume.data[0].arn
}
