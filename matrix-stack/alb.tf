module "label_alb" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["alb"]
  context    = module.this.context
}

module "alb_security_group" {
  source  = "cloudposse/security-group/aws"
  version = "2.2.0"
  count   = module.label_alb.enabled ? 1 : 0
  vpc_id  = local.vpc_id
  rules = [
    {
      key         = "HTTP"
      type        = "ingress",
      from_port   = "80",
      to_port     = "80",
      protocol    = "tcp",
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      key         = "HTTPS"
      type        = "ingress",
      from_port   = "443",
      to_port     = "443",
      protocol    = "tcp",
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      key         = "FEDERATION"
      type        = "ingress",
      from_port   = "8448",
      to_port     = "8448",
      protocol    = "tcp",
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  context = module.label_ec2.context
}

module "alb" {
  source                                  = "cloudposse/alb/aws"
  count                                   = module.label_alb.enabled ? 1 : 0
  version                                 = "1.10.0"
  context                                 = module.label_alb.context
  vpc_id                                  = local.vpc_id
  security_group_ids                      = [module.alb_security_group[0].id]
  subnet_ids                              = local.ec2_public_subnet_ids
  internal                                = false
  http_enabled                            = true
  http_redirect                           = true
  access_logs_enabled                     = true
  http2_enabled                           = true
  idle_timeout                            = 60
  ip_address_type                         = "ipv4"
  deletion_protection_enabled             = local.deletion_protection_enabled
  health_check_path                       = "/health"
  health_check_timeout                    = 5
  health_check_healthy_threshold          = 3
  health_check_unhealthy_threshold        = 3
  health_check_interval                   = 10
  health_check_matcher                    = "200-299"
  target_group_port                       = 80
  target_group_target_type                = "instance"
  default_target_group_enabled            = false
  alb_access_logs_s3_bucket_force_destroy = !local.deletion_protection_enabled
}

locals {
  target_groups_defaults = {
    cookie_duration                  = 86400
    deregistration_delay             = 300
    health_check_interval            = 10
    health_check_healthy_threshold   = 3
    health_check_path                = "/health"
    health_check_port                = "traffic-port"
    health_check_timeout             = 5
    health_check_unhealthy_threshold = 3
    health_check_matcher             = "200-299"
    stickiness_enabled               = true
    target_type                      = "instance"
    slow_start                       = 0
  }
}

resource "aws_lb_target_group" "ingress_client" {
  name                 = "${module.label_alb.id}-cli"
  count                = module.label_alb.enabled ? 1 : 0
  vpc_id               = local.vpc_id
  port                 = 80
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = local.target_groups_defaults["health_check_path"]
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [module.alb[0]]

  lifecycle {
    create_before_destroy = true
  }

  tags = module.label_alb.tags
}


resource "aws_lb_target_group" "ingress_federation" {
  name                 = "${module.label_alb.id}-fed"
  count                = module.label_alb.enabled ? 1 : 0
  vpc_id               = local.vpc_id
  port                 = 80
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = local.target_groups_defaults["health_check_path"]
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [module.alb[0]]

  lifecycle {
    create_before_destroy = true
  }

  tags = module.label_alb.tags
}


resource "aws_lb_target_group_attachment" "ingress_client" {
  count            = module.label_alb.enabled ? 1 : 0
  target_group_arn = aws_lb_target_group.ingress_client[0].arn
  target_id        = module.main_instance[0].instance_id
  port             = 80
}


resource "aws_lb_target_group_attachment" "ingress_federation" {
  count            = module.label_alb.enabled ? 1 : 0
  target_group_arn = aws_lb_target_group.ingress_federation[0].arn
  target_id        = module.main_instance[0].instance_id
  port             = 80
}

resource "aws_lb_listener" "ingress_https" {
  count             = module.label_alb.enabled ? 1 : 0
  load_balancer_arn = module.alb[0].alb_arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = local.certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.ingress_client[0].arn
    type             = "forward"
  }
}

resource "aws_lb_listener" "ingress_federation_https" {
  count             = module.label_alb.enabled ? 1 : 0
  load_balancer_arn = module.alb[0].alb_arn
  port              = "8448"
  protocol          = "HTTPS"
  certificate_arn   = local.certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.ingress_federation[0].id
    type             = "forward"
  }
}


resource "aws_lb_listener_rule" "ingress" {
  count        = module.label_alb.enabled ? 1 : 0
  listener_arn = aws_lb_listener.ingress_https[0].arn
  priority     = 70

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ingress_client[0].arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

resource "aws_lb_listener_rule" "ingress_federation" {
  count        = module.label_alb.enabled ? 1 : 0
  listener_arn = aws_lb_listener.ingress_federation_https[0].arn
  priority     = 70

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ingress_federation[0].arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

module "alb_alarms_ingress_federation" {
  source                  = "cloudposse/alb-target-group-cloudwatch-sns-alarms/aws"
  version                 = "0.17.0"
  context                 = module.label_alb.context
  name                    = "ingress-federation"
  alb_arn_suffix          = module.alb[0].alb_arn_suffix
  target_group_arn_suffix = aws_lb_target_group.ingress_federation[0].arn_suffix

  evaluation_periods             = var.alb_alarms_evaluation_periods
  period                         = var.alb_alarms_period
  target_3xx_count_threshold     = var.alb_alarms_target_3xx_count_threshold
  target_4xx_count_threshold     = var.alb_alarms_target_4xx_count_threshold
  target_5xx_count_threshold     = var.alb_alarms_target_5xx_count_threshold
  elb_5xx_count_threshold        = var.alb_alarms_elb_5xx_count_threshold
  target_response_time_threshold = var.alb_alarms_target_response_time_threshold

  notify_arns               = local.sns_topic_arns_alarms
  alarm_actions             = local.sns_topic_arns_alarms
  ok_actions                = local.sns_topic_arns_alarms
  insufficient_data_actions = local.sns_topic_arns_alarms
}

module "alb_alarms_ingress_client" {
  source                  = "cloudposse/alb-target-group-cloudwatch-sns-alarms/aws"
  version                 = "0.17.0"
  context                 = module.label_alb.context
  name                    = "ingress-client"
  alb_arn_suffix          = module.alb[0].alb_arn_suffix
  target_group_arn_suffix = aws_lb_target_group.ingress_client[0].arn_suffix

  evaluation_periods             = var.alb_alarms_evaluation_periods
  period                         = var.alb_alarms_period
  target_3xx_count_threshold     = var.alb_alarms_target_3xx_count_threshold
  target_4xx_count_threshold     = var.alb_alarms_target_4xx_count_threshold
  target_5xx_count_threshold     = var.alb_alarms_target_5xx_count_threshold
  elb_5xx_count_threshold        = var.alb_alarms_elb_5xx_count_threshold
  target_response_time_threshold = var.alb_alarms_target_response_time_threshold

  notify_arns               = local.sns_topic_arns_alarms
  alarm_actions             = local.sns_topic_arns_alarms
  ok_actions                = local.sns_topic_arns_alarms
  insufficient_data_actions = local.sns_topic_arns_alarms
}
