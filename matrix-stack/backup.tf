module "ec2_backup" {
  source            = "lgallard/backup/aws"
  version           = "0.19.3"
  vault_name        = "${module.this.id}-ec2"
  vault_kms_key_arn = var.kms_key_arn
  plan_name         = "${module.this.id}-ec2"

  notifications = {
    sns_topic_arn       = local.sns_topic_arns_alarms[0]
    backup_vault_events = ["BACKUP_JOB_COMPLETED", "BACKUP_JOB_FAILED"]
  }
  rules = [
    {
      name                     = "ec2"
      schedule                 = "cron(0 12 * * ? *)"
      target_vault_name        = null
      start_window             = 120
      completion_window        = 360
      enable_continuous_backup = false
      lifecycle = {
        delete_after = var.backup_storage_retention_days
      },
      recovery_point_tags = module.this.tags
    }
  ]
  selections = [
    {
      name = "ec2-instance"
      resources = [
        module.main_instance[0].instance_arn,
        #module.main_instance.data_volume_arn
      ]
    }
  ]
  tags = module.this.tags
}


module "rds_backup" {
  source            = "lgallard/backup/aws"
  version           = "0.19.3"
  vault_name        = "${module.this.id}-rds"
  vault_kms_key_arn = var.kms_key_arn
  plan_name         = "${module.this.id}-rds"

  notifications = {
    sns_topic_arn       = local.sns_topic_arns_alarms[0]
    backup_vault_events = ["BACKUP_JOB_COMPLETED", "BACKUP_JOB_FAILED"]
  }
  rules = [
    {
      name                     = "rds"
      schedule                 = "cron(0 5 * * ? *)"
      target_vault_name        = null
      start_window             = 120
      completion_window        = 480
      enable_continuous_backup = false
      lifecycle = {
        delete_after = var.backup_storage_retention_days
      },
      recovery_point_tags = module.this.tags
    }
  ]
  selections = [
    {
      name      = "ec2-instance"
      resources = module.db.*.this.db_instance_arn

    }
  ]
  tags = module.this.tags
}
