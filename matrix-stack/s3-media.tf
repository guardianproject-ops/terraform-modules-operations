module "label_s3media" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["synapse", "media"]
  context    = module.this.context
}
resource "aws_s3_bucket" "synapse_media" {
  count         = local.enabled ? 1 : 0
  bucket        = module.label_s3media.id
  force_destroy = var.s3_media_force_destroy
  tags          = module.label_s3media.tags
}

resource "aws_s3_bucket_lifecycle_configuration" "synapse_media" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.synapse_media[0].id
  rule {
    id     = "expiration"
    status = "Enabled"
    expiration {
      days = var.s3_media_expiration_days
    }
  }
}


resource "aws_kms_key" "media_bucket" {
  description             = module.label_s3media.id
  deletion_window_in_days = 10
  tags                    = module.label_s3media.tags
}

resource "aws_kms_alias" "media_bucket" {
  name          = "alias/${module.label_s3media.id}"
  target_key_id = aws_kms_key.media_bucket.key_id
}

resource "aws_s3_bucket_server_side_encryption_configuration" "synapse_media" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.synapse_media[0].id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.media_bucket.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "synapse_media" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.synapse_media[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_versioning" "synapse_media" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.synapse_media[0].id
  versioning_configuration {
    status = "Disabled"
  }
}
