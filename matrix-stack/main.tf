locals {
  enabled                     = module.this.enabled
  deletion_protection_enabled = true
  availability_zones          = sort(slice(data.aws_availability_zones.this.names, 0, 2))
  default_az                  = local.availability_zones[0]
  vpc_id                      = module.vpc[0].vpc_id
  vpc_cidr_block              = module.vpc[0].vpc_cidr_block
  rds_subnet_ids              = module.rds_subnets[0].private_subnet_ids
  ec2_private_subnet_ids      = module.ec2_subnets[0].private_subnet_ids
  ec2_private_subnet_main_id  = module.ec2_subnets[0].az_private_subnets_map[local.default_az][0]
  ec2_public_subnet_ids       = module.ec2_subnets[0].public_subnet_ids
  ec2_public_subnet_main_id   = module.ec2_subnets[0].az_public_subnets_map[local.default_az][0]
  synapse_media_bucket_name   = aws_s3_bucket.synapse_media[0].id
  certificate_arn             = module.acm_certs[0].certificate_arns[var.matrix_server_domain]
  private_subnet_cidrs        = module.ec2_subnets[0].private_subnet_cidrs
  public_subnet_cidrs         = module.ec2_subnets[0].public_subnet_cidrs
  operations_account_id       = data.aws_caller_identity.operations.account_id
  account_id                  = data.aws_caller_identity.this.account_id
  region                      = data.aws_region.current.name
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

provider "tailscale" {
  oauth_client_id     = var.tailscale_client_id
  oauth_client_secret = var.tailscale_client_secret
  tailnet             = var.tailscale_tailnet
}

data "aws_caller_identity" "this" {}
data "aws_region" "current" {}

data "aws_caller_identity" "operations" {
  provider = aws.gp_operations
}


data "aws_partition" "current" {}

data "aws_availability_zones" "this" {
  state = "available"
}

data "aws_ssm_parameter" "pagerduty_token" {
  name     = "/shared-secrets/pagerduty_token"
  provider = aws.gp_operations
}

provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}

provider "grafana" {
  url  = var.grafana_url
  auth = var.grafana_auth
  http_headers = {
    "CF-Access-Client-Id" : var.grafana_cloudflare_service_token_client_id
    "CF-Access-Client-Secret" : var.grafana_cloudflare_service_token_client_secret
  }
}
