module "label_db" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["db"]
  context    = module.this.context
  enabled    = !var.use_external_rds
}

resource "random_password" "rds_password" {
  length  = 32
  special = false
}


resource "aws_kms_key" "rds" {
  description             = "${module.label_db.id}-rds"
  deletion_window_in_days = 10
  tags                    = module.label_db.tags
}
resource "aws_kms_key_policy" "rds" {
  key_id = aws_kms_key.rds.id
  policy = data.aws_iam_policy_document.rds.json
}

resource "aws_kms_alias" "rds" {
  name          = "alias/${module.label_db.id}"
  target_key_id = aws_kms_key.rds.key_id
}

locals {
  db_identifier = var.rds_db_identifier != null ? var.rds_db_identifier : module.label_db.id
}

data "aws_iam_policy_document" "rds" {
  statement {
    sid = "Delegate permissions to IAM policies"

    actions = [
      "kms:*",
    ]

    resources = ["*"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.account_id}:root"]
    }
  }

  statement {
    sid    = "Allow Cloudwatch to use this key"
    effect = "Allow"
    actions = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*"
    ]
    resources = ["*"]

    principals {
      type        = "Service"
      identifiers = ["logs.${local.region}.amazonaws.com"]
    }

    condition {
      test     = "ArnEquals"
      variable = "kms:EncryptionContext:aws:logs:arn"
      values   = ["arn:aws:logs:${local.region}:${local.account_id}:log-group:/aws/rds/instance/${local.db_identifier}/*"]
    }
  }

  statement {
    sid = "Allow access through RDS for all principals in the account that are authorized to use RDS"

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:DescribeKey"
    ]

    resources = ["*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"

      values = [local.account_id]
    }

    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"

      values = [
        "rds.${local.region}.amazonaws.com"
      ]
    }
  }
}


module "db" {
  source  = "./rds"
  count   = module.label_db.enabled ? 1 : 0
  context = module.label_db.context

  allocated_storage               = var.rds_allocated_storage_gb
  max_allocated_storage_gb        = var.rds_max_allocated_storage_gb
  allow_access_cidr_blocks        = local.private_subnet_cidrs
  allow_major_version_upgrade     = var.rds_allow_major_version_upgrade
  apply_immediately               = true
  backup_retention_period         = 30
  backup_window                   = "03:00-06:00"
  database_name                   = var.rds_admin_db_name
  database_password               = coalesce(var.rds_admin_password, random_password.rds_password.result)
  database_username               = var.rds_admin_user
  db_identifier                   = var.rds_db_identifier
  db_subnet_group_name            = var.rds_db_subnet_group_name
  create_db_subnet_group          = var.rds_create_db_subnet_group
  db_subnet_group_use_name_prefix = var.rds_db_subnet_group_use_name_prefix
  deletion_protection_enabled     = var.rds_deletion_protection_enabled
  engine_version                  = var.rds_engine_version
  family                          = var.rds_family
  iops                            = var.rds_iops
  storage_throughput              = var.rds_storage_throughput
  instance_class                  = var.rds_instance_class
  kms_key_id                      = aws_kms_key.rds.arn
  major_engine_version            = var.rds_major_engine_version
  snapshot_identifier             = var.rds_snapshot_identifier
  skip_final_snapshot             = false
  sns_topic_arns_alarms           = local.sns_topic_arns_alarms
  subnet_ids                      = local.rds_subnet_ids
  vpc_id                          = local.vpc_id
  depends_on                      = [aws_kms_key_policy.rds]
}
