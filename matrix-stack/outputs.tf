output "vpc_id" {
  value = local.vpc_id
}
output "vpc_cidr_block" {
  value = local.vpc_cidr_block
}

output "rds_subnet_ids" {
  value = module.rds_subnets[0].private_subnet_ids
}

output "public_subnet_cidrs" {
  description = "IPv4 CIDRs assigned to the created public subnets"
  value       = module.ec2_subnets[0].public_subnet_cidrs
}

output "private_subnet_cidrs" {
  description = "IPv4 CIDRs assigned to the created private subnets"
  value       = module.ec2_subnets[0].private_subnet_cidrs
}

output "public_route_table_ids" {
  description = "IDs of the created public route tables"
  value       = module.ec2_subnets[0].public_route_table_ids
}

output "private_route_table_ids" {
  description = "IDs of the created private route tables"
  value       = module.ec2_subnets[0].private_route_table_ids
}
output "az_private_subnets_map" {
  description = "Map of AZ names to list of private subnet IDs in the AZs"
  value       = module.ec2_subnets[0].az_private_subnets_map
}

output "az_public_subnets_map" {
  description = "Map of AZ names to list of public subnet IDs in the AZs"
  value       = module.ec2_subnets[0].az_public_subnets_map
}

output "named_private_subnets_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of private subnet IDs"
  value       = module.ec2_subnets[0].named_private_subnets_map
}

output "named_public_subnets_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of public subnet IDs"
  value       = module.ec2_subnets[0].named_public_subnets_map
}

output "named_private_route_table_ids_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of private route table IDs"
  value       = module.ec2_subnets[0].named_private_route_table_ids_map
}

output "named_public_route_table_ids_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of public route table IDs"
  value       = module.ec2_subnets[0].named_public_route_table_ids_map
}

output "named_private_subnets_stats_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of objects with each object having three items: AZ, private subnet ID, private route table ID"
  value       = module.ec2_subnets[0].named_private_subnets_stats_map
}

output "named_public_subnets_stats_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of objects with each object having three items: AZ, public subnet ID, public route table ID"
  value       = module.ec2_subnets[0].named_public_subnets_stats_map
}

output "main_instance_id" {
  value = module.main_instance[0].instance_id
}

output "rds_admin_username" {
  value = var.rds_admin_user
}

output "sns_topics_alarms" {
  value = local.sns_topics_alarms
}
output "sns_topic_arns_alarms" {
  value = local.sns_topic_arns_alarms
}

output "db_instance_address" {
  value = join("", module.db.*.this.db_instance_address)
}

output "db_instance_arn" {
  value = join("", module.db.*.this.db_instance_arn)
}

output "db_instance_master_user_secret_arn" {
  value = join("", module.db.*.this.db_instance_master_user_secret_arn)
}

output "db_instance_master_user_secret_name" {
  value = join("", module.db.*.db_instance_master_user_secret_name)
}

output "synapse_media_bucket_name" {
  value = aws_s3_bucket.synapse_media[0].id
}

output "certificate_arns" {
  value = module.acm_certs[0].certificate_arns
}

output "matrix_certificate_arn" {
  value = module.acm_certs[0].certificate_arns[var.matrix_server_domain]
}

output "log_group_main_arn" {
  value = aws_cloudwatch_log_group.main[0].arn
}

output "log_group_main_name" {
  value = aws_cloudwatch_log_group.main[0].name
}

output "main_instance_tailnet_key" {
  value = tailscale_tailnet_key.main.key
}

output "kms_key_arn_rds" {
  description = "The ARN of the kms key that encrypts the RDS instance and metrics."
  value       = aws_kms_key.rds.arn
}

output "mirrors" {
  value = module.mirror
}
