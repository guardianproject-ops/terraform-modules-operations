variable "allow_access_cidr_blocks" {
  description = "List of CIDR strings to allow access on port 5432 to the database"
  type        = list(string)
}

variable "vpc_id" {
  type = string
}

variable "database_name" {
  type = string
}

variable "database_username" {
  type = string
}

variable "database_password" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "allocated_storage" {
  type = number
}

variable "family" {
  type = string
}

variable "major_engine_version" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "deletion_protection_enabled" {
  type = bool
}

variable "skip_final_snapshot" {
  type = bool
}

variable "allow_major_version_upgrade" {
  type = bool
}

variable "apply_immediately" {
  type = bool
}

variable "backup_retention_period" {
  type = number
}

variable "maintenance_window" {
  default = "Mon:00:00-Mon:03:00"
  type    = string
}

variable "backup_window" {
  default = "03:00-06:00"
  type    = string
}

variable "monitoring_interval" {
  default = "60"
  type    = string
}

variable "region_settings_params_enabled" {
  type        = bool
  default     = true
  description = "When enabled the connection information (except password) is stored in SSM Param Store region settings for this deployment"
}

variable "kms_key_id" {
  type        = string
  description = "The KMS key to use to encrypt the database, log groups, and performance insights."
}

variable "sns_topic_arns_alarms" {
  type = list(string)
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05"
  type        = string
  default     = null
}


variable "storage_type" {
  type        = string
  default     = "gp3"
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), 'gp3' (new generation of general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'gp2' if not. If you specify 'io1' or 'gp3' , you must also include a value for the 'iops' parameter"
}

variable "iops" {
  type        = number
  default     = null
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1' or `gp3`. See `notes` for limitations regarding this variable for `gp3`"
}


variable "max_allocated_storage_gb" {
  type        = number
  default     = 0
  description = "Specifies the value for Storage Autoscaling"
}

variable "ca_cert_identifier" {
  # ref: https://aws.amazon.com/blogs/aws/rotate-your-ssl-tls-certificates-now-amazon-rds-and-amazon-aurora-expire-in-2024/
  type        = string
  default     = "rds-ca-ecc384-g1"
  description = "Specifies the identifier of the CA certificate for the DB instance"
}

variable "cloudwatch_log_group_retention_in_days" {
  type = number

  default     = 90
  description = "The number of days to retain CloudWatch logs for the DB instance"
}
variable "performance_insights_retention_period_days" {
  type        = number
  default     = 93
  description = "The amount of time in days to retain Performance Insights data. Valid values are `7`, `731` (2 years) or a multiple of `31`"
}


variable "storage_throughput" {
  default = null
  type    = number
}

variable "db_identifier" {
  type    = string
  default = null
}


variable "db_subnet_group_name" {
  type    = string
  default = null
}

variable "db_subnet_group_use_name_prefix" {
  type    = bool
  default = true
}

variable "create_db_subnet_group" {
  type    = bool
  default = true
}
