resource "aws_security_group" "rds" {
  name        = module.this.id
  description = "Security group for DB instance for ${module.this.id}"
  vpc_id      = var.vpc_id
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = var.allow_access_cidr_blocks
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }


  tags = module.this.tags
}

module "db" {
  source                                 = "terraform-aws-modules/rds/aws"
  version                                = "6.2.0"
  allocated_storage                      = var.allocated_storage
  allow_major_version_upgrade            = var.allow_major_version_upgrade
  apply_immediately                      = var.apply_immediately
  backup_retention_period                = var.backup_retention_period
  backup_window                          = var.backup_window
  ca_cert_identifier                     = var.ca_cert_identifier
  create_db_option_group                 = false
  create_db_subnet_group                 = var.create_db_subnet_group
  db_subnet_group_use_name_prefix        = var.db_subnet_group_use_name_prefix
  db_subnet_group_name                   = var.db_subnet_group_name != null ? var.db_subnet_group_name : module.this.id
  create_monitoring_role                 = true
  create_cloudwatch_log_group            = true
  copy_tags_to_snapshot                  = true
  cloudwatch_log_group_retention_in_days = var.cloudwatch_log_group_retention_in_days
  cloudwatch_log_group_kms_key_id        = var.kms_key_id
  db_name                                = var.database_name
  db_instance_tags                       = module.this.tags
  db_parameter_group_tags                = module.this.tags
  db_option_group_tags                   = module.this.tags
  parameter_group_name                   = "${module.this.id}-${var.family}"
  db_subnet_group_tags                   = module.this.tags
  deletion_protection                    = var.deletion_protection_enabled
  engine                                 = "postgres"
  engine_version                         = var.engine_version
  family                                 = var.family
  identifier                             = var.db_identifier != null ? var.db_identifier : module.this.id
  instance_class                         = var.instance_class
  iops                                   = var.iops
  storage_throughput                     = var.storage_throughput
  kms_key_id                             = var.kms_key_id
  maintenance_window                     = var.maintenance_window
  major_engine_version                   = var.major_engine_version
  manage_master_user_password            = true
  max_allocated_storage                  = var.max_allocated_storage_gb
  monitoring_interval                    = var.monitoring_interval
  monitoring_role_name                   = "AllowRDSMonitoringFor-${module.this.id}"
  multi_az                               = false
  password                               = var.database_password
  performance_insights_enabled           = true
  performance_insights_retention_period  = var.performance_insights_retention_period_days
  performance_insights_kms_key_id        = var.kms_key_id
  port                                   = "5432"
  skip_final_snapshot                    = var.skip_final_snapshot
  snapshot_identifier                    = var.snapshot_identifier
  storage_encrypted                      = true
  subnet_ids                             = var.subnet_ids
  enabled_cloudwatch_logs_exports        = ["postgresql", "upgrade"]
  username                               = var.database_username
  vpc_security_group_ids                 = [aws_security_group.rds.id]
  storage_type                           = var.storage_type
  tags                                   = module.this.tags
}

module "rds_alarms" {
  source            = "lorenzoaiello/rds-alarms/aws"
  version           = "2.2.0"
  db_instance_id    = module.db.db_instance_resource_id
  db_instance_class = var.instance_class
  prefix            = "${module.this.id}-"
  actions_alarm     = var.sns_topic_arns_alarms
  actions_ok        = var.sns_topic_arns_alarms
  engine            = "postgres"
  tags              = module.this.tags
}

data "aws_secretsmanager_secret" "db" {
  arn = module.db.db_instance_master_user_secret_arn
}

output "this" {
  value = module.db
}

output "db_instance_master_user_secret_name" {
  value = data.aws_secretsmanager_secret.db.name
}
