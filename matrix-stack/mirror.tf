module "label_mirror" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["mirror"]
  context    = module.this.context
  enabled    = var.enable_mirrors
}

module "mirror" {
  for_each      = module.label_mirror.enabled ? var.mirrors : {}
  source        = "./mirror"
  context       = module.label_mirror.context
  origin_domain = var.matrix_server_domain
  attributes    = [each.value.name]
}
