terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.0"
    }
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1, aws.gp_operations]
      version               = ">= 4.0"
    }
    tailscale = {
      source  = "tailscale/tailscale"
      version = "0.13.11"
    }
    pagerduty = {
      source  = "pagerduty/pagerduty"
      version = ">= 3.1.1, < 4.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = ">= 3.17"
    }
  }
  required_version = ">= 1.3"
}
