variable "governed_regions" {
  description = "List of AWS regions to enable LandingZone, GuardDuty, etc in"
  type        = list(string)
}

variable "ous" {
  description = <<-EOT
Map of the child organizational units to create and manage. The map key is the name of the OU, and the value is an object containing configuration variables for the OU.
EOT

  type = map(object({
    tags      = optional(map(string), {})
    parent_ou = optional(string, null)
  }))
}

variable "guardduty_delegation_enabled" {
  type        = bool
  default     = true
  description = <<-EOT
Whether to delegate GuardDuty administration to the GuardDuty delegated admin account.
EOT
}

variable "controltower_guardrails" {
  type = list(object({
    control_name   = string
    is_global_type = optional(bool, true)
    ou_name        = string
    parameters     = optional(map(string), {})
  }))
  description = "Configuration of AWS Control Tower Guardrails for the whole organization"
}

variable "accounts" {
  description = <<-EOT
Map of the AWS accounts to ensure are created with AWS Control Tower.
EOT
  type = map(object({
    name   = string
    email  = string
    parent = string
    tags   = optional(map(string))
  }))
}

variable "control_tower_accounts" {
  description = "Information about the pre-req Control Tower core accounts. These must already exist!"
  type = object({
    audit = object({
      id = string
    })
    management = object({
      id = string
    })
    logging = object({
      id = string
    })
  })
}

variable "organizational_unit_id_on_delete" {
  description = "The ID of the organizational unit to move accounts to when they are deleted."
  type        = string
  default     = null
}
