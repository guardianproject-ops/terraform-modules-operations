terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
    controltower = {
      source  = "idealo/controltower"
      version = "2.1.0"
    }
  }
}

module "root" {
  #source = "/home/abel/src/guardianproject-ops/terraform-aws-account-baseline-root"
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-baseline-root?ref=main"

  context                          = module.this.context
  ous                              = var.ous
  guardduty_delegation_enabled     = var.guardduty_delegation_enabled
  governed_regions                 = var.governed_regions
  controltower_guardrails          = var.controltower_guardrails
  accounts                         = var.accounts
  control_tower_accounts           = var.control_tower_accounts
  organizational_unit_id_on_delete = var.organizational_unit_id_on_delete
}

output "landing_zone" {
  value = module.root.landing_zone
}

output "organization" {
  value = module.root.organization
}

output "guardrails" {
  value = module.root.guardrails
}
