module "region_settings" {
  source  = "../region-settings-data"
  context = module.this.context
}

module "sentry" {
  source                        = "./sentry"
  context = module.this.context
  sentry_fqdn                   = var.sentry_fqdn
  expiration_days               = var.expiration_days
  cloudflare_zone               = var.cloudflare_zone
  cloudflare_edit_token         = var.cloudflare_edit_token
  is_prod_like                  = var.is_prod_like
  vpc_id                        = module.region_settings.vpc.id
  kms_key_arn                   = module.region_settings.kms_key_arn
  secretsmanager_ssh_key_id     = module.region_settings.ssh_key_secret_id
  vpc_cidr_block                = module.region_settings.vpc.cidr_block
  subnet_id                     = module.region_settings.private_subnet_1.id
  availability_zone_1           = module.region_settings.private_subnet_1.availability_zone
  alb_public_subnet_ids         = module.region_settings.public_subnet_ids
  disk_allocation_gb            = var.disk_allocation_gb
  ebs_volume_disk_allocation_gb = var.ebs_volume_disk_allocation_gb
  instance_type                 = var.instance_type
  secure_metadata_endpoint      = true
}