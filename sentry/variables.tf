variable "sentry_fqdn" {
  description = "(Required) the domain at which sentry will be accessible"
}

variable "expiration_days" {
  type        = number
  description = "(Optional) The number of days after which logs expire"
  default     = 30
}

variable "cloudflare_zone" {
  description = "(Required) The cloudflare zone (root domain) sentry is accessible under"
  type        = string
}

variable "cloudflare_edit_token" {
  description = "(Required) A cloudflare api token for editing dns records"
  type        = string
}

variable "instance_type" {
  type        = string
  description = "(Required) The EC2 instance type"
}

variable "disk_allocation_gb" {
  description = "(Required) The amount in gigabytes the instance root disk will be allocated"
  type        = number
}

variable "ebs_volume_disk_allocation_gb" {
  description = "(Required) The amount in gigabytes that the extra ebs volume will be allocated"
  type        = number
}

variable "is_prod_like" {
  type        = bool
  description = "(Required) If false, things will be easier to destroy"
}