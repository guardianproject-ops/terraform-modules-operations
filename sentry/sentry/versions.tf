terraform {
  required_version = ">= 1.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 3.1, < 5.0"
    }
    aws = {
      source = "hashicorp/aws"
    }
  }
}

