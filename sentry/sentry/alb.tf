locals {

  target_groups_defaults = {
    cookie_duration                  = 86400
    deregistration_delay             = 300
    health_check_interval            = 10
    health_check_healthy_threshold   = 3
    health_check_path                = "/"
    health_check_port                = "traffic-port"
    health_check_timeout             = 5
    health_check_unhealthy_threshold = 3
    health_check_matcher             = "200-299"
    stickiness_enabled               = true
    target_type                      = "instance"
    slow_start                       = 0
  }
}

module "label_alb" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.24.1"
  attributes = ["loadbalancer"]
  context    = module.this.context
}
locals {
  instance_id = module.instance.id
}


resource "aws_security_group" "public_load_balancer" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_alb.tags
}

resource "aws_lb" "application" {
  # aws lb names are limited to 32 chars
  name                             = substr(module.label.id, 0, min(length(module.label.id), 32))
  load_balancer_type               = "application"
  internal                         = false
  security_groups                  = [aws_security_group.public_load_balancer.id]
  subnets                          = var.alb_public_subnet_ids
  idle_timeout                     = "60"
  enable_cross_zone_load_balancing = false
  enable_deletion_protection       = var.is_prod_like
  enable_http2                     = true
  ip_address_type                  = "ipv4"

  access_logs {
    enabled = true
    bucket  = aws_s3_bucket.log_bucket.id
    prefix  = module.label_alb.id
  }

  timeouts {
    create = "10m"
    delete = "10m"
    update = "10m"
  }

  tags = module.label.tags
  depends_on = [
    aws_s3_bucket_policy.alb_log_bucket
  ]
}

resource "aws_lb_target_group" "sentry" {
  name                 = "sentry"
  vpc_id               = var.vpc_id
  port                 = 9000
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = "/_health/"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "sentry")
    },
  )
}

resource "aws_lb_listener" "frontend_https" {
  load_balancer_arn = aws_lb.application.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = module.cert.acm_cert.id
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.sentry.id
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "sentry" {
  target_group_arn = aws_lb_target_group.sentry.arn
  target_id        = local.instance_id
}


resource "cloudflare_record" "primary" {
  zone_id = data.cloudflare_zone.this.id
  name    = var.sentry_fqdn
  value   = aws_lb.application.dns_name
  type    = "CNAME"
  ttl     = 1800
}