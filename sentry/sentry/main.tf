provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

module "label" {
  source  = "cloudposse/label/null"
  version = "0.25.0"
  context = module.this.context
  name    = "sentry"
  tags    = merge(module.this.tags, { Application = "sentry" })
}

module "label_log_groups_root" {
  source    = "cloudposse/label/null"
  version   = "0.25.0"
  delimiter = "/"
  context   = module.this.context
}

data "cloudflare_zone" "this" {
  name = var.cloudflare_zone
}

module "cert" {
  source                    = "./cert"
  domain_name               = var.sentry_fqdn
  cloudflare_zone_id        = data.cloudflare_zone.this.id
}

data "aws_iam_policy_document" "read_acm_cert" {
  statement {
    sid = "AllowListACMCert"
    actions = [
      "acm:ListCertificates",
      "acm:ListTagsForCertificate"
    ]

    resources = [
      "*"
    ]
  }
  statement {
    sid = "AllowReadACMCert"
    actions = [
      "acm:GetCertificate",
      "acm:DescribeCertificate"
    ]

    resources = [
      module.cert.acm_cert.id
    ]
  }
}

resource "aws_iam_policy" "read_acm_cert" {
  name        = "read-acm-cert-${module.this.id}"
  description = "Allows instance to read public ACM certificate"
  policy      = data.aws_iam_policy_document.read_acm_cert.json
}

module "instance" {
  source = "../../ec2-instance"

  is_prod_like                  = var.is_prod_like
  kms_key_arn                   = var.kms_key_arn
  secretsmanager_ssh_key_id     = var.secretsmanager_ssh_key_id
  vpc_id                        = var.vpc_id
  vpc_cidr_block                = var.vpc_cidr_block
  availability_zone_1           = var.availability_zone_1
  subnet_id                     = var.subnet_id
  disk_allocation_gb            = var.disk_allocation_gb
  instance_type                 = var.instance_type
  log_groups_root               = local.log_groups_root
  enable_ebs_volume             = true
  ebs_volume_disk_allocation_gb = var.ebs_volume_disk_allocation_gb
  ebs_volume_mount_path         = "/srv/data"
  secure_metadata_endpoint      = true
  extra_iam_policy_arns = [
    aws_iam_policy.read_acm_cert.arn,
  ]
  ingress = {
    "sentry" = {
      from_port = 9000,
      to_port   = 9000,
      protocol  = "tcp"
    }
  }
  context = module.label.context
}

locals {
  log_groups_root = "/${module.label_log_groups_root.id}"
  certs = {
    "${var.sentry_fqdn}" = {
      common_name = var.sentry_fqdn
      dns_names   = [var.sentry_fqdn, "push.${var.sentry_fqdn}"]
    }
  }
}