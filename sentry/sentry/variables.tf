variable "sentry_fqdn" {
  description = "(Required) the domain at which sentry will be accessible"
}

variable "alb_public_subnet_ids" {
  description = "(Required) at least two subnets in different AZs for the ALB"
  type        = list(string)
}

variable "expiration_days" {
  type        = number
  description = "(Optional) The number of days after which logs expire"
  default     = 30
}

variable "log_bucket_policy" {
  type        = string
  description = "(Optional) A valid bucket policy JSON document to attach to the log bucket"
  default     = ""
}

variable "cloudflare_zone" {
  description = "(Required) The cloudflare zone (root domain) sentry is accessible under"
  type        = string
}

variable "cloudflare_edit_token" {
  description = "(Required) A cloudflare api token for editing dns records"
  type        = string
}