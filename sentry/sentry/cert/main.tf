resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = module.this.tags
}

locals {
  aws_domain_validation_options = { for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
    name   = dvo.resource_record_name
    record = dvo.resource_record_value
    type   = dvo.resource_record_type
    }
  }
  domain_validation_options = var.is_already_validated ? {} : local.aws_domain_validation_options
  validated_hostnames       = var.is_already_validated ? var.validated_hostnames : [for record in cloudflare_record.acm : record.hostname]
}

resource "cloudflare_record" "acm" {
  for_each = local.domain_validation_options
  zone_id  = var.cloudflare_zone_id
  name     = each.value.name
  value    = each.value.record
  type     = each.value.type
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = local.validated_hostnames
  depends_on = [cloudflare_record.acm]
}
