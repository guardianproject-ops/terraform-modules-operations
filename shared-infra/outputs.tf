output "ami_sharing" {
  value = module.ami_sharing
}

output "smtp" {
  value     = module.smtp
  sensitive = true
}

output "cross_account_role_name" {
  value = module.infra_role.iam_role_name
}

output "shared_account_ids" {
  value = local.shared_account_ids
}

output "shared_account_ids_stringlist" {
  value = local.shared_account_ids_stringlist
}
