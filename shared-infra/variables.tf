terraform {
  experiments = [module_variable_optional_attrs]
}

variable "shared_to_accounts" {
  description = "accounts that will be allowed to decrypt the shared AMIs. key should be the account name"
  type = map(object({
    id           = string
    smtp_domains = optional(list(string))
  }))
}

variable "ses_domains" {
  type = map(object({
    dmarc_rua          = string
    cloudflare_zone_id = string
    mail_from_domain   = optional(string)
    iam_permissions    = optional(list(string))
  }))
}

variable "shared_parameters" {
  type = map(object({
    type  = optional(string)
    tags  = optional(map(string))
    value = string
  }))
  description = "Arbitrary values that will be saved to ssm param store"
}
