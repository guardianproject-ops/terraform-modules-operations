output "kms_key_alias" {
  value = module.kms_key.alias_name
}

output "kms_key" {
  value = module.kms_key
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}

output "ssm_parameter" {
  value = aws_ssm_parameter.kms_key.name
}
