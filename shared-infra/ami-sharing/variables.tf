variable "shared_to_accounts" {
  description = "accounts that will be allowed to decrypt the shared AMIs. key should be the account name"
  type = map(object({
    id = string
  }))
}
