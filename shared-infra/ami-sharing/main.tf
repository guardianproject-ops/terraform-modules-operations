data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "allow_kms_access" {
  statement {
    # rationale: https://docs.aws.amazon.com/kms/latest/developerguide/key-policies.html#key-policy-default-allow-root-enable-iam
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    actions   = ["kms:*"]
    resources = ["*"]
  }
  dynamic "statement" {
    # rationale: https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-modifying-external-accounts.html
    for_each = var.shared_to_accounts
    content {
      sid    = "Allow use of the ami key to account ${statement.key}"
      effect = "Allow"
      actions = [
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:CreateGrant",
        "kms:DescribeKey"
      ]
      resources = ["*"]
      principals {
        type = "AWS"
        identifiers = [
          "arn:aws:iam::${statement.value.id}:root"
        ]
      }
    }
  }
}

module "kms_key" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.9.0"
  context                 = module.this.context
  description             = "key used to encrypt shared AMIs"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  policy                  = data.aws_iam_policy_document.allow_kms_access.json
  alias                   = "alias/${module.this.id}"
}

resource "aws_ssm_parameter" "kms_key" {
  name  = "/ami-sharing/kms_key_arn"
  type  = "String"
  value = module.kms_key.key_arn
  tags  = module.this.tags
}
