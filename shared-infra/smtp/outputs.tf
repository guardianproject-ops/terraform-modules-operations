output "ses" {
  value     = module.ses
  sensitive = true
}

output "ssm_parameters" {
  value = aws_ssm_parameter.smtp
}
