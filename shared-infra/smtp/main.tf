provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

locals {
  default_iam_permissions = ["ses:SendRawEmail"]
}

module "ses" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ses-cloudflare?ref=tags/0.5.0"

  for_each = var.ses_domains

  domain_name        = each.key
  mail_from_domain   = each.value.mail_from_domain == null ? "" : each.value.mail_from_domain
  dmarc_rua          = each.value.dmarc_rua
  cloudflare_zone_id = each.value.cloudflare_zone_id
  iam_permissions    = coalesce(each.value.iam_permissions, local.default_iam_permissions)

  name    = replace(each.key, ".", "_")
  context = module.this.context
}

resource "aws_ssm_parameter" "smtp" {
  for_each = module.ses

  name  = "/smtp/${each.key}"
  type  = "SecureString"
  value = jsonencode(each.value)
  tags  = module.this.tags
}
