terraform {
  experiments = [module_variable_optional_attrs]
}

variable "cloudflare_edit_token" {
  description = "An api token with Zone.DNS.edit permissions valid for all domains/accounts."
  type        = string
}


variable "ses_domains" {
  type = map(object({
    dmarc_rua          = string
    cloudflare_zone_id = string
    mail_from_domain   = optional(string)
    iam_permissions    = optional(list(string))
  }))
}
