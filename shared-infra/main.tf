data "aws_region" "default" {}
data "aws_caller_identity" "default" {}
data "aws_ssm_parameter" "cloudflare_edit_token_guardianproject" {
  name = "/shared-secrets/cloudflare_edit_token_guardianproject"
}

locals {
  region                = data.aws_region.default.name
  account_id            = data.aws_caller_identity.default.account_id
  cloudflare_edit_token = data.aws_ssm_parameter.cloudflare_edit_token_guardianproject.value

  shared_account_ids = [
    for a in var.shared_to_accounts : a.id
  ]
  shared_account_ids_stringlist = join(",", local.shared_account_ids)
}

###########################################################
# AMI Sharing
###########################################################
# Create a KMS key used to encrypt AMIs and allow other access to access it

module "ami_sharing" {
  source             = "./ami-sharing"
  shared_to_accounts = var.shared_to_accounts
  context            = module.this.context
}

###########################################################
# SMTP
###########################################################
# Setup AWS SES and SMTP iam users for domains used by this AWS Org

module "smtp" {
  source                = "./smtp"
  ses_domains           = var.ses_domains
  cloudflare_edit_token = local.cloudflare_edit_token
  context               = module.this.context
}

###########################################################
# Shared SSM Parameters
###########################################################

resource "aws_ssm_parameter" "shared" {
  for_each = var.shared_parameters

  name  = "/shared/${each.key}"
  type  = try(each.value.type, "SecureString")
  value = each.value.value
  tags  = merge(module.this.tags, try(each.value.tags, {}))
}

###########################################################
# Cross Account Access
###########################################################
# This allows engineers from the source_account account
# to assume the infra role in the operations account
# in order to access shared resources.

module "infra_role" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-cross-account-dest?ref=tags/1.0.1"

  iam_role_name   = "infra"
  source_accounts = var.shared_to_accounts
}

locals {
  smtp_param_names = [for p in module.smtp.ssm_parameters :
    "arn:aws:ssm:${local.region}:${local.account_id}:parameter${p.name}"
  ]

  param_read_actions = ["ssm:GetParameter", "ssm:GetParametersByPath", "ssm:GetParameters"]
}

data "aws_iam_policy_document" "shared_infra" {
  statement {
    sid       = "ReadSMTPCreds"
    actions   = local.param_read_actions
    resources = local.smtp_param_names
    effect    = "Allow"
  }

  statement {
    sid     = "ReadAmiSharingParam"
    actions = local.param_read_actions
    resources = [
      "arn:aws:ssm:${local.region}:${local.account_id}:parameter${module.ami_sharing.ssm_parameter}",
    ]
    effect = "Allow"
  }

  statement {
    sid     = "ReadSharedParams"
    actions = local.param_read_actions
    resources = [
      "arn:aws:ssm:${local.region}:${local.account_id}:parameter/shared/*",
      "arn:aws:ssm:${local.region}:${local.account_id}:parameter/shared",
    ]
    effect = "Allow"
  }
  /* Note: 2022-07-03
  I (abel) applied this module and the following statement was removed from this policy.
  But I don't know where this came from, presumably it was click-opsed via the AWS Console UI
  statement {
    actions = [
      "s3:PutBucketPolicy",
      "s3:GetBucketPolicy",
      "s3:DeleteBucketPolicy",
    ]
    effect   = "Allow"
    resource = "arn:aws:s3:::gp-operations-prod-shared-weblite-bucket"
    sid      = "WebliteBucketPolicies"
  }
  */
}

resource "aws_iam_policy" "shared_infra" {
  name        = module.this.id
  description = "allow access to shared ops infrastructure"
  policy      = data.aws_iam_policy_document.shared_infra.json
}

resource "aws_iam_role_policy_attachment" "infra_policy_attachment" {
  role       = module.infra_role.iam_role_name
  policy_arn = aws_iam_policy.shared_infra.id
}
