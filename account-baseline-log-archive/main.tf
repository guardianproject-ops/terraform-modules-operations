locals {
  logging_account_id = data.aws_caller_identity.this.account_id
}

#kdata "aws_region" "current" {}
data "aws_caller_identity" "this" {}

module "audit_logs_key" {
  source                  = "cloudposse/kms-key/aws"
  version                 = "0.12.2"
  context                 = module.this.context
  name                    = "audit-logs"
  deletion_window_in_days = 10
  enable_key_rotation     = true
  alias                   = "alias/audit-logs-key"
  policy                  = data.aws_iam_policy_document.audit_logs_key.json
}

data "aws_iam_policy_document" "audit_logs_key_accounts" {
  for_each = var.account_ids_allowed_to_encrypt
  statement {
    sid       = "AllowEncrypt-${each.key}"
    effect    = "Allow"
    actions   = ["kms:Encrypt", "kms:GenerateDataKey"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${each.value}:root"]
    }
  }
}

data "aws_iam_policy_document" "audit_logs_key" {
  source_policy_documents = [for k, v in data.aws_iam_policy_document.audit_logs_key_accounts : v.json]
  statement {
    sid       = "Enable IAM User Permissions"
    effect    = "Allow"
    actions   = ["kms:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.logging_account_id}:root"]
    }
  }
}

resource "aws_ssm_parameter" "kms_key" {
  name  = "/log-archive/audit-logs-key-arn"
  type  = "String"
  value = module.audit_logs_key.key_arn
  tags  = module.this.tags
}
