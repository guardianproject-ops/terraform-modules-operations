variable "account_ids_allowed_to_encrypt" {
  type        = map(string)
  default     = {}
  description = <<-EOT
A map of account names to account ids that are allowed to encrypt objects with the audit logs key.
EOT
}

#variable "audit_account_id" {
#  type        = string
#  description = "The account id for the AWS Control Tower Audit account"
#}

#variable "governed_regions" {
#  description = "List of AWS regions to enable LandingZone, GuardDuty, etc in"
#  type        = list(string)
#}

#variable "aws_config_rule_identifiers" {
#  type = list(string)
#  default = [
#    "CLOUD_TRAIL_ENABLED",
#    "ENCRYPTED_VOLUMES",
#    "ROOT_ACCOUNT_MFA_ENABLED",
#    "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED"
#  ]
#  description = <<-EOT
#A list of AWS Config Rule Identifiers to enable in the AWS Config service.
#EOT
#}
