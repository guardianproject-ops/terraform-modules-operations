output "audit_logs_key_arn" {
  value       = module.audit_logs_key.key_arn
  description = "Audit Logs Key ARN"
}

output "audit_logs_key_id" {
  value       = module.audit_logs_key.key_id
  description = "Audit Logs Key ID"
}
