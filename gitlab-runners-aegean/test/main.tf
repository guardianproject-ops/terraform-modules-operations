terraform {
  required_version = ">= 1.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 17.7.1"
    }
  }
}

provider "aws" {
}

provider "gitlab" {
  token = var.gitlab_token_guardianproject
  alias = "guardianproject"
}

provider "gitlab" {
  token = var.gitlab_token_guardianproject_ops
  alias = "guardianproject_ops"
}

variable "docker_auth_token" { type = string }
variable "gitlab_token_guardianproject" { type = string }
variable "gitlab_token_guardianproject_ops" { type = string }

module "this" {
  source            = "../"
  namespace         = "gpex"
  tenant            = "agn-ci"
  stage             = "dev"
  label_order       = ["namespace", "environment", "tenant", "stage", "name", "attributes"]
  docker_auth_token = var.docker_auth_token
  providers = {
    gitlab.guardianproject     = gitlab.guardianproject
    gitlab.guardianproject_ops = gitlab.guardianproject_ops
  }
}

output "this" {
  value     = module.this
  sensitive = true
}
