data "aws_caller_identity" "this" {}

data "aws_availability_zones" "this" {
  state = "available"
}
provider "gitlab" {
  alias = "guardianproject"
  token = var.gitlab_token_guardianproject
}

provider "gitlab" {
  alias = "guardianproject_ops"
  token = var.gitlab_token_guardianproject_ops
}

locals {
  enabled                = module.this.enabled
  availability_zones     = sort(slice(data.aws_availability_zones.this.names, 0, 2))
  default_az             = local.availability_zones[0]
  vpc_id                 = module.vpc[0].vpc_id
  private_subnet_ids     = module.subnets[0].private_subnet_ids
  private_subnet_main_id = module.subnets[0].az_private_subnets_map[local.default_az][0]
  ami_name_gitlab_worker = "gitlab-worker-ubuntu-24.04"
  ami_name_gitlab_runner = "gitlab-runner-amazon-linux-2023"
}

module "vpc" {
  source                           = "cloudposse/vpc/aws"
  version                          = "2.2.0"
  count                            = local.enabled ? 1 : 0
  ipv4_primary_cidr_block          = var.vpc_cidr
  assign_generated_ipv6_cidr_block = false
  context                          = module.this.context
  attributes                       = ["vpc"]
}

module "subnets" {
  source                          = "cloudposse/dynamic-subnets/aws"
  version                         = "2.4.2"
  count                           = local.enabled ? 1 : 0
  max_subnet_count                = 2
  availability_zones              = local.availability_zones
  vpc_id                          = local.vpc_id
  igw_id                          = [module.vpc[0].igw_id]
  ipv4_cidr_block                 = [var.subnets_cidr]
  ipv6_enabled                    = false
  ipv4_enabled                    = true
  public_subnets_additional_tags  = { "Visibility" : "Public" }
  private_subnets_additional_tags = { "Visibility" : "Private" }
  metadata_http_endpoint_enabled  = true
  metadata_http_tokens_required   = true
  public_subnets_enabled          = true
  context                         = module.this.context
  attributes                      = ["vpc", "subnet"]
}

module "guardianproject" {
  source                         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-gitlab-runners?ref=v0.0.2"
  context                        = module.this.context
  name                           = "gp"
  docker_auth_token              = var.docker_auth_token
  vpc_id                         = local.vpc_id
  subnet_id                      = local.private_subnet_main_id
  subnet_ids                     = local.private_subnet_ids
  tailscale_client_secret_runner = var.tailscale_client_secret_runner
  tailscale_client_id_runner     = var.tailscale_client_id_runner
  tailscale_tailnet              = var.tailscale_tailnet
  tailscale_tags_runner          = var.tailscale_tags_runner
  tailscale_enabled              = true


  runner_instances = {
    "aegean" = {
      untagged    = false
      tag_list    = ["aegean"]
      runner_type = "group_type"
      group_id    = 724805
    }
  }

  runner_ami_filter = {
    name = [local.ami_name_gitlab_runner]
  }
  worker_ami_filter = {
    name = [local.ami_name_gitlab_worker]
  }
  runner_ami_owners = [data.aws_caller_identity.this.account_id]
  worker_ami_owners = [data.aws_caller_identity.this.account_id]

  providers = {
    gitlab = gitlab.guardianproject
  }
}
module "guardianproject_ops" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-gitlab-runners?ref=v0.0.2"
  context           = module.this.context
  name              = "gp-ops"
  docker_auth_token = var.docker_auth_token
  vpc_id            = local.vpc_id
  subnet_id         = local.private_subnet_main_id
  subnet_ids        = local.private_subnet_ids

  tailscale_enabled = false

  runner_instances = {
    "aegean" = {
      untagged    = false
      tag_list    = ["aegean"]
      runner_type = "group_type"
      group_id    = 3262938
    }
  }

  runner_ami_filter = {
    name = [local.ami_name_gitlab_runner]
  }
  worker_ami_filter = {
    name = [local.ami_name_gitlab_worker]
  }
  runner_ami_owners = [data.aws_caller_identity.this.account_id]
  worker_ami_owners = [data.aws_caller_identity.this.account_id]
  providers = {
    gitlab = gitlab.guardianproject_ops
  }
}
