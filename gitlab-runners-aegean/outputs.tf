output "guardianproject" {
  value = module.guardianproject
}

output "guardianproject_ops" {
  value = module.guardianproject_ops
}
