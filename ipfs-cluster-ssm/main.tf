data "aws_region" "current" {}

locals {
  kms_key_arn    = var.kms_key_arn
  aws_region     = data.aws_region.current.id
  ipfs_cluster_defaults = { 
    ipfs_cluster_server_fqdn            = var.ipfs_cluster_server_fqdn
  }

  ipfs_cluster_params = merge(local.ipfs_cluster_defaults, var.ipfs_cluster_params)
}

module "ssm_prefix" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.2"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  context           = module.this.context
  region            = local.aws_region
  kms_key_arn       = local.kms_key_arn
}

module "label_log_groups_root" {
  source    = "cloudposse/label/null"
  version   = "0.25.0"
  delimiter = "/"
  context   = module.this.context
}
