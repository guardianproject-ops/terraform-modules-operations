output "kms_key_arn" {
  value = local.kms_key_arn
}

output "ssm_prefix_all" {
  value = "/${module.this.id}"
}

output "ssm_prefix_ipfs_cluster" {
  value = local.ssm_prefix_ipfs_cluster
}

output "ssm_prefix_policy_arn" {
  value = module.ssm_prefix.policy_arn
}

output "log_groups_root" {
  value = "/${module.label_log_groups_root.id}"
}

output "log_group_ipfs_cluster_arn" {
  value = aws_cloudwatch_log_group.ipfs_cluster.arn
}

output "log_group_ipfs_cluster_name" {
  value = aws_cloudwatch_log_group.ipfs_cluster.name
}
