module "label_ipfs_cluster" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  context    = module.this.context
  attributes = ["ipfs-cluster"]
  tags       = merge(var.tags, { Application = "ipfs-cluster" })
}

locals {
  ssm_prefix_ipfs_cluster = "${module.ssm_prefix.full_prefix}/ipfs-cluster"
}

resource "aws_ssm_parameter" "ipfs_cluster_params" {
  for_each = local.ipfs_cluster_params

  name      = "${local.ssm_prefix_ipfs_cluster}/${each.key}"
  value     = each.value
  type      = "SecureString"
  overwrite = true
  tags      = module.this.tags
}

module "label_log_ipfs_cluster" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.25.0"
  delimiter = "/"
  context   = module.label_ipfs_cluster.context
}

resource "aws_cloudwatch_log_group" "ipfs_cluster" {
  name              = "/${module.label_log_ipfs_cluster.id}"
  retention_in_days = var.log_retention_days
}
