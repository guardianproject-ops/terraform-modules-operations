variable "kms_key_arn" {
  type = string
}

variable "ipfs_cluster_params" {
  type = map(string)
  default = {}
}

variable "ipfs_cluster_server_fqdn" {
  type        = string
  description = "The public facing domain where clients will access the server (e.g. ipfs-cluster.example.com)"
}

variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}


