terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "baseline_account" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-baseline//modules/account?ref=main"
  #source                            = "/home/abel/src/guardianproject-ops/terraform-aws-account-baseline//modules/account"

  context                           = module.this.context
  aws_iam_account_alias             = var.aws_iam_account_alias
  aws_s3_public_access_block_config = var.aws_s3_public_access_block_config
}
