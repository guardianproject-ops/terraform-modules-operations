variable "aws_iam_account_alias" {
  type        = string
  default     = null
  description = "The alias for the AWS account"
}

variable "aws_s3_public_access_block_config" {
  type = object({
    enabled                 = optional(bool, true)
    block_public_acls       = optional(bool, true)
    block_public_policy     = optional(bool, true)
    ignore_public_acls      = optional(bool, true)
    restrict_public_buckets = optional(bool, true)
  })
  default     = {}
  description = "S3 bucket-level Public Access Block config"
}
