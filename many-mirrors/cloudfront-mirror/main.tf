
resource "aws_cloudfront_distribution" "this" {
  comment         = "CDN for ${var.origin_domain}"
  enabled         = true
  is_ipv6_enabled = true
  aliases         = var.aliases

  lifecycle {
    prevent_destroy = true
  }

  origin {
    domain_name         = var.origin_domain
    origin_id           = "Custom-${var.origin_domain}"
    connection_attempts = 3
    connection_timeout  = 10

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "match-viewer"
      origin_ssl_protocols     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    dynamic "custom_header" {
      for_each = var.bypass_token == null ? [] : [0]
      content {
        name  = "Bypass-Rate-Limit-Token"
        value = var.bypass_token
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "Custom-${var.origin_domain}"
    viewer_protocol_policy = "redirect-to-https"
    # CachingDisabled managed AWS policy
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html
    cache_policy_id = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"

    # AllViewerExceptHostHeader managed AWS policy
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-origin-request-policies.html#managed-origin-request-policy-all-viewer-except-host-header
    origin_request_policy_id = "b689b0a8-53d0-40ab-baf2-68738e2966ac"

  }

  viewer_certificate {
    cloudfront_default_certificate = var.acm_certificate_arn == null
    acm_certificate_arn            = var.acm_certificate_arn
    ssl_support_method             = var.acm_certificate_arn == null ? null : "sni-only"
    minimum_protocol_version       = "TLSv1"
  }

  dynamic "logging_config" {
    for_each = var.logging_bucket == null ? [] : [1]
    content {
      bucket          = var.logging_bucket
      include_cookies = false
    }
  }

  tags = module.this.tags
}

resource "aws_cloudwatch_metric_alarm" "high_bandwidth" {
  alarm_name          = "bandwidth-out-high-${aws_cloudfront_distribution.this.id}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "BytesDownloaded"
  namespace           = "AWS/CloudFront"
  period              = "3600"
  statistic           = "Sum"
  threshold           = var.max_transfer_per_hour
  alarm_description   = "Alerts when bandwidth out exceeds specified threshold in an hour"
  actions_enabled     = "true"
  alarm_actions       = [var.sns_topic_arn]
  dimensions = {
    DistributionId = aws_cloudfront_distribution.this.id
    Region         = "Global"
  }

  tags = module.this.tags
}

resource "aws_cloudwatch_metric_alarm" "low_bandwidth" {
  count               = var.low_bandwidth_alarm ? 1 : 0
  alarm_name          = "bandwidth-out-low-${aws_cloudfront_distribution.this.id}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "6"
  metric_name         = "BytesDownloaded"
  namespace           = "AWS/CloudFront"
  period              = "3600"
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "Alerts when bandwidth out is zero for six hours"
  actions_enabled     = "true"
  alarm_actions       = [var.sns_topic_arn]
  treat_missing_data  = "breaching"
  dimensions = {
    DistributionId = aws_cloudfront_distribution.this.id
    Region         = "Global"
  }

  tags = module.this.tags
}

