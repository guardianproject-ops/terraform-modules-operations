variable "kms_key_arn" {
  type        = string
  description = "The kms key ARN used for various purposes throughout the deployment"
}

variable "origin_domain" {
  type        = string
  description = "The URL for the origin server"
}

variable "mirrors" {
  description = "A simple map of names representing the mirrors, the names themselves are not important, but they must be unique."
  type = map(object({
    name    = string
    aliases = optional(list(string))
  }))
}

variable "cloudflare_edit_token" {
  type = string
}

variable "primary_domain" {
  type        = string
  description = "The main domain for which the cert will be issued (all other domains are SANs). "
}

variable "alias_domains" {
  type = map(object({
    domain_name        = string
    cloudflare_zone_id = string
  }))

}
