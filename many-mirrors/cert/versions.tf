terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = ">= 4.0, < 5.0"
    }
    aws = {
      source = "hashicorp/aws"
    }
  }
  required_version = ">= 1.3"
}
