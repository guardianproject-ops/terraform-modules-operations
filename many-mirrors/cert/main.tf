locals {
  primary_domain_info = [for name, info in var.domains : info if name == var.primary_domain][0]
  sans_names          = [for name, info in var.domains : name if name != var.primary_domain]
}
resource "aws_acm_certificate" "cert" {
  domain_name               = local.primary_domain_info.domain_name
  subject_alternative_names = local.sans_names
  validation_method         = "DNS"
  lifecycle {
    create_before_destroy = true
  }
  tags = module.this.tags
}

locals {
  aws_domain_validation_options = { for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
    name               = dvo.resource_record_name
    record             = dvo.resource_record_value
    type               = dvo.resource_record_type
    domain_name        = dvo.domain_name
    cloudflare_zone_id = [for name, info in var.domains : info.cloudflare_zone_id if dvo.domain_name == name][0]
    }
  }
  domain_validation_options = var.is_already_validated ? {} : local.aws_domain_validation_options
  validated_hostnames       = var.is_already_validated ? var.validated_hostnames : [for record in cloudflare_record.acm : record.hostname]
}

resource "cloudflare_record" "acm" {
  for_each        = local.domain_validation_options
  zone_id         = each.value.cloudflare_zone_id
  name            = each.value.name
  content         = each.value.record
  type            = each.value.type
  allow_overwrite = true
  comment         = "ACM validation string for ${each.value.domain_name} as a part of ${module.this.id}"
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = local.validated_hostnames
}
