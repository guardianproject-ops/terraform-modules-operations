variable "validated_hostnames" {
  #type    = list(string)
  default = []
}

variable "is_already_validated" {
  type    = bool
  default = false
}

variable "primary_domain" {
  type        = string
  description = "The main domain for which the cert will be issued (all other domains are SANs). "
}

variable "domains" {
  description = "The main domain and SANs domains"
  type = map(object({
    domain_name        = string
    cloudflare_zone_id = string
  }))
}
