resource "aws_sns_topic" "mirrors" {
  name = module.this.id
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

locals {
  mirrors_with_aliases = { for k, v in var.mirrors : k => v if length(v.aliases == null ? [] : v.aliases) > 0 }
  cname_records = { for k, cert in module.certs.validation_options : k =>
    {
      cloudflare_zone_id = cert.cloudflare_zone_id
      domain_name        = cert.domain_name
      cloudfront_distribution_domain = [for name, mirror in module.proxy : mirror.domain_name
        if contains(mirror.aliases, cert.domain_name)
      ][0]

  } }
}

# we create one ACM cert with all the domains
module "certs" {
  source         = "./cert"
  domains        = var.alias_domains
  primary_domain = var.primary_domain
  context        = module.this.context

  providers = {
    aws = aws.us_east_1
  }
}

module "proxy" {
  for_each            = var.mirrors
  source              = "./cloudfront-mirror"
  context             = module.this.context
  origin_domain       = var.origin_domain
  low_bandwidth_alarm = false
  sns_topic_arn       = aws_sns_topic.mirrors.arn
  aliases             = each.value.aliases
  acm_certificate_arn = each.value.aliases != null ? module.certs.acm_cert_arn : null
}


resource "cloudflare_record" "primary" {
  for_each = local.cname_records
  zone_id  = each.value.cloudflare_zone_id
  name     = each.value.domain_name
  content  = each.value.cloudfront_distribution_domain
  type     = "CNAME"
  comment  = "Cloudfront mirror ${each.value.domain_name} as a part of ${module.this.id}"
}
