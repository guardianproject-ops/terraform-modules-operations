output "mirrors" {
  value = module.proxy
}

output "cert_id" {
  value = module.certs.acm_cert_id
}

output "cert_arn" {
  value = module.certs.acm_cert_arn
}

output "alias_cnames" {
  value = local.cname_records
}
