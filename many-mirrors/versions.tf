terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.0, < 5.0"
    }
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1, aws.gp_operations]
    }
  }
  required_version = ">= 1.3"
}
