locals {
  secret_prefix = "${module.this.id}/ssh_*"
}

module "ssm_setup" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-ansible-setup?ref=tags/3.2.0"

  context       = module.this.context
  force_destroy = !var.is_prod_like
}

module "rotate_ssh" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-lambda-secrets-manager-ssh-key-rotation.git?ref=tags/3.1.0"

  context               = module.this.context
  attributes            = ["ssh-key-rotate"]
  server_username       = "admin"
  tag_name              = "RotateSSHKeys"
  tag_value             = "true"
  secret_prefix         = local.secret_prefix
  attach_network_policy = true
  vpc_id                = var.vpc_id
  vpc_subnet_ids        = var.vpc_subnet_ids
  hash_extra            = var.ssh_lambda_hash_extra

  # enable for debugging
  # lambda_log_level       = "DEBUG"
  # lambda_timeout_seconds = 120
}

resource "aws_secretsmanager_secret" "ssh_key" {
  name        = "${module.this.id}/ssh_key"
  description = "SSH Key used by all instances in this deployment"

  depends_on = [
    module.rotate_ssh,
    aws_ssm_parameter.ssh_rotate_lambda_security_group_id
  ]
}

resource "aws_secretsmanager_secret_rotation" "ssh_key" {
  secret_id           = aws_secretsmanager_secret.ssh_key.id
  rotation_lambda_arn = module.rotate_ssh.lambda_arn

  rotation_rules {
    automatically_after_days = var.ssh_key_rotate_after_days
  }

  depends_on = [
    module.rotate_ssh,
    aws_ssm_parameter.ssh_rotate_lambda_security_group_id
  ]
}

module "label_log" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  name       = var.label_log_name == "" ? var.name : var.label_log_name
  attributes = ["journald"]
  delimiter  = "/"
  context    = module.this.context
}
resource "aws_cloudwatch_log_group" "journald" {
  name              = "/${module.label_log.id}"
  retention_in_days = var.log_retention_days
}

module "kms_key" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.9.0"
  context                 = module.this.context
  description             = "general purpose KMS key for this deployment"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  alias                   = "alias/${module.this.id}"
}

resource "aws_ssm_parameter" "kms_key" {
  name  = "/region-settings/kms_key_arn"
  type  = "String"
  value = module.kms_key.key_arn
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "ssh_key" {
  name  = "/region-settings/secretsmanager_ssh_key_secret"
  type  = "String"
  value = aws_secretsmanager_secret.ssh_key.id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "ssh_rotate_lambda_security_group_id" {
  name  = "/region-settings/ssh_rotate_lambda_security_group_id"
  type  = "String"
  value = module.rotate_ssh.security_group_id
  tags  = module.this.tags
}
