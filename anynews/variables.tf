variable "feeds_json" {
  description = "(Required) A string containing the json description of feeds served by this deployment"
  type        = string
}

variable "gitlab_project" {
  description = "(Required) The anynews gitlab project that will build and push images to AWS ECR"
  type        = string
}
variable "gitlab_access_token" {
  description = "(Required) The gitlab token used to set variables on CI pipelines"
  type        = string
  sensitive   = true
}