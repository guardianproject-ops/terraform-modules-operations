terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "aws_region" "current" {}
data "aws_caller_identity" "this" {}
resource "aws_ecr_repository" "this" {
  name                 = module.this.id
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = module.this.tags
}

locals {
  ecr_address    = coalesce(format("%v.dkr.ecr.%v.amazonaws.com", data.aws_caller_identity.this.account_id, data.aws_region.current.name))
  ecr_repo       = aws_ecr_repository.this.id
  image_tag      = "latest"
  ecr_fqrn = format("%v/%v", local.ecr_address, local.ecr_repo)
  ecr_image_name = format("%v/%v:%v", local.ecr_address, local.ecr_repo, local.image_tag)
}

resource "aws_iam_user" "deploy_user" {
  name = "${module.this.id}-deploy"
  tags = module.this.tags
}

resource "aws_iam_access_key" "deploy_user_key_v4" {
  user   = aws_iam_user.deploy_user.name
  status = "Active"
}

data "aws_iam_policy_document" "deploy_rw" {
  statement {
    sid    = "AllowEcrGetAuthToken"
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
    ]

    resources = ["*"]
  }
  statement {
    sid    = "AllowEcrPushPull"
    effect = "Allow"
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage",
    ]
    resources = [
      "${aws_ecr_repository.this.arn}/*"
    ]
  }
}

resource "aws_iam_user_policy" "deploy_rw" {
  name   = "AllowIamUserECRPushPull-${module.this.id}"
  user   = aws_iam_user.deploy_user.name
  policy = data.aws_iam_policy_document.deploy_rw.json
}

resource "gitlab_project_variable" "aws_default_region" {
  project   = var.gitlab_project
  key       = "AWS_DEFAULT_REGION"
  value     = "eu-west-1"
  protected = false
}

resource "gitlab_project_variable" "aws_region" {
  project   = var.gitlab_project
  key       = "AWS_REGION"
  value     = "eu-west-1"
  protected = false
}

resource "gitlab_project_variable" "aws_access_key" {
  project   = var.gitlab_project
  key       = "AWS_ACCESS_KEY_ID"
  value     = aws_iam_access_key.deploy_user_key_v4.id
  protected = true
  masked    = true
}

resource "gitlab_project_variable" "aws_access_secret" {
  project   = var.gitlab_project
  key       = "AWS_SECRET_ACCESS_KEY"
  value     = aws_iam_access_key.deploy_user_key_v4.secret
  protected = true
  masked    = true
}

resource "gitlab_project_variable" "ecr_repo" {
  project   = var.gitlab_project
  key       = "ECR_REPO"
  value     = local.ecr_fqrn
  protected = true
  masked    = false
}

module "anynews" {
  //source     = "git::https://gitlab.com/guardianproject/anynews/terraform-aws-anynews-republisher.git?ref=main"
  source                          = "/workspace/guardianproject/anynews/terraform-aws-anynews-republisher"
  feeds_json                      = var.feeds_json
  republisher_container_image_uri = local.ecr_image_name
  context                         = module.this.context
}