output "lambda" {
  value = module.anynews
}

output "ecr_image_name" {
  value = local.ecr_image_name
}