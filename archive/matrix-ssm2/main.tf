data "aws_region" "current" {}

locals {
  kms_key_arn    = var.kms_key_arn
  aws_region     = data.aws_region.current.id
  ssm_prefix_shared = "${module.ssm_prefix.full_prefix}/shared"
  ssm_prefix_synapse = "${module.ssm_prefix.full_prefix}/synapse"
  ssm_prefix_ma1sd = "${module.ssm_prefix.full_prefix}/ma1sd"
  ssm_prefix_sygnal = "${module.ssm_prefix.full_prefix}/sygnal"
}

module "ssm_prefix" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.2"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  context           = module.this.context
  region            = local.aws_region
  kms_key_arn       = local.kms_key_arn
}

module "label_log_groups_root" {
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.this.context
}

// SHARED

resource "aws_ssm_parameter" "db_address" {
  name      = "${local.ssm_prefix_shared}/db_address"
  value     = var.db_address
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "db_port" {
  name      = "${local.ssm_prefix_shared}/db_port"
  value     = var.db_port
  type      = "SecureString"
  overwrite = true
}

// SYNAPSE
module "label_synapse" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["synapse"]
  tags       = merge(var.tags, { Application = "synapse" })
}

module "label_log_synapse" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_synapse.context
}

resource "aws_cloudwatch_log_group" "synapse" {
  name              = "/${module.label_log_synapse.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "synapse__log_group_synapse" {
  name      = "${local.ssm_prefix_synapse}/log_group_synapse"
  value     = aws_cloudwatch_log_group.synapse.name
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__s3_media_store_bucket_name" {
  name      = "${local.ssm_prefix_synapse}/s3_media_store_bucket_name"
  value = var.s3_media_store_bucket_name
  type      = "SecureString"
  overwrite = true
}

/// MA1SD

module "label_ma1sd" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["ma1sd"]
  tags       = merge(var.tags, { Application = "ma1sd" })
}

module "label_log_ma1sd" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_ma1sd.context
}

resource "aws_cloudwatch_log_group" "ma1sd" {
  name              = "/${module.label_log_ma1sd.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "ma1sd__log_group_ma1sd" {
  name      = "${local.ssm_prefix_ma1sd}/log_group_ma1sd"
  value     = aws_cloudwatch_log_group.ma1sd.name
  type      = "SecureString"
  overwrite = true
}

// SYGNAL

module "label_sygnal" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["sygnal"]
  tags       = merge(var.tags, { Application = "sygnal" })
}

module "label_log_sygnal" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_sygnal.context
}

resource "aws_cloudwatch_log_group" "sygnal" {
  name              = "/${module.label_log_sygnal.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "sygnal__log_group_sygnal" {
  name      = "${local.ssm_prefix_sygnal}/log_group_sygnal"
  value     = aws_cloudwatch_log_group.sygnal.name
  type      = "SecureString"
  overwrite = true
}