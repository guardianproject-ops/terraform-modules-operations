variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "kms_key_arn" {
  type = string
}

variable "db_address" {
  type = string
}

variable "db_port" {
  type = number
}

variable "s3_media_store_bucket_name" {
  type = string
}