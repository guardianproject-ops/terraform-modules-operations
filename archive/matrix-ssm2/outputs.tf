output "kms_key_arn" {
  value = local.kms_key_arn
}

output "ssm_prefix_all" {
  value = "/${module.this.id}"
}

output "ssm_prefix_shared" {
  value = local.ssm_prefix_sygnal
}
output "ssm_prefix_sygnal" {
  value = local.ssm_prefix_sygnal
}
output "ssm_prefix_synapse" {
  value = local.ssm_prefix_synapse
}
output "ssm_prefix_ma1sd" {
  value = local.ssm_prefix_ma1sd
}
output "ssm_prefix_policy_arn" {
  value = module.ssm_prefix.policy_arn
}

output "log_groups_root" {
  value = "/${module.label_log_groups_root.id}"
}
output "log_group_synapse_arn" {
  value = aws_cloudwatch_log_group.synapse.arn
}

output "log_group_synapse_name" {
  value = aws_cloudwatch_log_group.synapse.name
}

output "log_group_sygnal_arn" {
  value = aws_cloudwatch_log_group.sygnal.arn
}

output "log_group_sygnal_name" {
  value = aws_cloudwatch_log_group.sygnal.name
}

output "log_group_ma1sd_arn" {
  value = aws_cloudwatch_log_group.ma1sd.arn
}

output "log_group_ma1sd_name" {
  value = aws_cloudwatch_log_group.ma1sd.name
}
