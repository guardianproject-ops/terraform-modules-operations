variable "web_domain" {
  type = string
}

variable "weblite_domain" {
  type = string
}

variable "matrix_server_fqdn" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_edit_token" {
  type = string
}
