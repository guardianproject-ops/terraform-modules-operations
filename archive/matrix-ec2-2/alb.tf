locals {
  target_groups_defaults = {
    cookie_duration                  = 86400
    deregistration_delay             = 300
    health_check_interval            = 10
    health_check_healthy_threshold   = 3
    health_check_path                = "/"
    health_check_port                = "traffic-port"
    health_check_timeout             = 5
    health_check_unhealthy_threshold = 3
    health_check_matcher             = "200-299"
    stickiness_enabled               = true
    target_type                      = "instance"
    slow_start                       = 0
  }
}

module "label_alb" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.24.1"
  attributes = ["loadbalancer"]
  context    = module.this.context
}

locals {
  instance_id = module.instance.id
}


resource "aws_security_group" "public_load_balancer" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = var.federation_port
    to_port     = var.federation_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_alb.tags
}

resource "aws_lb" "application" {
  # aws lb names are limited to 32 chars
  name                             = substr(module.label.id, 0, min(length(module.label.id), 32))
  load_balancer_type               = "application"
  internal                         = false
  security_groups                  = [aws_security_group.public_load_balancer.id]
  subnets                          = var.alb_public_subnet_ids
  idle_timeout                     = "60"
  enable_cross_zone_load_balancing = false
  enable_deletion_protection       = var.is_prod_like
  enable_http2                     = true
  ip_address_type                  = "ipv4"

  access_logs {
    enabled = true
    bucket  = aws_s3_bucket.log_bucket.id
    prefix  = module.label_alb.id
  }

  timeouts {
    create = "10m"
    delete = "10m"
    update = "10m"
  }

  tags = module.label.tags
  depends_on = [
    aws_s3_bucket_policy.alb_log_bucket
  ]
}

resource "aws_lb_target_group" "synapse" {
  name                 = "synapse"
  vpc_id               = var.vpc_id
  port                 = var.synapse_client_port
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = "/health"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "synapse")
    },
  )
}

resource "aws_lb_target_group" "synapse_federation" {
  count                = var.synapse_federation_enabled ? 1 : 0
  name                 = "synapse-federation"
  vpc_id               = var.vpc_id
  port                 = var.synapse_federation_port
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = "/_matrix/federation/v1/version"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.label.id,
        module.this.delimiter,
        "synapse_federation",
      )
    },
  )
}

resource "aws_lb_target_group" "sygnal" {
  name                 = "sygnal"
  vpc_id               = var.vpc_id
  port                 = var.sygnal_port
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    interval            = local.target_groups_defaults["health_check_interval"]
    path                = local.target_groups_defaults["health_check_path"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "sygnal")
    },
  )
}

resource "aws_lb_target_group" "ma1sd" {
  name                 = "ma1sd"
  vpc_id               = var.vpc_id
  port                 = var.ma1sd_port
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    interval            = local.target_groups_defaults["health_check_interval"]
    path                = "/_matrix/identity/api/v1"
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = local.target_groups_defaults["cookie_duration"]
    enabled         = local.target_groups_defaults["stickiness_enabled"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "ma1sd")
    },
  )
}

resource "aws_lb_listener" "frontend_https" {
  load_balancer_arn = aws_lb.application.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.synapse_acm_tls_certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.synapse.id
    type             = "forward"
  }
}

resource "aws_lb_listener" "frontend_federation_https" {
  count             = var.synapse_federation_enabled ? 1 : 0
  load_balancer_arn = aws_lb.application.arn
  port              = var.synapse_federation_port
  protocol          = "HTTPS"
  certificate_arn   = var.synapse_acm_tls_certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.synapse_federation[0].id
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "ma1sd" {
  listener_arn = aws_lb_listener.frontend_https.arn
  priority     = 50

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ma1sd.arn
  }

  condition {
    path_pattern {
      values = ["/_matrix/identity*"]
    }
  }
}

resource "aws_lb_listener_rule" "ma1sd_directory_search" {
  listener_arn = aws_lb_listener.frontend_https.arn
  priority     = 55

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ma1sd.arn
  }

  condition {
    path_pattern {
      values = ["/_matrix/client/r0/user_directory*"]
    }
  }
}

resource "aws_lb_listener_rule" "sygnal_host" {
  listener_arn = aws_lb_listener.frontend_https.arn
  priority     = 60

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.sygnal.arn
  }

  condition {
    host_header {
      values = ["push.${var.matrix_server_fqdn}"]
    }
  }
}

resource "aws_lb_listener_rule" "sygnal_path" {
  listener_arn = aws_lb_listener.frontend_https.arn
  priority     = 65

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.sygnal.arn
  }

  condition {

    path_pattern {
      values = ["/_matrix/push*"]
    }
  }
}

resource "aws_lb_listener_rule" "synapse" {
  listener_arn = aws_lb_listener.frontend_https.arn
  priority     = 70

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.synapse.arn
  }

  condition {
    path_pattern {
      values = ["/_matrix/*"]
    }
  }
}

resource "aws_lb_listener_rule" "synapse_federation" {
  count        = var.synapse_federation_enabled ? 1 : 0
  listener_arn = aws_lb_listener.frontend_federation_https[0].arn
  priority     = 70

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.synapse_federation[0].arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }
}

resource "aws_lb_target_group_attachment" "synapse" {
  target_group_arn = aws_lb_target_group.synapse.arn
  target_id        = local.instance_id
}

resource "aws_lb_target_group_attachment" "synapse_federation" {
  count            = var.synapse_federation_enabled ? 1 : 0
  target_group_arn = aws_lb_target_group.synapse_federation[0].arn
  target_id        = local.instance_id
}

resource "aws_lb_target_group_attachment" "sygnal" {
  target_group_arn = aws_lb_target_group.sygnal.arn
  target_id        = local.instance_id
}

resource "aws_lb_target_group_attachment" "ma1sd" {
  target_group_arn = aws_lb_target_group.ma1sd.arn
  target_id        = local.instance_id
}

resource "cloudflare_record" "primary" {
  zone_id = var.cloudflare_zone_id
  name    = var.matrix_server_fqdn
  value   = aws_lb.application.dns_name
  type    = "CNAME"
  ttl     = 1800
}

resource "cloudflare_record" "push" {
  zone_id = var.cloudflare_zone_id
  name    = "push.${var.matrix_server_fqdn}"
  value   = aws_lb.application.dns_name
  type    = "CNAME"
  ttl     = 1800
}

resource "cloudflare_record" "federation" {
  count   = !var.prefer_well_known_over_dns_srv && var.synapse_federation_enabled ? 1 : 0
  zone_id = var.cloudflare_zone_id
  name    = "_matrix._tcp"
  type    = "SRV"
  ttl     = 1800
  data {
    service  = "_matrix"
    proto    = "_tcp"
    name     = var.matrix_domain
    priority = 10
    weight   = 0
    port     = var.synapse_federation_port
    target   = var.matrix_server_fqdn
  }
}

resource "cloudflare_record" "identity" {
  count   = !var.prefer_well_known_over_dns_srv && var.synapse_federation_enabled ? 1 : 0
  zone_id = var.cloudflare_zone_id
  name    = "_matrix-identity._tcp"
  type    = "SRV"
  ttl     = 1800
  data {
    service  = "_matrix-identity"
    proto    = "_tcp"
    name     = var.matrix_domain
    priority = 10
    weight   = 0
    port     = 443
    target   = var.matrix_server_fqdn
  }
}

module "label_waf" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.24.1"
  attributes = ["waf"]
  context    = module.this.context
}

resource "aws_wafv2_regex_pattern_set" "msie" {
  name        = module.label_waf.id
  description = "Matches MSIE browser user-agent header"
  scope       = "REGIONAL"
  regular_expression {
    // source: https://stackoverflow.com/a/19987665
    regex_string = "\\bMSIE.*;.*\\bTrident"
  }
  tags = module.label_waf.tags
}

resource "aws_wafv2_web_acl" "main" {
  name        = module.label_waf.id
  description = "WAF for matrix services"
  scope       = "REGIONAL"

  default_action {
    allow {}
  }
  rule {
    name     = "BlockMSIEUserAgentRule"
    priority = 1

    action {
      block {}
    }

    statement {
      regex_pattern_set_reference_statement {
        arn = aws_wafv2_regex_pattern_set.msie.arn

        field_to_match {
          single_header {
            name = "user-agent"
          }
        }
        text_transformation {
          priority = 2
          type     = "NONE"
        }
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = false
      metric_name                = "Matrix-Block-MSIE-Browser"
      sampled_requests_enabled   = false
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = false
    metric_name                = "Matrix-WAF-ACL"
    sampled_requests_enabled   = false
  }
  tags = module.label_waf.tags
}


resource "aws_wafv2_web_acl_association" "main" {
  resource_arn = aws_lb.application.arn
  web_acl_arn  = aws_wafv2_web_acl.main.arn
}