provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

module "label" {
  source  = "cloudposse/label/null"
  version = "0.24.1"
  context = module.this.context
  name    = "matrix"
  tags    = merge(module.this.tags, { Application = "matrix" })
}

data "aws_iam_policy_document" "read_acm_cert" {
  statement {
    sid = "AllowListACMCert"
    actions = [
      "acm:ListCertificates",
      "acm:ListTagsForCertificate"
    ]

    resources = [
      "*"
    ]
  }
  statement {
    sid = "AllowReadACMCert"
    actions = [
      "acm:GetCertificate",
      "acm:DescribeCertificate"
    ]

    resources = [
      var.synapse_acm_tls_certificate_arn
    ]
  }
}

resource "aws_iam_policy" "read_acm_cert" {
  name        = "read-acm-cert-${module.this.id}"
  description = "Allows instance to read public ACM certificate"
  policy      = data.aws_iam_policy_document.read_acm_cert.json
}

data "aws_iam_policy_document" "s3_media_rw" {
  statement {
    sid = "AllowBucketList"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      var.s3_media_bucket_arn
    ]
  }
  statement {
    sid = "AllowBucketRW"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${var.s3_media_bucket_arn}/*"
    ]
  }
  # don't forget this one!
  # our bucket has server side encryption enabled by default
  # but to allow the client to request a file be encrypted ( on the server )
  # we need this statement
  # it's not the same as action kms:Encrypt
  statement {
    sid = "AllowClientSideEncryption"
    actions = [
      "kms:GenerateDataKey"
    ]
    resources = [var.kms_key_arn]

  }
}

resource "aws_iam_policy" "s3_media_rw" {
  name        = "media-bucket-rw-${module.this.id}"
  description = "Alows instance to read and write to s3 media bucket"
  policy      = data.aws_iam_policy_document.s3_media_rw.json
}

module "instance" {
  source                        = "../ec2-instance2"
  is_prod_like                  = var.is_prod_like
  ssm_prefix                    = var.ssm_prefix
  ssm_prefix_policy_arn         = var.ssm_prefix_policy_arn
  kms_key_arn                   = var.kms_key_arn
  vpc_id                        = var.vpc_id
  vpc_cidr_block                = var.vpc_cidr_block
  availability_zone_1           = var.availability_zone_1
  subnet_id                     = var.subnet_id
  disk_allocation_gb            = var.disk_allocation_gb
  instance_type                 = var.instance_type
  log_groups_root               = var.log_groups_root
  enable_ebs_volume             = true
  ebs_volume_disk_allocation_gb = var.ebs_volume_disk_allocation_gb
  ebs_volume_mount_path         = "/var/lib/matrix"
  secure_metadata_endpoint      = var.secure_metadata_endpoint

  extra_iam_policy_arns = [
    aws_iam_policy.read_acm_cert.arn,
    aws_iam_policy.s3_media_rw.arn
  ]
  ingress = {
    "synapse-client" = {
      from_port = var.synapse_client_port,
      to_port   = var.synapse_client_port,
      protocol  = "tcp"
    }
    "synapse-federation" = {
      from_port = var.synapse_federation_port,
      to_port   = var.synapse_federation_port,
      protocol  = "tcp"
    }
    "sygnal" = {
      from_port = var.sygnal_port,
      to_port   = var.sygnal_port,
      protocol  = "tcp"
    }
    "ma1sd" = {
      from_port = var.ma1sd_port,
      to_port   = var.ma1sd_port,
      protocol  = "tcp"
    }
    "synapse-backend-metrics" = {
      # TODO the real ports
      from_port = 9395,
      to_port   = 9395,
      protocol  = "tcp"
    }
  }
  context = module.label.context
}

