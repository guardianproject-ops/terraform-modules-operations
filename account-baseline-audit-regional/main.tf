terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "baseline_region" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-baseline-audit//modules/iam-access-analzyer-region?ref=main"
  #source  = "/home/abel/src/guardianproject-ops/terraform-aws-account-baseline-audit//modules/iam-access-analzyer-region"
  context = module.this.context
}

#resource "aws_sns_topic_subscription" "aws_config" {
#  for_each               = var.aws_config_sns_subscription
#  endpoint               = each.value.endpoint
#  endpoint_auto_confirms = length(regexall("http", each.value.protocol)) > 0
#  protocol               = each.value.protocol
#  topic_arn              = "arn:aws:sns:${data.aws_region.current.name}:${var.control_tower_account_ids.audit}:aws-controltower-AggregateSecurityNotifications"
#
#  provider = aws.audit
#}
