output "kms_key" {
  value = module.kms_key
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}

output "bucket_name" {
  value = aws_s3_bucket.default.id
}

output "bucket_arn" {
  value = aws_s3_bucket.default.arn
}
