variable "archived_projects" {
  description = "Projects stored in this archive. They can be safely removed after their expiration. Does not create the folder."
  type = map(object({
    path               = string
    expiration_enabled = bool
    project_end_date   = string
    expiration_date    = string
    standard_days      = number
    glacier_days       = number
  }))
}
