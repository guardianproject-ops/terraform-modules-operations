data "aws_region" "default" {}
data "aws_caller_identity" "default" {}
data "aws_partition" "current" {}
locals {
  region     = data.aws_region.default.name
  account_id = data.aws_caller_identity.default.account_id
  partition  = data.aws_partition.current.partition
  all_archived_project_paths = [
    for k, project in var.archived_projects : project.path
  ]
  all_archived_project_paths_star_with_arn = [
    for path in local.all_archived_project_paths : "arn:${local.partition}:s3:::${aws_s3_bucket.default.id}/${path}/*"
  ]

  all_archived_project_paths_with_arn = [
    for path in local.all_archived_project_paths : "arn:${local.partition}:s3:::${aws_s3_bucket.default.id}/${path}"
  ]
}

module "kms_key" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.9.0"
  context                 = module.this.context
  description             = "KMS encryption key for archive bucket"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  alias                   = "alias/${module.this.id}"
}

resource "aws_s3_bucket" "default" {
  bucket        = module.this.id
  acl           = "private"
  force_destroy = false

  tags = module.this.tags

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = module.kms_key.key_arn

      }
    }
  }


  dynamic "lifecycle_rule" {
    for_each = var.archived_projects
    content {
      id      = lifecycle_rule.key
      enabled = lifecycle_rule.value.expiration_enabled

      prefix = "${lifecycle_rule.value.path}/"

      transition {
        date          = formatdate("YYYY-MM-DD", timeadd("${lifecycle_rule.value.project_end_date}T00:00:00Z", "${lifecycle_rule.value.standard_days * 24}h"))
        storage_class = "STANDARD_IA"
      }

      transition {
        date          = formatdate("YYYY-MM-DD", timeadd("${lifecycle_rule.value.project_end_date}T00:00:00Z", "${lifecycle_rule.value.glacier_days * 24}h"))
        storage_class = "GLACIER"
      }

      expiration {
        date = lifecycle_rule.value.expiration_date
      }
    }
  }

}

# this policy
# * denys un-encrypted objects to be stored in it
# * denys access to objects outside the defined archived projects
data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid           = "DenyUnknownProjectPath"
    effect        = "Deny"
    actions       = ["s3:PutObject", "s3:GetObject"]
    not_resources = concat(local.all_archived_project_paths_with_arn, local.all_archived_project_paths_star_with_arn)

    principals {
      identifiers = ["*"]
      type        = "*"
    }
  }
  statement {
    sid       = "DenyIncorrectEncryptionHeader"
    effect    = "Deny"
    actions   = ["s3:PutObject"]
    resources = ["arn:${local.partition}:s3:::${aws_s3_bucket.default.id}/*"]

    principals {
      identifiers = ["*"]
      type        = "*"
    }

    condition {
      test     = "StringNotEquals"
      values   = ["aws:kms"]
      variable = "s3:x-amz-server-side-encryption"
    }
  }

  statement {
    sid       = "DenyUnEncryptedObjectUploads"
    effect    = "Deny"
    actions   = ["s3:PutObject"]
    resources = ["arn:${local.partition}:s3:::${aws_s3_bucket.default.id}/*"]

    principals {
      identifiers = ["*"]
      type        = "*"
    }

    condition {
      test     = "Null"
      values   = ["true"]
      variable = "s3:x-amz-server-side-encryption"
    }
  }
}


resource "aws_s3_bucket_policy" "default" {
  bucket     = aws_s3_bucket.default.id
  policy     = data.aws_iam_policy_document.bucket_policy.json
  depends_on = [aws_s3_bucket_public_access_block.default]
}

# Refer to the terraform documentation on s3_bucket_public_access_block at
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block
# for the nuances of the blocking options
resource "aws_s3_bucket_public_access_block" "default" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.default.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "time_sleep" "wait_30_seconds" {
  # we need to wait a little while for the policy to apply
  # before attempting to write a file
  depends_on      = [aws_s3_bucket_policy.default]
  create_duration = "30s"
}

resource "aws_s3_bucket_object" "projects" {
  bucket     = aws_s3_bucket.default.id
  for_each   = var.archived_projects
  key        = "${each.value.path}/meta.yml"
  content    = yamlencode(each.value)
  kms_key_id = module.kms_key.key_arn
  depends_on = [time_sleep.wait_30_seconds]
}


### RDS IAM
# allows for rds to export to our s3 archive bucket
data "aws_iam_policy_document" "rds_s3_export_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["export.rds.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "rds_s3_export" {
  statement {
    sid    = "AllowListS3Archive"
    effect = "Allow"
    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation"
    ]
    resources = [
      aws_s3_bucket.default.arn,
    ]
  }
  statement {
    sid    = "AllowWriteAccessToS3Archive"
    effect = "Allow"
    actions = [
      "s3:PutObject*",
      "s3:GetObject*",
      "s3:CopyObject*",
      "s3:DeleteObject*"
    ]
    resources = [
      aws_s3_bucket.default.arn,
      "${aws_s3_bucket.default.arn}/*"

    ]
  }
}

resource "aws_iam_policy" "export_policy" {
  name   = "${module.this.id}-rds-export"
  policy = data.aws_iam_policy_document.rds_s3_export.json
}

resource "aws_iam_role" "rds_role" {
  name               = "rds-s3-export-to-archive"
  description        = "Role for exporting RDS snapshots to S3 archive"
  assume_role_policy = data.aws_iam_policy_document.rds_s3_export_assume.json
}

resource "aws_iam_role_policy_attachment" "attachment" {
  role       = aws_iam_role.rds_role.name
  policy_arn = aws_iam_policy.export_policy.arn
}
