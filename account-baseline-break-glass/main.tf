terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "break_glass" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-break-glass?ref=main"
  #source            = "/home/abel/src/guardianproject-ops/terraform-aws-account-break-glass"
  context           = module.this.context
  break_glass_users = var.break_glass_users
}
