variable "break_glass_users" {
  type = map(object({
    email = string
  }))
  description = <<-EOT
Map of the users that will be created and added to the break glass IAM group.
The map key is a unique/non-changing identifier for the user, and the value is an object containing configuration variables for the user.
EOT
}
