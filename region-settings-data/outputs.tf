output "vpc" {
  value = data.aws_vpc.selected
}

output "public_subnet_ids" {
  value = data.aws_subnet_ids.public.ids
}

output "private_subnet_1" {
  value = data.aws_subnet.private1
}

output "private_subnet_2" {
  value = data.aws_subnet.private2
}

output "kms_key_arn" {
  value = data.aws_ssm_parameter.kms_key.value
}

output "ssh_key_secret_id" {
  value = data.aws_ssm_parameter.ssh_key.value
}