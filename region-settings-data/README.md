# region-settings-data

This module is the counterpart to ../region-settings.

This module is a pure data module only. It does not change any resources, it
only exposes the region settings.