data "aws_vpc" "selected" {
  tags = {
    "Namespace" : module.this.namespace,
    "Environment" : module.this.environment,
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.selected.id
  tags = {
    "Namespace" : module.this.namespace,
    "Environment" : module.this.environment,
    "Visibility" : "public"
  }
}

data "aws_subnet" "private1" {
  vpc_id = data.aws_vpc.selected.id
  tags = {
    "Namespace" : module.this.namespace,
    "Environment" : module.this.environment,
    "Visibility" : "private",
    "Attributes" : "private1"
  }
}
data "aws_subnet" "private2" {
  vpc_id = data.aws_vpc.selected.id
  tags = {
    "Namespace" : module.this.namespace,
    "Environment" : module.this.environment,
    "Visibility" : "private",
    "Attributes" : "private2"
  }
}

data "aws_ssm_parameter" "kms_key" {
  name = "/region-settings/kms_key_arn"
}

data "aws_ssm_parameter" "ssh_key" {
  name = "/region-settings/secretsmanager_ssh_key_secret"
}

// data "aws_secretsmanager_secret" "ssh_key" {
// name = data.aws_ssm_parameter.ssh_key.value
// }