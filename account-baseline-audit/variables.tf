variable "governed_regions" {
  description = "List of AWS regions to enable LandingZone, GuardDuty, etc in"
  type        = list(string)
}

variable "guardduty_enabled" {
  type        = bool
  default     = true
  description = <<-EOT
Whether to GuardDuty across the AWS Organization.
EOT
}

variable "management_account_id" {
  type        = string
  description = <<-EOT
The account id for the AWS Organizations root account
EOT
}

variable "logging_account_id" {
  type        = string
  description = <<-EOT
The account id for the AWS Control Tower Log archive account
EOT
}


variable "aws_organization_root_id" {
  type        = string
  description = <<-EOT
The organization root id, this is NOT the root account id, this is a property of the AWS Organization resource it self.
Use: aws organizations list-roots --query "Roots[0].Id"
EOT
}
