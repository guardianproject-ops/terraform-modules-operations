terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "audit" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-baseline-audit?ref=main"
  #source = "/home/abel/src/guardianproject-ops/terraform-aws-account-baseline-audit"

  guardduty_enabled        = var.guardduty_enabled
  governed_regions         = var.governed_regions
  aws_organization_root_id = var.aws_organization_root_id
  management_account_id    = var.management_account_id
  logging_account_id       = var.logging_account_id
  context                  = module.this.context
}
