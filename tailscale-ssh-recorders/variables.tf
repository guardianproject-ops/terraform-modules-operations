variable "subnets_cidr" { type = string }
variable "vpc_cidr" { type = string }
variable "tailscale_tailnet" { type = string }

variable "tsrecorder_container_image" {
  type        = string
  default     = "docker.io/tailscale/tsrecorder:stable"
  description = <<EOT
The fully qualified container image for tailscale.
EOT
}

variable "exec_enabled" {
  type        = bool
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
  default     = false
}

variable "tsrecorder_instances" {
  type = map(object({
    hostname = string
    count    = number
  }))
  description = <<EOT
A tsrecorder ecs service with desired_count = count will be started for each entry in this map.
EOT
}

variable "tsrecorder_instance_secrets" {
  sensitive   = true
  description = <<EOT
Every key in this map should correspond to one in tsrecorder_instances. We have to split them up because this one contains secrets.
EOT
  type = map(object({
    tailscale_tags          = list(string)
    tailscale_client_id     = string
    tailscale_client_secret = string
  }))
}

variable "tsrecorder_retention_expire_days" {
  type        = number
  description = "The number of days after which objects expire"
  default     = 180
}
