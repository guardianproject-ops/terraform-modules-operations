output "tsrecorder" {
  value = module.tsrecorder
}

output "vpc" {
  value = module.vpc
}

output "subnets" {
  value = module.subnets
}

output "bucket_name" {
  value = try(aws_s3_bucket.tsrecorder[0].id, "")
}
