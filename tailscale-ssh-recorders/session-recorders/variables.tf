variable "subnets_cidr" { type = string }
variable "availability_zones" { type = list(string) }
variable "vpc_cidr" { type = string }
variable "tailscale_tailnet" { type = string }
variable "tsrecorder_container_image" { type = string }
variable "kms_key_arn" { type = string }
variable "exec_enabled" { type = bool }

variable "tsrecorder_instances" {
  type = map(object({
    hostname = string
    count    = number
  }))
}

variable "tsrecorder_instance_secrets" {
  sensitive = true
  type = map(object({
    tailscale_tags          = list(string)
    tailscale_client_id     = string
    tailscale_client_secret = string
  }))
}

variable "retention_expire_days" { type = number }
