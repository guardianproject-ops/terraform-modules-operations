################
# VPC
################

module "vpc" {
  source                           = "cloudposse/vpc/aws"
  version                          = "2.2.0"
  ipv4_primary_cidr_block          = var.vpc_cidr
  assign_generated_ipv6_cidr_block = false
  context                          = module.this.context
  attributes                       = ["vpc"]
}

module "subnets" {
  source                          = "cloudposse/dynamic-subnets/aws"
  version                         = "2.4.2"
  max_subnet_count                = 2
  availability_zones              = var.availability_zones
  vpc_id                          = module.vpc.vpc_id
  igw_id                          = [module.vpc.igw_id]
  ipv4_cidr_block                 = [var.subnets_cidr]
  ipv4_enabled                    = true
  ipv6_enabled                    = false
  nat_gateway_enabled             = false
  nat_instance_enabled            = false
  public_subnets_additional_tags  = { "Visibility" : "Public" }
  private_subnets_additional_tags = { "Visibility" : "Private" }
  metadata_http_endpoint_enabled  = true
  metadata_http_tokens_required   = true
  public_subnets_enabled          = true
  context                         = module.this.context
  attributes                      = ["vpc", "subnet"]
}

################
# ECS CLUSTER
################

module "ecs_cluster" {
  source                          = "cloudposse/ecs-cluster/aws"
  version                         = "0.9.0"
  context                         = module.this.context
  enabled                         = module.this.enabled
  container_insights_enabled      = true
  capacity_providers_fargate      = true
  capacity_providers_fargate_spot = false
  kms_key_id                      = var.kms_key_arn
}



################
# S3
################

resource "aws_s3_bucket" "tsrecorder" {
  count  = module.this.enabled ? 1 : 0
  bucket = "${module.this.id}-recordings"
  tags   = module.this.tags
}

resource "aws_s3_bucket_versioning" "this" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.tsrecorder[0].id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "tsrecorder" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.tsrecorder[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.tsrecorder[0].id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_key_arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.tsrecorder[0].id
  rule {
    id     = "RetentionRule"
    status = "Enabled"
    filter {}
    expiration {
      days = var.retention_expire_days
    }
  }
}

################
# tsrecorder
################

module "tsrecorder" {
  source                  = "guardianproject-ops/ecs-service-tsrecorder/aws"
  version                 = "0.0.3"
  for_each                = var.tsrecorder_instances
  context                 = module.this.context
  vpc_id                  = module.vpc.vpc_id
  ecs_cluster_arn         = module.ecs_cluster.arn
  public_subnet_ids       = module.subnets.public_subnet_ids
  private_subnet_ids      = module.subnets.private_subnet_ids
  tsrecorder_hostname     = each.value.hostname
  tailscale_tailnet       = var.tailscale_tailnet
  tailscale_client_id     = var.tsrecorder_instance_secrets[each.key].tailscale_client_id
  tailscale_client_secret = var.tsrecorder_instance_secrets[each.key].tailscale_client_secret
  tailscale_tags          = var.tsrecorder_instance_secrets[each.key].tailscale_tags
  tsrecorder_node_count   = each.value.count
  #tsrecorder_bucket_prefix   = var.tsrecorder_bucket_prefix
  tsrecorder_bucket          = try(aws_s3_bucket.tsrecorder[0].id, "")
  task_cpu                   = 512
  task_memory                = 1024
  tsrecorder_container_image = var.tsrecorder_container_image
  kms_key_arn                = var.kms_key_arn
  exec_enabled               = var.exec_enabled
}
