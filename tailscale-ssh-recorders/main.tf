data "aws_ssm_parameter" "pagerduty_token" {
  name     = "/shared-secrets/pagerduty_token"
  provider = aws.gp_operations
}

provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}

provider "grafana" {
  url  = var.grafana_url
  auth = var.grafana_auth
  http_headers = {
    "CF-Access-Client-Id" : var.grafana_cloudflare_service_token_client_id
    "CF-Access-Client-Secret" : var.grafana_cloudflare_service_token_client_secret
  }
}

data "aws_availability_zones" "this" {
  state = "available"
}

locals {
  availability_zones = sort(slice(data.aws_availability_zones.this.names, 0, 2))
}


module "kms_key" {
  source                  = "cloudposse/kms-key/aws"
  version                 = "0.12.2"
  context                 = module.this.context
  description             = "Key for encryption of tailscale audit artifacts (ssh recordings, audit logs, etc)"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  alias                   = "alias/${module.this.id}"
}

module "recorders" {
  source                      = "./session-recorders/"
  context                     = module.this.context
  name                        = "ssh-record"
  subnets_cidr                = var.subnets_cidr
  vpc_cidr                    = var.vpc_cidr
  availability_zones          = local.availability_zones
  tailscale_tailnet           = var.tailscale_tailnet
  tsrecorder_container_image  = var.tsrecorder_container_image
  kms_key_arn                 = module.kms_key.key_arn
  retention_expire_days       = var.tsrecorder_retention_expire_days
  exec_enabled                = var.exec_enabled
  tsrecorder_instances        = var.tsrecorder_instances
  tsrecorder_instance_secrets = var.tsrecorder_instance_secrets
}

resource "aws_ssm_parameter" "kms_key" {
  name  = "/tailscale/ssh-recordings-key-arn"
  type  = "String"
  value = module.kms_key.key_arn
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "bucket_name" {
  name  = "/tailscale/ssh-recordings-bucket-name"
  type  = "String"
  value = module.recorders.bucket_name
  tags  = module.this.tags
}

output "recorders" {
  value = module.recorders
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}
