terraform {
  required_version = ">= 1.3"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 5.0.0"
      configuration_aliases = [aws.gp_operations]
    }
    pagerduty = {
      source  = "pagerduty/pagerduty"
      version = ">= 3.18.1"
    }
    grafana = {
      source  = "grafana/grafana"
      version = ">= 3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.6.3"
    }
  }
}
