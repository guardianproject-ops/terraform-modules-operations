variable "pagerduty_aegean_team_emails" {
  type        = list(string)
  description = "A list of emails of the SREs in the Aegean escalation policy"
}

variable "pagerduty_ccx_team_emails" {
  type        = list(string)
  description = "A list of emails of the SREs in the CCX escalation policy"
}


variable "pagerduty_core_team_emails" {
  type        = list(string)
  description = "A list of emails of the SREs in the Core escalation policy"
}
