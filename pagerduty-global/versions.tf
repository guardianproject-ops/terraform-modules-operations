terraform {
  required_version = ">= 1.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
    pagerduty = {
      source  = "pagerduty/pagerduty"
      version = ">= 3.1.1, < 4.0"
    }
  }
}

