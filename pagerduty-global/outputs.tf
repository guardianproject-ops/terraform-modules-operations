output "pagerduty_global_integration_key" {
  value = local.global_integration_key
}

output "pagerduty_global_integration_email_address" {
  value = local.global_integration_email_address
}

output "pagerduty_global_http_endpoint" {
  value = local.global_http_endpoint
}

output "pagerduty_escalation_policies" {
  value = [
    pagerduty_escalation_policy.core.name,
    pagerduty_escalation_policy.ccx.name,
    pagerduty_escalation_policy.aegean.name

  ]
}