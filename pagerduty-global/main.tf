provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}

data "aws_ssm_parameter" "pagerduty_token" {
  name = "/shared-secrets/pagerduty_token"
}

data "pagerduty_vendor" "cloudwatch" {
  name = "Amazon CloudWatch"
}

data "pagerduty_schedule" "default" {
  name = "GP Ops - Weekly Rotation"
}

data "pagerduty_ruleset" "default_global" {
  name = "Default Global"
}

data "pagerduty_vendor" "email" {
  name = "Email"
}

locals {
  global_account_domain            = "guardianproject.eu.pagerduty.com"
  global_http_endpoint             = "https://events.eu.pagerduty.com/v2/enqueue"
  global_integration_key           = data.pagerduty_ruleset.default_global.routing_keys[0]
  global_integration_email_address = "${local.global_integration_key}@${local.global_account_domain}"
  controltower_sns_topics = [
    "aws-controltower-SecurityNotifications",
    "aws-controltower-AggregateSecurityNotifications"
  ]
}

##############################
# -------- Core Infrastructure 
##############################
# Sets up the PD subsrciption to the AWS Control Tower SNS topics for security alerts and config drift

provider "aws" {
  alias   = "audit_account"
  region  = "eu-west-1"
  profile = "gp-root"
  assume_role {
    role_arn = "arn:aws:iam::889117913633:role/AWSControlTowerExecution"
  }
  # Control Tower restricts access to the SNS topics in the audit account to the AWSControlTowerExecution role
  # the only way to assume this role is to login to the root account as admin, then assume it.
}


data "pagerduty_user" "core_users" {
  for_each = toset(var.pagerduty_core_team_emails)
  email    = each.key
}

resource "pagerduty_escalation_policy" "core" {
  name        = "Core Infra"
  description = "SREs on the core infra team"
  num_loops   = 5
  rule {
    escalation_delay_in_minutes = 30
    dynamic "target" {
      for_each = data.pagerduty_user.core_users
      content {
        type = "user_reference"
        id   = target.value["id"]
      }

    }
  }
  rule {
    escalation_delay_in_minutes = 30
    target {
      type = "schedule_reference"
      id   = data.pagerduty_schedule.default.id
    }
  }
}

module "core_alerts" {
  source                           = "../pagerduty-service"
  context                          = module.this.context
  pagerduty_service_name           = "GP AWS Control Tower"
  pagerduty_escalation_policy_name = pagerduty_escalation_policy.core.name
  suppress_alerts                  = true
  cloudwatch_sns_topics            = []
  providers = {
    aws = aws.audit_account
  }
  depends_on = [
    pagerduty_escalation_policy.core
  ]
}

# this sets up a global rule for the global inbound event email
# filters on emails from aws-controltower and routes them to the pager duty GP Control Tower Service
resource "pagerduty_ruleset_rule" "global_controltower" {
  ruleset  = data.pagerduty_ruleset.default_global.id
  position = 0
  disabled = false
  conditions {
    operator = "and"
    subconditions {
      operator = "contains"
      parameter {
        value = "aws-controltower"
        path  = "headers.from.0.address"
      }

    }
  }
  actions {
    suppress {
      value = true
    }
    route {
      value = module.core_alerts.pagerduty_service_id
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }
}

data "aws_sns_topic" "cloudwatch_alarm_topics" {
  provider = aws.audit_account
  for_each = toset(local.controltower_sns_topics)
  name     = each.key
}

resource "aws_sns_topic_subscription" "pagerduty_subscription" {
  provider  = aws.audit_account
  for_each  = data.aws_sns_topic.cloudwatch_alarm_topics
  topic_arn = each.value.arn
  protocol  = "email-json"
  endpoint  = local.global_integration_email_address
}

################################
# -------- CCX Escalation Policy
################################

data "pagerduty_user" "ccx_users" {
  for_each = toset(var.pagerduty_ccx_team_emails)
  email    = each.key
}

resource "pagerduty_escalation_policy" "ccx" {
  name        = "CCX"
  description = "SREs on the CCX team"
  num_loops   = 5
  rule {
    escalation_delay_in_minutes = 30
    dynamic "target" {
      for_each = data.pagerduty_user.ccx_users
      content {
        type = "user_reference"
        id   = target.value["id"]
      }

    }
  }
  rule {
    escalation_delay_in_minutes = 30
    target {
      type = "schedule_reference"
      id   = data.pagerduty_schedule.default.id
    }
  }
}

###################################
# -------- Aegean Escalation Policy
###################################
data "pagerduty_user" "aegean_users" {
  for_each = toset(var.pagerduty_aegean_team_emails)
  email    = each.key
}
resource "pagerduty_escalation_policy" "aegean" {
  name        = "Aegean"
  description = "SREs on the Aegean team"
  num_loops   = 5
  rule {
    escalation_delay_in_minutes = 30
    dynamic "target" {
      for_each = data.pagerduty_user.aegean_users
      content {
        type = "user_reference"
        id   = target.value["id"]
      }

    }
  }
  rule {
    escalation_delay_in_minutes = 30
    target {
      type = "schedule_reference"
      id   = data.pagerduty_schedule.default.id
    }
  }
}