terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "break_glass" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-break-glass//modules/regional?ref=main"
  #source              = "/home/abel/src/guardianproject-ops/terraform-aws-account-break-glass/modules/regional"
  context             = module.this.context
  alert_email_address = var.alert_email_address
  subscribers = {
    ops_bot = {
      protocol = "https"
      endpoint = var.matrix_bot_endpoint
    }
  }
}
