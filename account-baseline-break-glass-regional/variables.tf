variable "matrix_bot_endpoint" {
  description = "The endpoint of the matrix bot"
  type        = string
}

variable "alert_email_address" {
  description = "An optional email address that will be alerted when Break Glass users are used"
  type        = string
  default     = null
}
