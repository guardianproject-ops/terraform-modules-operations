locals {
  enabled     = module.this.enabled
  bucket_name = var.bucket_name != null ? var.bucket_name : aws_s3_bucket.this[0].id
}

data "aws_region" "current" {}

################
# S3
################

resource "aws_s3_bucket" "this" {
  count  = local.enabled ? 1 : 0
  bucket = module.this.id
  tags   = module.this.tags
}

resource "aws_s3_bucket_versioning" "this" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.this[0].id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  count                   = local.enabled ? 1 : 0
  bucket                  = aws_s3_bucket.this[0].id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.this[0].id

  rule {
    bucket_key_enabled = true
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_key_arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  count  = local.enabled ? 1 : 0
  bucket = aws_s3_bucket.this[0].id
  rule {
    id     = "retain-logs-for-${var.retention_expire_days}-days"
    status = "Enabled"
    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
    expiration {
      days = var.retention_expire_days
    }
    noncurrent_version_expiration {
      noncurrent_days = var.retention_expire_days
    }
  }
  rule {
    id     = "abort-incomplete-multipart-upload"
    status = "Enabled"
    abort_incomplete_multipart_upload {
      days_after_initiation = 7
    }
  }
}
