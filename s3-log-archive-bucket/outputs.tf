output "bucket_name" {
  value = try(local.bucket_name, "")
}

output "region" {
  value = data.aws_region.current.name
}
