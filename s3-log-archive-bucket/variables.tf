variable "kms_key_arn" { type = string }

variable "retention_expire_days" {
  type        = number
  description = "The number of days after which objects expire"
  default     = 365 * 2
}

variable "bucket_name" {
  type        = string
  default     = null
  description = "The bucket name, optional, if not provided a name from the null-label will be generated"
}
