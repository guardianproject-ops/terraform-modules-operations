data "aws_partition" "current" {}

locals {
  partition = data.aws_partition.current.partition
}

resource "aws_s3_bucket_versioning" "default" {
  bucket = aws_s3_bucket.default.id
  versioning_configuration {
    status = "Disabled"
  }
}

resource "aws_s3_bucket" "default" {
  bucket        = module.this.id
  acl           = "private"
  force_destroy = var.force_destroy

  dynamic "lifecycle_rule" {
    for_each = var.expiration_enabled ? [var.expiration_days] : []
    content {
      id      = module.this.id
      enabled = true
      expiration {
        days = lifecycle_rule.value
      }
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = var.kms_key_arn
      }
    }
  }

  tags = module.this.tags
}

# Refer to the terraform documentation on s3_bucket_public_access_block at
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block
# for the nuances of the blocking options
resource "aws_s3_bucket_public_access_block" "default" {
  count  = module.this.enabled ? 1 : 0
  bucket = aws_s3_bucket.default.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
