variable "kms_key_arn" {
  type        = string
  description = "the kms key used to encrypt the bucket contents"
}

variable "force_destroy" {
  description = "(Optional, Default:false ) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  type        = bool
  default     = false
}

variable "expiration_enabled" {
  type        = bool
  description = "Whether objects should be expunged automatically, see expiration_days"
}

variable "expiration_days" {
  description = "Number of days after which to expunge the objects."
  default     = -1 # this will throw an error at runtime if the user doesn't override it but has expiration_enabled
  type        = number
}
