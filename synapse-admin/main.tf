terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1]
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    github = {
      source  = "integrations/github"
      version = "~> 5.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

provider "github" {}

provider "gitlab" {
  token = var.gitlab_access_token
}

provider "aws" {
  # CloudFront requires that Lambda@Edge be in us-east-1
  alias  = "us_east_1"
  region = "us-east-1"
}

module "instance" {
  for_each = var.instances
  providers = {
    aws.us_east_1 = aws.us_east_1
  }
  context           = module.this.context
  attributes        = [each.value.domain_name]
  source            = "./synapse-admin"
  domain_name       = each.value.domain_name
  gitlab_project    = each.value.gitlab_project
  cloudflare_zone   = each.value.cloudflare_zone
  synapse_admin_tag = each.value.synapse_admin_tag
}

output "instances" {
  value = module.instance
}
