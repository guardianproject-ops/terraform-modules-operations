variable "cloudflare_edit_token" {
  type      = string
  sensitive = true
}

variable "gitlab_access_token" {
  type      = string
  sensitive = true
}

variable "instances" {
  type = map(object({
    cloudflare_zone = string,
    domain_name     = string,
    gitlab_project  = string,
    synapse_admin_tag = string
  }))
}