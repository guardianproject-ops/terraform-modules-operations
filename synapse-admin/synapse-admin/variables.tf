variable "cloudflare_zone" {
  type        = string
  description = "The cloudflare zone (e.g., example.com)"
}

variable "domain_name" {
  type        = string
  description = "Domain name of the static website to serve."
}

variable "gitlab_project" {
  type        = string
  description = "The group qualified gitlab project name (e.g., guardianproject/someproject)"
}

variable "synapse_admin_tag" {
  type = string
  description = "The synapse-admin app tag to deploy"

}