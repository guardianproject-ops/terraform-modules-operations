output "bucket_domain_name" {
  value       = module.cdn.s3_bucket_domain_name
  description = "FQDN of bucket"
}

output "bucket_id" {
  value       = module.cdn.s3_bucket
  description = "Bucket ID"
}

output "bucket_arn" {
  value       = module.cdn.s3_bucket_arn
  description = "Bucket ARN"
}
output "bucket_region" {
  value       = data.aws_region.current.name
  description = "Bucket region"
}

output "synapse_admin_release" {
  value = data.github_release.this
}