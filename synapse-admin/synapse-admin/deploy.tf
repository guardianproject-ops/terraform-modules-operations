data "github_release" "this" {
  repository  = "synapse-admin"
  owner       = "Awesome-Technologies"
  retrieve_by = "tag"
  release_tag = var.synapse_admin_tag
}

resource "null_resource" "unpack_and_upload" {
  provisioner "local-exec" {
    command = <<EOL
      set -eu
      curl --silent -L "${data.github_release.this.assets[0].browser_download_url}" --output "${path.module}/${data.github_release.this.assets[0].name}"
      mkdir -p "${path.module}/unpacked"
      tar --strip-components=1 -xzf "${path.module}/${data.github_release.this.assets[0].name}" -C "${path.module}/unpacked"
      aws s3 sync --delete "${path.module}/unpacked/" "s3://${module.cdn.s3_bucket}"
      rm -rf "${path.module}/unpacked" "${path.module}/${data.github_release.this.assets[0].name}"
EOL
  }

  triggers = {
    always_run = "${timestamp()}" # Ensures that the provisioner is always run
  }
}
