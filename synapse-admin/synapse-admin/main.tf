terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1]
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.0"
    }
  }
}

data "aws_region" "current" {}


module "cdn" {
  source = "cloudposse/cloudfront-s3-cdn/aws"
  version = "0.92.0"
  context                             = module.this.context
  comment                             = "CDN for Synapse Admin ${var.domain_name}"
  aliases                             = [var.domain_name]
  dns_alias_enabled                   = false
  cloudfront_access_logging_enabled   = true
  cloudfront_access_log_create_bucket = true
  acm_certificate_arn                 = aws_acm_certificate.this.arn
}

resource "cloudflare_record" "this" {
  zone_id = data.cloudflare_zone.this.id
  name    = var.domain_name
  value   = local.aws_cloudfront_distribution_domain_name
  type    = "CNAME"
  ttl     = 3600
}


locals {
  aws_s3_bucket_arn                       = module.cdn.s3_bucket_arn
  aws_cloudfront_distribution_arn         = module.cdn.cf_arn
  aws_cloudfront_distribution_domain_name = module.cdn.cf_domain_name
}
