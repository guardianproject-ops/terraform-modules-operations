variable "certs" {
  type = map(object({
    common_name = string
    dns_names = list(string)
  }))
}

variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_edit_token" {
  type = string
}
