# this cert is in region us_east_1. why?
# to use an ACM certificate with Amazon CloudFront, you must request or import
# the certificate in the US East (N. Virginia) region.
module "cert" {
  for_each                  = var.certs
  source                    = "./cert"
  domain_name               = each.value.common_name
  subject_alternative_names = each.value.dns_names
  cloudflare_zone_id        = var.cloudflare_zone_id
  context                   = module.this.context

  providers = {
    aws = aws.us_east_1
  }
}

# this cert is in the region of the deployment. why?

# to use a certificate with a load balancer for the same fully qualified domain
# name (FQDN) or set of FQDNs in more than one AWS region, you must request or
# import a certificate for each region. You cannot copy a certificate between
# regions.
module "cert_local" {
  for_each                  = var.certs
  source                    = "./cert"
  domain_name               = each.value.common_name
  subject_alternative_names = each.value.dns_names
  cloudflare_zone_id        = var.cloudflare_zone_id
  is_already_validated      = true
  validated_hostnames       = module.cert[each.key].validated_hostnames
  context                   = module.this.context

  providers = {
    aws = aws.main
  }
}

# what's up with the validated_hostnames and is_already_validated?
# well we need to validate the cert in muliple regions, but the CNAME validation string
# stays the same across the regions
# this will cause the cloudflare provider to error out with:
#      Error: expected DNS record to not already be present but already exists
# we can't create the same record twice with 2 different terraform resources
# so we just skip the creation of the records in cloudflare if we have succesfully created
# them once before.
# i love this stuff.
