output "acm" {
  description = "Attributes from aws_acm_certificate (https://www.terraform.io/docs/providers/aws/r/acm_certificate.html)"
  value       = aws_acm_certificate.cert
}

output "acm_cert_id" {
  value = aws_acm_certificate.cert.id
}

output "validated_hostnames" {
  value = local.validated_hostnames
}
