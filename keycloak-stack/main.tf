
provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

data "aws_ssm_parameter" "pagerduty_token" {
  name     = "/shared-secrets/pagerduty_token"
  provider = aws.gp_operations
}

provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}

provider "grafana" {
  url  = var.grafana_url
  auth = var.grafana_auth
  http_headers = {
    "CF-Access-Client-Id" : var.grafana_cloudflare_service_token_client_id
    "CF-Access-Client-Secret" : var.grafana_cloudflare_service_token_client_secret
  }
}

data "aws_availability_zones" "this" {
  state = "available"
}

locals {
  availability_zones           = sort(slice(data.aws_availability_zones.this.names, 0, 2))
  rds_allow_access_cidr_blocks = concat(module.subnets.private_subnet_cidrs, module.subnets.public_subnet_cidrs)
}


################
# VPC
################

module "vpc" {
  source                           = "cloudposse/vpc/aws"
  version                          = "2.2.0"
  ipv4_primary_cidr_block          = var.vpc_cidr
  assign_generated_ipv6_cidr_block = false
  context                          = module.this.context
  attributes                       = ["vpc"]
}

module "subnets" {
  source                          = "cloudposse/dynamic-subnets/aws"
  version                         = "2.4.2"
  max_subnet_count                = 2
  availability_zones              = local.availability_zones
  vpc_id                          = module.vpc.vpc_id
  igw_id                          = [module.vpc.igw_id]
  ipv4_cidr_block                 = [var.subnets_cidr]
  ipv4_enabled                    = true
  ipv6_enabled                    = false
  nat_gateway_enabled             = false
  nat_instance_enabled            = false
  public_subnets_additional_tags  = { "Visibility" : "Public" }
  private_subnets_additional_tags = { "Visibility" : "Private" }
  metadata_http_endpoint_enabled  = true
  metadata_http_tokens_required   = true
  public_subnets_enabled          = true
  context                         = module.this.context
  attributes                      = ["vpc", "subnet"]
}

################
# RDS / DB
################

module "label_db" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["db"]
  context    = module.this.context
  enabled    = true
}

module "db" {
  source                              = "guardianproject-ops/rds-postgresql/aws"
  version                             = "0.0.2"
  context                             = module.label_db.context
  allocated_storage                   = var.rds_allocated_storage_gb
  max_allocated_storage               = var.rds_max_allocated_storage_gb
  allow_access_cidr_blocks            = local.rds_allow_access_cidr_blocks
  postgres_major_version              = var.rds_postgres_major_version
  apply_immediately                   = true
  backup_retention_period             = 35
  deletion_protection_enabled         = var.deletion_protection_enabled
  iam_database_authentication_enabled = true
  instance_class                      = var.rds_instance_class
  skip_final_snapshot                 = false
  alarms_enabled                      = true
  alarms_sns_topics                   = local.alarms_sns_topics_arns
  subnet_ids                          = concat(module.subnets.public_subnet_ids, module.subnets.private_subnet_ids)
  vpc_id                              = module.vpc.vpc_id
  kms_key_id                          = var.kms_key_arn
}

################
# Cloudflare Certs
################

module "label_certs" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["certs"]
  context    = module.this.context
}

module "acm_certs" {
  source = "../acm-cloudflare-certs"
  providers = {
    aws.us_east_1 = aws.us_east_1
    aws.main      = aws
  }
  context               = module.label_certs.context
  cloudflare_edit_token = var.cloudflare_edit_token
  cloudflare_zone_id    = var.cloudflare_zone_id
  certs = {
    "keycloak_domain" = {
      common_name = var.keycloak_domain
      dns_names   = [var.keycloak_domain]
    }
  }
}
################
# Keycloak
################

module "keycloak" {
  #source = "/home/abel/src/guardianproject-ops/terraform-aws-ecs-keycloak"
  source  = "guardianproject-ops/ecs-keycloak/aws"
  version = "0.0.1"

  context                      = module.this.context
  vpc_id                       = module.vpc.vpc_id
  public_subnet_ids            = module.subnets.public_subnet_ids
  public_subnet_cidrs          = module.subnets.public_subnet_cidrs
  private_subnet_ids           = module.subnets.private_subnet_ids
  keycloak_acm_certificate_arn = module.acm_certs.certificate_arns["keycloak_domain"]
  db_keycloak_host             = module.db.address
  rds_master_username          = module.db.admin_username
  rds_resource_id              = module.db.resource_id
  rds_master_user_secret_arn   = module.db.master_user_secret_arn
  tailscale_client_id          = var.tailscale_client_id
  tailscale_client_secret      = var.tailscale_client_secret
  tailscale_tailnet            = var.tailscale_tailnet
  tailscale_tags_keycloak      = var.tailscale_tags_keycloak
  keycloak_admin_subdomain     = var.keycloak_admin_subdomain
  deletion_protection_enabled  = var.deletion_protection_enabled
  task_cpu                     = var.task_cpu
  task_memory                  = var.task_memory
  jvm_heap_min                 = var.jvm_heap_min
  jvm_heap_max                 = var.jvm_heap_max
  jvm_meta_min                 = var.jvm_meta_min
  jvm_meta_max                 = var.jvm_meta_max
  keycloak_node_count          = var.keycloak_node_count
  keycloak_container_image     = var.keycloak_container_image
  tailscale_container_image    = var.tailscale_container_image
  java_opts_extra              = var.java_opts_extra
  kms_key_arn                  = var.kms_key_arn
  alarms_sns_topics_arns       = local.alarms_sns_topics_arns
  exec_enabled                 = var.exec_enabled
}

resource "cloudflare_record" "this" {
  zone_id = var.cloudflare_zone_id
  name    = var.keycloak_domain
  content = module.keycloak.alb.alb_dns_name
  type    = "CNAME"
  ttl     = 1
  proxied = true
  comment = "Created by Terraform as a part of ${module.this.id}"
}


output "keycloak_password" {
  description = "The password for the temporary keycloak admin account used to bootstrap the instance"
  value       = module.keycloak.keycloak_password
  sensitive   = true
}

output "alb" {
  value = module.keycloak.alb
}

output "db" {
  value = module.db
}

output "keycloak" {
  value = module.keycloak
}

output "vpc" {
  value = module.vpc
}

output "subnets" {
  value = module.subnets
}
