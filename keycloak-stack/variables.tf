variable "subnets_cidr" { type = string }
variable "vpc_cidr" { type = string }
variable "tailscale_tailnet" { type = string }
variable "tailscale_client_id" { type = string }
variable "tailscale_client_secret" { type = string }
variable "tailscale_tags_keycloak" { type = list(string) }
variable "keycloak_admin_subdomain" { type = string }
variable "cloudflare_edit_token" { type = string }
variable "cloudflare_zone_id" { type = string }

variable "rds_instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
}

variable "rds_postgres_major_version" {
  description = "The postgres major version you want to run. The specific major version is then calculated by the module. Example: 16, 17, etc"
  type        = string
}

variable "rds_allocated_storage_gb" {
  description = "The allocated storage in gigabytes for the RDS database"
  type        = number
}

variable "rds_max_allocated_storage_gb" {
  description = "The maximum storage the RDS database can autoscale to"
  type        = number
}

variable "keycloak_domain" {
  type        = string
  description = "The public domain at which keycloak will be made available"
}



variable "task_cpu" {
  type        = number
  description = "The number of CPU units used by the task. If unspecified, it will default to `container_cpu`. If using `FARGATE` launch type `task_cpu` must match supported memory values (https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size)"
}

variable "task_memory" {
  type        = number
  description = "The amount of memory (in MiB) used by the task. If unspecified, it will default to `container_memory`. If using Fargate launch type `task_memory` must match supported cpu value (https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size)"
}


variable "keycloak_node_count" {
  type        = number
  description = "The number of keycloak containers to run in clustering mode"
  default     = 2
}

variable "java_opts_extra" {
  type        = list(string)
  default     = []
  description = <<EOT
An optional list of arguments to add to JAVA_OPTS
EOT
}

variable "jvm_heap_min" {
  description = <<EOT
Minimum JVM heap size for Keycloak in MB
  EOT
  type        = number
  default     = 512
}

variable "jvm_heap_max" {
  description = <<EOT
Maximum JVM heap size for Keycloak in MB. The default is 75% of the task_memory
EOT
  type        = number
  default     = null
}

variable "jvm_meta_min" {
  description = <<EOT
Minimum JVM meta space size for Keycloak in MB"
EOT
  type        = number
  default     = 128
}

variable "jvm_meta_max" {
  description = <<EOT
 Maximum JVM meta space size for Keycloak in MB.
EOT
  type        = number
  default     = 256
}

variable "keycloak_container_image" {
  type        = string
  description = <<EOT
The fully qualified container image for keycloak.
EOT
}

variable "tailscale_container_image" {
  type        = string
  default     = "ghcr.io/tailscale/tailscale:stable"
  description = <<EOT
The fully qualified container image for tailscale.
EOT
}

variable "deletion_protection_enabled" {
  type        = bool
  description = "Whether or not to enable deletion protection on things that support it"
  default     = true
}

variable "kms_key_arn" {
  type        = string
  description = "The kms key ARN used for various purposes throughout the deployment"
  default     = null
}

variable "exec_enabled" {
  type        = bool
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
  default     = false
}
