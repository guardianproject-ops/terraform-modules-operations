module "expiration_lambda" {
  source     = "./secret-expiration-lambda"
  context    = module.this.context
  attributes = ["expiration"]
}
module "static_secret" {
  source                  = "./static-secret"
  for_each                = var.static_secrets
  context                 = module.this.context
  name                    = each.key
  secret_key              = each.key
  secret_config           = each.value
  dynamodb_table          = module.expiration_lambda.dynamodb_table
  dynamodb_table_hash_key = module.expiration_lambda.dynamodb_table_hash_key
  cloudwatch_namespace    = module.expiration_lambda.cloudwatch_namespace
}

module "deployment_kms_key_label" {
  for_each = var.deployment_kms_keys

  source      = "cloudposse/label/null"
  version     = "0.25.0"
  namespace   = each.value.namespace
  environment = each.value.environment
  name        = each.value.project
  tags = {
    "Project" : each.value.project
    "ManagedBy" : "gp-operations-terraform"
  }
}

module "deployment_kms_keys" {
  for_each                = var.deployment_kms_keys
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.12.1"
  context                 = module.deployment_kms_key_label[each.key].context
  description             = "KMS Key for encrypting secrets in the ${each.value.project} deployment"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  alias                   = "alias/${module.deployment_kms_key_label[each.key].id}"
}