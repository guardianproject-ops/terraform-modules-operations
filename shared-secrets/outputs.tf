output "static_secrets" {
  value = module.static_secret
}

output "deployment_kms_keys" {
  value = module.deployment_kms_keys
}