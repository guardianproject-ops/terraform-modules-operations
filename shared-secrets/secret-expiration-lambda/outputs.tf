output "lambda" {
  value = module.lambda_function
}

output "dynamodb_table" {
  value = aws_dynamodb_table.secrets.name
}

output "dynamodb_table_hash_key" {
  value = aws_dynamodb_table.secrets.hash_key
}

output "cloudwatch_namespace" {
  value = module.this.id
}