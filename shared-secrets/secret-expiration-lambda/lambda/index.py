import boto3
import os
from datetime import date, datetime

table_name = os.environ["DYNAMODB_TABLE"]
cloudwatch_namespace = os.environ["CLOUDWATCH_NAMESPACE"]
ddb = boto3.client("dynamodb")
cw = boto3.client("cloudwatch")


def read_secrets():
    data = ddb.scan(TableName=table_name)
    return data["Items"]


def parse_items(items):
    for item in items:
        yield {
            "secret_key": item["secret_key"]["S"],
            "expiration_date": item["expiration_date"]["S"],
        }


def publish_metric(metric):
    secret_key = metric["secret_key"]
    cw.put_metric_data(
        MetricData=[
            {
                "MetricName": f"days_remaining_{secret_key}",
                "Unit": "Count",
                "Value": metric["days_remaining"],
            }
        ],
        Namespace=cloudwatch_namespace,
    )


def days_remaining_until(now, a_date_str):
    expiration_date = datetime.strptime(a_date_str, "%Y-%m-%d")
    diff = expiration_date - now
    return diff.days


def test_days_remaining_until():
    now = datetime.strptime("2022-01-01", "%Y-%m-%d")
    result = days_remaining_until(now, "2022-01-01")
    assert result == 0

    result = days_remaining_until(now, "2022-02-01")
    assert result == 31

    result = days_remaining_until(now, "2021-12-01")
    assert result == -31


def item_to_metric(now, item):
    expiration_date = item["expiration_date"]
    days_remaining = days_remaining_until(now, expiration_date)
    secret_key = item["secret_key"]
    print(
        f"secret {secret_key} expires on {expiration_date } and has {days_remaining} days remaining"
    )
    return {"secret_key": secret_key, "days_remaining": days_remaining}


def test_item_to_metric():
    now = datetime.strptime("2022-01-01", "%Y-%m-%d")
    result = item_to_metric(
        now, {"secret_key": "foobar", "expiration_date": "2022-02-01"}
    )
    assert result == {"secret_key": "foobar", "days_remaining": 31}


def lambda_handler(event, context):
    now = datetime.today()
    items = read_secrets()
    print(f"Found {len(items)} items")
    for item in parse_items(items):
        print(item)
        metric = item_to_metric(now, item)
        publish_metric(metric)

    return event