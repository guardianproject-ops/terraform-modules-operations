resource "aws_dynamodb_table" "secrets" {
  name         = module.this.id
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "secret_key"
  attribute {
    name = "secret_key"
    type = "S"
  }
}
data "aws_iam_policy_document" "policy" {
  statement {
    sid = "AllowDynamoDbMeta"
    actions = [
      "dynamodb:List*",
      "dynamodb:DescribeReservedCapacity*",
      "dynamodb:DescribeLimits",
      "dynamodb:DescribeTimeToLive"

    ]
    resources = ["*"]
  }
  statement {
    sid = "AllowDynamoDbRO"
    actions = [
      "dynamodb:BatchGet*",
      "dynamodb:DescribeStream",
      "dynamodb:DescribeTable",
      "dynamodb:Get*",
      "dynamodb:Query",
      "dynamodb:Scan"
    ]
    resources = [aws_dynamodb_table.secrets.arn]
  }
  statement {
    sid = "AllowCloudwatchW"
    actions = [
      "cloudwatch:PutMetricData"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda" {
  name        = module.this.id
  path        = "/"
  description = "Allows the secrets expiration lambda to and dynamodb"
  policy      = data.aws_iam_policy_document.policy.json
  tags        = module.this.tags
}

resource "aws_cloudwatch_event_rule" "every_day" {
  name                = "${module.this.id}-cron"
  description         = "Fires every day"
  schedule_expression = "rate(1440 minutes)"
}


module "lambda_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "2.35.0"

  function_name                     = module.this.id
  description                       = "Creates cloudwatch metrics of our static secret expiration dates"
  handler                           = "index.lambda_handler"
  runtime                           = "python3.9"
  build_in_docker                   = true
  publish                           = true
  source_path                       = "${path.module}/lambda"
  cloudwatch_logs_retention_in_days = 7
  create_package                    = true
  attach_policies                   = true
  number_of_policies                = 2
  policies = [
    aws_iam_policy.lambda.arn,
    "arn:aws:iam::aws:policy/CloudWatchLambdaInsightsExecutionRolePolicy"
  ]
  environment_variables = {
    DYNAMODB_TABLE       = aws_dynamodb_table.secrets.name
    CLOUDWATCH_NAMESPACE = module.this.id
  }
  allowed_triggers = {
    CronRule = {
      principal  = "events.amazonaws.com"
      source_arn = aws_cloudwatch_event_rule.every_day.arn
    }
  }
  tags = module.this.tags
}