variable "static_secrets" {
  description = "The secrets to store in AWS SecretsManager. The expiration_date must be of the format YYYY-MM-DD and be in the future."

  type = map(object({
    description      = string
    current_value    = string
    expiration_date  = string
    extra_remarks    = string
    extra_attributes = list(string)
  }))

  validation {
    condition = length([
      for k, v in var.static_secrets : v.expiration_date
      if can(regex("^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$", v.expiration_date))
    ]) == length(var.static_secrets)

    error_message = "Error: One of the static secrets has an invalid expiration_date."
  }
}

variable "deployment_kms_keys" {
  type = map(object({
    namespace   = string
    environment = string
    project     = string
  }))
}
