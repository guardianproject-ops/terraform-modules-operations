output "ssm_param" {
  value = aws_ssm_parameter.param.name
}

output "ssm_param_arn" {
  value = aws_ssm_parameter.param.arn
}