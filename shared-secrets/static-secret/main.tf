locals {
  now = parseint(formatdate("YYYYMMDD", timestamp()), 10)
}

data "assert_test" "static_secrets" {
  test  = parseint(replace(var.secret_config.expiration_date, "-", ""), 10) > local.now
  throw = "static secret ${var.secret_key} expired on ${var.secret_config.expiration_date}. Please provide a new one."
}


module "static_secrets_key_label" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  context    = module.this.context
  attributes = var.secret_config.extra_attributes
  tags = {
    "ExpiresAt" : var.secret_config.expiration_date
    "ManagedBy" : "gp-operations-terraform"
  }
}

resource "aws_ssm_parameter" "param" {
  name  = "/shared-secrets/${var.secret_key}"
  type  = "SecureString"
  value = var.secret_config.current_value
  tags  = module.this.tags
}

resource "aws_dynamodb_table_item" "secrets" {
  table_name = var.dynamodb_table
  hash_key   = var.dynamodb_table_hash_key

  item = <<ITEM
{
  "secret_key": {"S": "${var.secret_key}"},
  "expiration_date": {"S": "${var.secret_config.expiration_date}"}
}
ITEM
}

resource "aws_cloudwatch_metric_alarm" "alarm" {
  alarm_name                = "days_remaining_${module.this.id}"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = var.secret_key
  namespace                 = var.cloudwatch_namespace
  period                    = "3600"
  statistic                 = "Minimum"
  threshold                 = "30"
  alarm_description         = "This metric monitors the expiration date for the static secret ${var.secret_key}, and will alarm when it needs to be rotated"
  insufficient_data_actions = []
  treat_missing_data        = "ignore"
  tags                      = module.this.tags
  actions_enabled           = false
}