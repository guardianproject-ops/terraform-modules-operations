variable "secret_key" {
  type = string
}
variable "secret_config" {
  type = object({
    description      = string
    current_value    = string
    expiration_date  = string
    extra_remarks    = string
    extra_attributes = list(string)
  })
}

variable "dynamodb_table" { type = string }
variable "dynamodb_table_hash_key" { type = string }
variable "cloudwatch_namespace" { type = string }