variable "nat_instance_type" {
  type        = string
  description = "NAT Instance type"
  default     = "t3.micro"
}

variable "aws_route_create_timeout" {
  type        = string
  default     = "2m"
  description = "Time to wait for AWS route creation specifed as a Go Duration, e.g. `2m`"
}

variable "aws_route_delete_timeout" {
  type        = string
  default     = "5m"
  description = "Time to wait for AWS route deletion specifed as a Go Duration, e.g. `5m`"
}

variable "az1" {
  type = string
}

variable "az2" {
  type = string
}

variable "is_prod_like" {
  type = bool
}

variable "cidr_block" {
  type = string
}

variable "subnet_offset" {
  type    = number
  default = 0
}
