module "vpc" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-vpc?ref=tags/0.2.2"

  cidr_block               = var.cidr_block
  nat_instance_type        = var.nat_instance_type
  aws_route_create_timeout = var.aws_route_create_timeout
  aws_route_delete_timeout = var.aws_route_delete_timeout

  availability_zones = [var.az1, var.az2]
  nat_gateways       = var.is_prod_like ? { (var.az1) = "public_1" } : {}
  nat_instances      = !var.is_prod_like ? { (var.az1) = "public_1" } : {}

  subnets = {
    (var.az1) = {
      "public_1" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 0)
        "public_route_enabled" = true
      }
      "private_1" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 1)
        "public_route_enabled" = false
      }
      "rds_1" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 2)
        "public_route_enabled" = false
      }
    }
    (var.az2) = {
      "public_2" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 3)
        "public_route_enabled" = true
      }
      "private_2" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 4)
        "public_route_enabled" = false
      }
      "rds_2" = {
        "cidr_block"           = cidrsubnet(var.cidr_block, 8, var.subnet_offset + 5)
        "public_route_enabled" = false
      }
    }
  }
  context = module.this.context
}

locals {
  region_settings_prefix = "/${module.this.namespace}-${module.this.environment}-region-settings"
}

resource "aws_ssm_parameter" "vpc_id" {
  name  = "${local.region_settings_prefix}/default-vpc/vpc-id"
  type  = "String"
  value = module.vpc.vpc.vpc_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "public_subnet1" {
  name  = "${local.region_settings_prefix}/default-vpc/subnet-public1-id"
  type  = "String"
  value = module.vpc.public_subnets["public_1"].subnet_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "public_subnet2" {
  name  = "${local.region_settings_prefix}/default-vpc/subnet-public2-id"
  type  = "String"
  value = module.vpc.public_subnets["public_2"].subnet_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "subnet1" {
  name  = "${local.region_settings_prefix}/default-vpc/subnet-private1-id"
  type  = "String"
  value = module.vpc.private_subnets["private_1"].subnet_id
  tags  = module.this.tags
}

resource "aws_ssm_parameter" "subnet2" {
  name  = "${local.region_settings_prefix}/default-vpc/subnet-private2-id"
  type  = "String"
  value = module.vpc.private_subnets["private_2"].subnet_id
  tags  = module.this.tags
}
