terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1]
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

resource "aws_s3_bucket" "this" {
  bucket = var.domain_name
  acl    = "private"

  tags = module.this.tags
}

resource "aws_cloudfront_origin_access_identity" "this" {
  comment = "Origin access identity for www domain access"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.this.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.this.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_cloudfront_distribution" "this" {
  aliases = [var.domain_name]

  origin {
    domain_name = aws_s3_bucket.this.bucket_regional_domain_name
    origin_id   = "S3-${var.domain_name}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
    }
  }

  dynamic "origin" {
    for_each = var.matrix_domain_name == null ? [] : ["matrix"]
    content {
      domain_name = var.matrix_domain_name
      origin_id   = "matrix-${var.matrix_domain_name}"

      custom_origin_config {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "https-only"
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
    }
  }

  dynamic "origin" {
    for_each = var.weblite_paths == null ? [] : ["weblite_path_bucket"]
    content {
      domain_name = var.shared_bucket_domain_name
      origin_id   = "S3-${var.shared_bucket_id}"

      s3_origin_config {
        origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
      }
    }
  }

  dynamic "origin" {
    for_each = var.cleaninsights_domain_name == null ? [] : ["cleaninsights_domain"]
    content {
      domain_name = var.cleaninsights_domain_name
      origin_id   = "ci-${var.cleaninsights_domain_name}"

      custom_origin_config {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "https-only"
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
    }
  }

  origin {
    domain_name = "sentry.gpcmdln.net"
    origin_id   = "sentry-gpcmdln.net"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-${var.domain_name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0 # 3600
    max_ttl                = 0 # 86400

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = var.origin_request_arn
    }
    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = var.origin_response_arn
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.weblite_paths == null ? {} : var.weblite_paths
    content {
      path_pattern     = "/${ordered_cache_behavior.value}/config.json"
      allowed_methods  = ["GET", "HEAD"]
      cached_methods   = ["GET", "HEAD"]
      target_origin_id = "S3-${var.domain_name}"

      forwarded_values {
        query_string = false

        cookies {
          forward = "none"
        }
      }

      viewer_protocol_policy = "redirect-to-https"
      min_ttl                = 0
      default_ttl            = 0 # 3600
      max_ttl                = 0 # 86400

      lambda_function_association {
        event_type = "origin-request"
        lambda_arn = var.origin_request_arn
      }
      lambda_function_association {
        event_type = "origin-response"
        lambda_arn = var.origin_response_arn
      }
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.weblite_paths == null ? {} : var.weblite_paths
    content {
      path_pattern     = "/${ordered_cache_behavior.value}/*"
      allowed_methods  = ["GET", "HEAD"]
      cached_methods   = ["GET", "HEAD"]
      target_origin_id = "S3-${var.shared_bucket_id}"

      forwarded_values {
        query_string = false

        cookies {
          forward = "none"
        }
      }

      viewer_protocol_policy = "redirect-to-https"
      min_ttl                = 0
      default_ttl            = 0 # 3600
      max_ttl                = 0 # 86400

      lambda_function_association {
        event_type = "origin-request"
        lambda_arn = var.weblite_origin_request_arn
      }
      lambda_function_association {
        event_type = "origin-response"
        lambda_arn = var.weblite_origin_response_arn
      }
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.matrix_domain_name == null ? [] : ["_matrix"]
    content {
      path_pattern             = "/_matrix/*"
      allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
      cached_methods           = ["GET", "HEAD"]
      cache_policy_id          = var.pass_cache_policy_id
      origin_request_policy_id = var.pass_origin_request_policy_id
      compress                 = true
      target_origin_id         = "matrix-${var.matrix_domain_name}"
      viewer_protocol_policy   = "redirect-to-https"
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.cleaninsights_domain_name == null ? [] : ["cleaninsights_php"]
    content {
      path_pattern             = "/cleaninsights.php"
      allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
      cached_methods           = ["GET", "HEAD"]
      cache_policy_id          = var.pass_cache_policy_id
      origin_request_policy_id = var.pass_origin_request_policy_id
      compress                 = true
      target_origin_id         = "ci-${var.cleaninsights_domain_name}"
      viewer_protocol_policy   = "redirect-to-https"
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.cleaninsights_domain_name == null ? [] : ["matomo_php"]
    content {
      path_pattern             = "/matomo.php"
      allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
      cached_methods           = ["GET", "HEAD"]
      cache_policy_id          = var.pass_cache_policy_id
      origin_request_policy_id = var.pass_origin_request_policy_id
      compress                 = true
      target_origin_id         = "ci-${var.cleaninsights_domain_name}"
      viewer_protocol_policy   = "redirect-to-https"
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.cleaninsights_domain_name == null ? [] : ["matomo_js"]
    content {
      path_pattern             = "/matomo.js"
      allowed_methods          = ["GET", "HEAD", "OPTIONS"]
      cached_methods           = ["GET", "HEAD"]
      cache_policy_id          = var.pass_cache_policy_id
      origin_request_policy_id = var.pass_origin_request_policy_id
      compress                 = true
      target_origin_id         = "ci-${var.cleaninsights_domain_name}"
      viewer_protocol_policy   = "redirect-to-https"
    }
  }

  ordered_cache_behavior {
    path_pattern             = "/sentry/*"
    allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods           = ["GET", "HEAD"]
    cache_policy_id          = var.pass_cache_policy_id
    origin_request_policy_id = var.pass_origin_request_policy_id
    compress                 = true
    target_origin_id         = "sentry-gpcmdln.net"
    viewer_protocol_policy   = "redirect-to-https"

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = var.origin_request_arn
    }
  }


  dynamic "custom_error_response" {
    for_each = [
      400,
      403,
      #404,
      405,
      414,
      416
    ]
    content {
      error_caching_min_ttl = 10
      error_code            = custom_error_response.value
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_100"
  default_root_object = "index.html"

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate_validation.this.certificate_arn
    minimum_protocol_version = "TLSv1.2_2019"
    ssl_support_method       = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = module.this.tags
}

data "cloudflare_zone" "this" {
  name = var.cloudflare_zone
}

resource "cloudflare_record" "this" {
  zone_id = data.cloudflare_zone.this.id
  name    = var.domain_name
  value   = aws_cloudfront_distribution.this.domain_name
  type    = "CNAME"
  ttl     = 3600
}

resource "cloudflare_record" "www" {
  zone_id = data.cloudflare_zone.this.id
  name    = "www.${var.domain_name}"
  value   = "192.0.2.1"
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_page_rule" "www" {
  zone_id  = data.cloudflare_zone.this.id
  target   = "${cloudflare_record.www.hostname}/*"
  priority = 1

  actions {
    forwarding_url {
      url         = "https://${var.domain_name}/$1"
      status_code = 302
    }
  }
}
