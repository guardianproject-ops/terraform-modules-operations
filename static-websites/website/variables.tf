variable "cloudflare_zone" {
  type = string
}

variable "cleaninsights_domain_name" {
  type    = string
  default = null
}

variable "domain_name" {
  type        = string
  description = "Domain name of the static website to serve."
}

variable "matrix_domain_name" {
  type = string
}

variable "weblite_paths" {
  type = map(string)
}

variable "gitlab_project" {
  type = string
}

variable "weblite_origin_request_arn" {
  type = string
}

variable "weblite_origin_response_arn" {
  type = string
}

variable "origin_request_arn" {
  type = string
}

variable "origin_response_arn" {
  type = string
}

variable "shared_bucket_domain_name" {
  type = string
}

variable "shared_bucket_id" {
  type = string
}

variable "shared_bucket_arn" {
  type = string
}

variable "pass_cache_policy_id" {
  type = string
}

variable "pass_origin_request_policy_id" {
  type = string
}

