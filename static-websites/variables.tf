variable "cloudflare_edit_token" {
  type      = string
  sensitive = true
}

variable "gitlab_access_token" {
  type      = string
  sensitive = true
}

variable "shared_bucket_domain_name" {
  type = string
}

variable "shared_bucket_id" {
  type = string
}

variable "shared_bucket_arn" {
  type = string
}

variable "websites" {
  type = map(object({
    cloudflare_zone           = string,
    domain_name               = string,
    matrix_domain_name        = string,
    cleaninsights_domain_name = string,
    weblite_paths             = map(string),
    gitlab_project            = string
  }))
}
