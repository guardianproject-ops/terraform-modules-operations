terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

provider "gitlab" {
  token = var.gitlab_access_token
}

provider "aws" {
  # CloudFront requires that Lambda@Edge be in us-east-1
  alias  = "us_east_1"
  region = "us-east-1"
}



module "lambda_origin_response" {
  providers = {
    aws = aws.us_east_1
  }
  source                            = "terraform-aws-modules/lambda/aws"
  function_name                     = "${module.this.id}-static-origin-response"
  description                       = "Add security headers"
  handler                           = "index.handler"
  runtime                           = "nodejs18.x"
  source_path                       = "${path.module}/lambda-origin-response/"
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}

module "lambda_origin_request" {
  providers = {
    aws = aws.us_east_1
  }
  source                            = "terraform-aws-modules/lambda/aws"
  function_name                     = "${module.this.id}-static-origin-request"
  description                       = "Directories"
  handler                           = "index.handler"
  runtime                           = "nodejs18.x"
  source_path                       = "${path.module}/lambda-origin-request/"
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}

module "weblite_origin_request" {
  providers = {
    aws = aws.us_east_1
  }
  source                            = "terraform-aws-modules/lambda/aws"
  function_name                     = "${module.this.id}-weblite-origin-request"
  description                       = "Directories"
  handler                           = "index.handler"
  runtime                           = "nodejs18.x"
  source_path                       = "${path.module}/lambda-origin-request-weblite/"
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}

module "weblite_origin_response" {
  providers = {
    aws = aws.us_east_1
  }
  source                            = "terraform-aws-modules/lambda/aws"
  function_name                     = "${module.this.id}-weblite-origin-response"
  description                       = "Add security headers for weblite"
  handler                           = "index.handler"
  runtime                           = "nodejs18.x"
  source_path                       = "${path.module}/lambda-origin-response-weblite/"
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}

resource "aws_cloudfront_cache_policy" "front_cache_policy" {
  name        = "${module.this.id}-cache-policy"
  comment     = "Pass-through custom origin"
  default_ttl = 0
  max_ttl     = 0
  min_ttl     = 0
  parameters_in_cache_key_and_forwarded_to_origin {
    cookies_config {
      cookie_behavior = "none"
    }
    headers_config {
      header_behavior = "none"
    }
    query_strings_config {
      query_string_behavior = "none"
    }
  }
}

resource "aws_cloudfront_origin_request_policy" "front_origin_request_policy" {
  name    = "${module.this.id}-origin-request-policy"
  comment = "Pass-through custom origin"
  cookies_config {
    cookie_behavior = "all"
  }
  headers_config {
    header_behavior = "allExcept"
    # We do not want to pass the Host header through because when doing domain traversals the Host header will be wrong
    # and the origin could be unhappy
    # example:
    # you are using weblite at unready.im/lite
    # weblite makes a request through the mirror to unready.im/_matrix/...
    # the cloudfront mirror routes this through the matrix.unready.im origin
    # but without this exception it will send the header "Host: unready.im"
    # this makes the origin angry
    # the result is you get obscure cloudfront 403 errors
    headers {
      items = ["Host"]
    }
  }
  query_strings_config {
    query_string_behavior = "all"
  }
}

module "website" {
  for_each = var.websites
  providers = {
    aws.us_east_1 = aws.us_east_1
  }
  source                        = "./website"
  cloudflare_zone               = each.value.cloudflare_zone
  domain_name                   = each.value.domain_name
  matrix_domain_name            = each.value.matrix_domain_name
  cleaninsights_domain_name     = each.value.cleaninsights_domain_name
  weblite_paths                 = each.value.weblite_paths
  gitlab_project                = each.value.gitlab_project
  context                       = module.this.context
  attributes                    = [each.value.domain_name]
  origin_request_arn            = module.lambda_origin_request.lambda_function_qualified_arn
  weblite_origin_request_arn    = module.weblite_origin_request.lambda_function_qualified_arn
  weblite_origin_response_arn   = module.weblite_origin_response.lambda_function_qualified_arn
  origin_response_arn           = module.lambda_origin_response.lambda_function_qualified_arn
  shared_bucket_arn             = var.shared_bucket_arn
  shared_bucket_id              = var.shared_bucket_id
  shared_bucket_domain_name     = var.shared_bucket_domain_name
  pass_cache_policy_id          = aws_cloudfront_cache_policy.front_cache_policy.id
  pass_origin_request_policy_id = aws_cloudfront_origin_request_policy.front_origin_request_policy.id
}

data "aws_iam_policy_document" "weblite_s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${var.shared_bucket_arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [for w in module.website : w.origin_access_identity]
    }
  }
}

resource "aws_s3_bucket_policy" "weblite" {
  bucket = var.shared_bucket_id
  policy = data.aws_iam_policy_document.weblite_s3_policy.json
}

