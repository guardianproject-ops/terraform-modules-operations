output "kms_key" {
  value = module.kms_key
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}

