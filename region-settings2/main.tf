locals {
  secret_prefix = "${module.this.id}/ssh_*"
}

module "kms_key" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.9.0"
  context                 = module.this.context
  description             = "general purpose KMS key for this deployment"
  deletion_window_in_days = 30
  enable_key_rotation     = true
  alias                   = "alias/${module.this.id}"
}

resource "aws_ssm_parameter" "kms_key" {
  name  = "/region-settings/kms_key_arn"
  type  = "String"
  value = module.kms_key.key_arn
  tags  = module.this.tags
}