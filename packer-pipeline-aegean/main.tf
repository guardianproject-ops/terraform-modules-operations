terraform {
  required_version = ">= 1.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 17.7.1"
    }
  }
}

variable "oidc_gitlab_match_value" {
  type = list(any)
}

module "pipeline" {
  source                  = "git::https://gitlab.com/guardianproject-ops/terraform-aws-packer-pipeline?ref=v0.0.3"
  context                 = module.this.context
  name                    = "packer-pipeline"
  vpc_cidr                = "10.88.129.0/24"
  subnets_cidr            = "10.88.129.0/26"
  oidc_gitlab_match_value = var.oidc_gitlab_match_value
}

resource "gitlab_project_variable" "PACKER_ENV_CGD" {
  project   = "64418570" # https://gitlab.com/guardianproject-ops/packer-aws-ami
  key       = "PACKER_ENV_CGD"
  value     = module.pipeline.pkrvars_hcl_b64
  protected = true
  masked    = true
}


output "pipeline" {
  value     = module.pipeline
  sensitive = true
}
