module "label_alarms" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["alarms"]
  context    = module.this.context
}

resource "aws_sns_topic" "alarms_pagerduty" {
  name = module.label_alarms.id
}

locals {
  sns_topics_alarms = {
    "pagerduty" : {
      "topic_arn" : aws_sns_topic.alarms_pagerduty.arn
      "topic_name" : module.label_alarms.id
    }
  }
  sns_topic_arns_alarms = [for k, topic in local.sns_topics_alarms : topic.topic_arn if topic.topic_arn != null]
}

module "pagerduty" {
  source                           = "../pagerduty-service"
  pagerduty_service_name           = var.pagerduty_service_name
  pagerduty_escalation_policy_name = var.pagerduty_escalation_policy_name
  suppress_alerts                  = var.pagerduty_suppress_alerts
  cloudwatch_sns_topics            = local.sns_topics_alarms
  context                          = module.this.context
}

data "aws_ssm_parameters_by_path" "cloudwatch_cross_account_arns" {
  path      = "/cloudwatch-cross-account-roles/"
  recursive = true
}

locals {
  # this is a list of json values. the json contains role_arn, deployment_id, default_region, and external_id
  cloudwatch_cross_account_roles_data = nonsensitive([for data in data.aws_ssm_parameters_by_path.cloudwatch_cross_account_arns.values : jsondecode(data)])
  cloudwatch_cross_account_roles_map  = { for data in local.cloudwatch_cross_account_roles_data : data.deployment_id => data }
}

data "aws_iam_policy_document" "cross_account_cloudwatch" {
  dynamic "statement" {
    for_each = local.cloudwatch_cross_account_roles_map

    content {
      actions   = ["sts:AssumeRole"]
      effect    = "Allow"
      resources = [statement.value.role_arn]

      condition {
        test     = "StringEquals"
        variable = "sts:ExternalId"
        values   = [statement.value.external_id]
      }
    }
  }
}

resource "aws_iam_policy" "cross_account_cloudwatch" {
  name        = "cross-account-cloudwatch-access"
  description = "Allow access to assume role in other accounts to access CloudWatch"
  policy      = data.aws_iam_policy_document.cross_account_cloudwatch.json
}

resource "aws_iam_role_policy_attachment" "assume_role_policy_attachment" {
  role       = module.main_instance[0].iam_role_name
  policy_arn = aws_iam_policy.cross_account_cloudwatch.arn
}

