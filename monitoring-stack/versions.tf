terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.0, < 5.0"
    }
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.us_east_1]
    }
    tailscale = {
      source  = "tailscale/tailscale"
      version = "0.13.11"
    }
    pagerduty = {
      source = "pagerduty/pagerduty"
    }
  }
  required_version = ">= 1.3"
}
