variable "kms_key_arn" {
  type        = string
  description = "The kms key ARN used for various purposes throughout the deployment"
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR of the VPC"
}

variable "ec2_subnets_cidr" {
  type        = string
  description = "The CIDR of the subnets in the VPC, will be subdivided into public and private segments."
}

variable "rds_subnets_cidr" {
  type        = string
  description = "The CIDR of the subnets in the VPC that is for RDS instances"
}

variable "monitoring_domain" {
  type        = string
  description = "The base domain for the monitoring stack. (e.g., monitoring.example.com)"
}

variable "grafana_domain" {
  type        = string
  description = "The grafana domain for the monitoring stack. (e.g., grafana.example.com)"
}

variable "matrix_bot_domain" {
  type        = string
  description = "The domain that the matrix bot will be available under."
}

variable "cloudflare_access_ops_groups" {
  type        = list(string)
  description = "The list of cloudflare access group ids that will be allowed access to the monitoring stack."
}

variable "cloudflare_edit_token" { type = string }
variable "cloudflare_account_id" { type = string }
variable "cloudflare_zone_id" { type = string }

variable "rds_admin_password" {
  type    = string
  default = null
}
variable "rds_allocated_storage_gb" { type = number }
variable "rds_instance_class" { type = string }
variable "rds_admin_user" {
  type    = string
  default = "root"
}
variable "rds_admin_db_name" {
  type    = string
  default = "root"
}

variable "main_ec2_disk_allocation_gb" {
  type = number
}

variable "main_ebs_volume_disk_allocation_gb" {
  type = number
}

variable "main_instance_type" {
  default = null
  type    = string
}

variable "log_group_retention_in_days" {
  default     = 14
  type        = number
  description = <<EOT
The number in days that cloudwatch logs will be retained.
EOT
}

variable "tailscale_client_id" {
  type        = string
  description = <<EOT
The Tailscale client ID, used to create a pre-auth key for the created instances.
EOT
}
variable "tailscale_client_secret" {
  type        = string
  description = <<EOT
The Tailscale client secret, used to create a pre-auth key for the created instances.
EOT
}

variable "tailscale_tailnet" {
  type        = string
  description = <<EOT
The Tailscale tailnet to create the key in.
EOT
}

variable "tailscale_main_tags" {
  type        = list(string)
  description = <<EOT
The tailscale acl tags (must start with 'tag:') to apply to the created instance.
EOT
}

variable "pagerduty_escalation_policy_name" {
  description = "The name of the pager duty escalation policy to use for this service"
  type        = string
}

variable "pagerduty_service_name" {
  description = "The name of the service. Recomended is the domain name of the service. e.g., neo.keanu.im"
  type        = string
}

variable "pagerduty_suppress_alerts" {
  description = "Controls whether all alerts are suppressed or not (does not create an incident nor page anyone)"
  type        = bool
  default     = false
}

variable "grafana_service_account_token_terraform_provider" {
  type        = string
  default     = ""
  description = <<EOT

The API token for grafana for the service account grafana-terraform-provider.
This must be supplied AFTER the grafana instance is up and running, and you need
to make it using clickops in the grafana admin.

Why is it here? Because we pop it into AWS SSM param store so other modules can
fetch it using the gp_operations account and not keep it in their own secrets file.

EOT
}
