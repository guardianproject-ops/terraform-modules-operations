locals {
  enabled                     = module.this.enabled
  deletion_protection_enabled = false
  availability_zones          = sort(slice(data.aws_availability_zones.this.names, 0, 2))
  default_az                  = local.availability_zones[0]
  partition                   = data.aws_partition.current.partition
  vpc_id                      = module.vpc[0].vpc_id
  vpc_cidr_block              = module.vpc[0].vpc_cidr_block
  rds_subnet_ids              = module.rds_subnets[0].private_subnet_ids
  ec2_private_subnet_ids      = module.ec2_subnets[0].private_subnet_ids
  ec2_private_subnet_main_id  = module.ec2_subnets[0].az_private_subnets_map[local.default_az][0]
  ec2_public_subnet_ids       = module.ec2_subnets[0].public_subnet_ids
  ec2_public_subnet_main_id   = module.ec2_subnets[0].az_public_subnets_map[local.default_az][0]
  private_subnet_cidrs        = module.ec2_subnets[0].private_subnet_cidrs
  public_subnet_cidrs         = module.ec2_subnets[0].public_subnet_cidrs
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

provider "tailscale" {
  oauth_client_id     = var.tailscale_client_id
  oauth_client_secret = var.tailscale_client_secret
  tailnet             = var.tailscale_tailnet
}

data "aws_caller_identity" "this" {}

data "aws_partition" "current" {}

data "aws_availability_zones" "this" {
  state = "available"
}

data "aws_ssm_parameter" "pagerduty_token" {
  name = "/shared-secrets/pagerduty_token"
}

provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}

resource "aws_ssm_parameter" "cloudflare_access_service_token_grafana_client_secret" {
  name        = "/shared-secrets/cloudflare_access_service_token_grafana_client_secret"
  type        = "SecureString"
  value       = cloudflare_access_service_token.grafana.client_secret
  description = "The cloudflare access service token client secret to use to access grafana through cloudflare tunnel"
  tags        = module.this.tags
}

resource "aws_ssm_parameter" "cloudflare_access_service_token_grafana_client_id" {
  name        = "/shared-secrets/cloudflare_access_service_token_grafana_client_id"
  type        = "SecureString"
  value       = cloudflare_access_service_token.grafana.client_id
  description = "The cloudflare access service token client id to use to access grafana through cloudflare tunnel"
  tags        = module.this.tags
}

resource "aws_ssm_parameter" "grafana_service_account_token_terraform_provider" {
  name        = "/shared-secrets/grafana_service_account_token_terraform_provider"
  type        = "SecureString"
  value       = var.grafana_service_account_token_terraform_provider
  description = "The API token for grafana for the service account grafana-terraform-provider"
  tags        = module.this.tags
}

resource "aws_ssm_parameter" "grafana_url" {
  name        = "/shared-secrets/grafana_url"
  type        = "String"
  value       = "https://${var.grafana_domain}/"
  description = "The root URL of a Grafana server used by other modules to interact with our grafana."
  tags        = module.this.tags
}
