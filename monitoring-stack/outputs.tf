output "vpc_id" {
  value = local.vpc_id
}
output "vpc_cidr_block" {
  value = local.vpc_cidr_block
}

output "rds_subnet_ids" {
  value = module.rds_subnets[0].private_subnet_ids
}

output "public_subnet_cidrs" {
  description = "IPv4 CIDRs assigned to the created public subnets"
  value       = module.ec2_subnets[0].public_subnet_cidrs
}

output "private_subnet_cidrs" {
  description = "IPv4 CIDRs assigned to the created private subnets"
  value       = module.ec2_subnets[0].private_subnet_cidrs
}

output "public_route_table_ids" {
  description = "IDs of the created public route tables"
  value       = module.ec2_subnets[0].public_route_table_ids
}

output "private_route_table_ids" {
  description = "IDs of the created private route tables"
  value       = module.ec2_subnets[0].private_route_table_ids
}
output "az_private_subnets_map" {
  description = "Map of AZ names to list of private subnet IDs in the AZs"
  value       = module.ec2_subnets[0].az_private_subnets_map
}

output "az_public_subnets_map" {
  description = "Map of AZ names to list of public subnet IDs in the AZs"
  value       = module.ec2_subnets[0].az_public_subnets_map
}

output "named_private_subnets_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of private subnet IDs"
  value       = module.ec2_subnets[0].named_private_subnets_map
}

output "named_public_subnets_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of public subnet IDs"
  value       = module.ec2_subnets[0].named_public_subnets_map
}

output "named_private_route_table_ids_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of private route table IDs"
  value       = module.ec2_subnets[0].named_private_route_table_ids_map
}

output "named_public_route_table_ids_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of public route table IDs"
  value       = module.ec2_subnets[0].named_public_route_table_ids_map
}

output "named_private_subnets_stats_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of objects with each object having three items: AZ, private subnet ID, private route table ID"
  value       = module.ec2_subnets[0].named_private_subnets_stats_map
}

output "named_public_subnets_stats_map" {
  description = "Map of subnet names (specified in `subnets_per_az_names` variable) to lists of objects with each object having three items: AZ, public subnet ID, public route table ID"
  value       = module.ec2_subnets[0].named_public_subnets_stats_map
}

output "main_instance_id" {
  value = module.main_instance[0].instance_id
}

output "log_group_main_arn" {
  value = aws_cloudwatch_log_group.main[0].arn
}

output "log_group_main_name" {
  value = aws_cloudwatch_log_group.main[0].name
}

output "tunnel_secret" {
  value = local.tunnel_secret
}

output "tunnel_token" {
  value = cloudflare_tunnel.tunnel.tunnel_token
}

output "tunnel_id" {
  value = cloudflare_tunnel.tunnel.id
}

output "tunnel_cname" {
  value = cloudflare_tunnel.tunnel.cname
}

output "cloudflare_access_application_audience" {
  value = cloudflare_access_application.default.aud
}

output "cloudflare_access_application_id" {
  value = cloudflare_access_application.default.id
}

output "cloudflare_access_service_token_grafana_client_secret" {
  value = cloudflare_access_service_token.grafana.client_secret
}

output "cloudflare_access_service_token_grafana_client_id" {
  value = cloudflare_access_service_token.grafana.client_id
}

output "main_instance_tailnet_key" {
  value = tailscale_tailnet_key.main.key
}

output "cloudwatch_cross_account_roles" {
  value = local.cloudwatch_cross_account_roles_data
}

output "cloudwatch_cross_account_roles_map" {
  value = local.cloudwatch_cross_account_roles_map
}

output "db_instance_address" {
  value = join("", module.db.*.this.db_instance_address)
}

output "db_instance_arn" {
  value = join("", module.db.*.this.db_instance_arn)
}

output "db_instance_master_user_secret_arn" {
  value = join("", module.db.*.this.db_instance_master_user_secret_arn)
}

output "db_instance_master_user_secret_name" {
  value = join("", module.db.*.db_instance_master_user_secret_name)
}

output "rds_admin_username" {
  value = var.rds_admin_user
}

output "sns_topics_alarms" {
  value = local.sns_topics_alarms
}
output "sns_topic_arns_alarms" {
  value = local.sns_topic_arns_alarms
}
