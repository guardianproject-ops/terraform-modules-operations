resource "random_password" "tunnel_secret" {
  length = 32
}

locals {
  tunnel_secret = base64encode(random_password.tunnel_secret.result)
}

resource "cloudflare_tunnel" "tunnel" {
  account_id = var.cloudflare_account_id
  name       = module.this.id
  secret     = local.tunnel_secret
}

resource "cloudflare_record" "monitoring" {
  zone_id = var.cloudflare_zone_id
  name    = var.monitoring_domain
  content = cloudflare_tunnel.tunnel.cname
  proxied = true
  type    = "CNAME"
  comment = "Created by Terraform as a part of ${module.this.id}"
}

resource "cloudflare_record" "grafana" {
  zone_id = var.cloudflare_zone_id
  name    = var.grafana_domain
  content = cloudflare_tunnel.tunnel.cname
  proxied = true
  type    = "CNAME"
  comment = "Created by Terraform as a part of ${module.this.id}"
}

resource "cloudflare_record" "matrix_bot" {
  zone_id = var.cloudflare_zone_id
  name    = var.matrix_bot_domain
  content = cloudflare_tunnel.tunnel.cname
  proxied = true
  type    = "CNAME"
  comment = "Created by Terraform as a part of ${module.this.id}"
}

resource "cloudflare_access_service_token" "grafana" {
  account_id           = var.cloudflare_account_id
  name                 = "${module.this.id}-grafana-terraform-provider"
  min_days_for_renewal = 0
  lifecycle {
    create_before_destroy = true
  }
}

resource "cloudflare_access_application" "default" {
  zone_id                   = var.cloudflare_zone_id
  name                      = var.monitoring_domain
  domain                    = var.monitoring_domain
  self_hosted_domains       = [var.monitoring_domain, var.grafana_domain]
  session_duration          = "12h"
  auto_redirect_to_identity = true
  service_auth_401_redirect = true
}

resource "cloudflare_access_policy" "allow" {
  application_id = cloudflare_access_application.default.id
  zone_id        = var.cloudflare_zone_id
  name           = "Admins"
  precedence     = "1"
  decision       = "allow"

  include {
    group = var.cloudflare_access_ops_groups
  }
}

resource "cloudflare_access_policy" "grafana" {
  application_id = cloudflare_access_application.default.id
  zone_id        = var.cloudflare_zone_id
  name           = "Grafana Terraform Service Token"
  precedence     = "2"
  decision       = "non_identity"

  include {
    service_token = [cloudflare_access_service_token.grafana.id]
  }
}

resource "cloudflare_access_policy" "deny" {
  application_id = cloudflare_access_application.default.id
  zone_id        = var.cloudflare_zone_id
  name           = "Public"
  precedence     = "3"
  decision       = "deny"

  include {
    everyone = true
  }
}


resource "cloudflare_access_application" "matrix_bot" {
  zone_id                   = var.cloudflare_zone_id
  name                      = var.matrix_bot_domain
  domain                    = var.matrix_bot_domain
  session_duration          = "12h"
  auto_redirect_to_identity = true
}

resource "cloudflare_access_policy" "matrix_bot_allow_all" {
  application_id = cloudflare_access_application.matrix_bot.id
  zone_id        = var.cloudflare_zone_id
  name           = "Matrix Bot Webhooks"
  precedence     = "1"
  decision       = "bypass"
  include {
    everyone = true
  }
}
