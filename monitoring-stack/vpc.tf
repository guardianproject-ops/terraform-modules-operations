module "vpc" {
  source                           = "cloudposse/vpc/aws"
  version                          = "2.1.0"
  count                            = local.enabled ? 1 : 0
  ipv4_primary_cidr_block          = var.vpc_cidr
  assign_generated_ipv6_cidr_block = false
  context                          = module.this.context
  attributes                       = ["vpc"]
}

module "ec2_subnets" {
  source                          = "cloudposse/dynamic-subnets/aws"
  version                         = "2.4.1"
  count                           = local.enabled ? 1 : 0
  max_subnet_count                = 2
  availability_zones              = local.availability_zones
  vpc_id                          = module.vpc[0].vpc_id
  igw_id                          = [module.vpc[0].igw_id]
  ipv4_cidr_block                 = [var.ec2_subnets_cidr]
  ipv6_enabled                    = false
  ipv4_enabled                    = true
  public_subnets_additional_tags  = { "Visibility" : "Public" }
  private_subnets_additional_tags = { "Visibility" : "Private" }
  metadata_http_endpoint_enabled  = true
  metadata_http_tokens_required   = true
  public_subnets_enabled          = true
  context                         = module.this.context
  attributes                      = ["vpc", "subnet", "ec2"]
}

module "rds_subnets" {
  source                          = "cloudposse/dynamic-subnets/aws"
  version                         = "2.4.1"
  count                           = local.enabled ? 1 : 0
  max_subnet_count                = 2
  availability_zones              = local.availability_zones
  vpc_id                          = module.vpc[0].vpc_id
  igw_id                          = [module.vpc[0].igw_id]
  ipv4_cidr_block                 = [var.rds_subnets_cidr]
  ipv6_enabled                    = false
  public_subnets_enabled          = false
  nat_gateway_enabled             = false
  nat_instance_enabled            = false
  context                         = module.this.context
  private_subnets_additional_tags = { "Visibility" : "Private" }
  attributes                      = ["vpc", "subnet", "rds", "private"]
}
