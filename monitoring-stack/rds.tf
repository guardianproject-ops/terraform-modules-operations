module "label_db" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["db"]
  context    = module.this.context
  enabled    = true
}

resource "random_password" "rds_password" {
  length  = 32
  special = false
}

module "db" {
  source  = "./rds"
  count   = module.label_db.enabled ? 1 : 0
  context = module.label_db.context

  allocated_storage           = var.rds_allocated_storage_gb
  allow_access_cidr_blocks    = local.private_subnet_cidrs
  allow_major_version_upgrade = false
  apply_immediately           = true
  backup_retention_period     = 30
  backup_window               = "03:00-06:00"
  database_name               = var.rds_admin_db_name
  database_password           = coalesce(var.rds_admin_password, random_password.rds_password.result)
  database_username           = var.rds_admin_user
  deletion_protection_enabled = true
  engine_version              = "15"
  family                      = "postgres15"
  instance_class              = var.rds_instance_class
  kms_key_id                  = var.kms_key_arn
  major_engine_version        = "15.4"
  skip_final_snapshot         = false
  sns_topic_arns_alarms       = local.sns_topic_arns_alarms
  subnet_ids                  = local.rds_subnet_ids
  vpc_id                      = local.vpc_id
}
