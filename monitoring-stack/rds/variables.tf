variable "allow_access_cidr_blocks" {
  description = "List of CIDR strings to allow access on port 5432 to the database"
  type        = list(string)
}

variable "vpc_id" {
  type = string
}

variable "database_name" {
  type = string
}

variable "database_username" {
  type = string
}

variable "database_password" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "allocated_storage" {
  type = number
}

variable "family" {
  type = string
}

variable "major_engine_version" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "deletion_protection_enabled" {
  type = bool
}

variable "skip_final_snapshot" {
  type = bool
}

variable "allow_major_version_upgrade" {
  type = bool
}

variable "apply_immediately" {
  type = bool
}

variable "backup_retention_period" {
  type = number
}

variable "maintenance_window" {
  default = "Mon:00:00-Mon:03:00"
  type    = string
}

variable "backup_window" {
  default = "03:00-06:00"
  type    = string
}

variable "monitoring_interval" {
  default = "60"
  type    = string
}

variable "region_settings_params_enabled" {
  type        = bool
  default     = true
  description = "When enabled the connection information (except password) is stored in SSM Param Store region settings for this deployment"
}

variable "kms_key_id" {
  type        = string
  description = "The KMS key to use to encrypt the database, if not provided, one will be generated."
}

variable "sns_topic_arns_alarms" {
  type = list(string)
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05"
  type        = string
  default     = null
}
