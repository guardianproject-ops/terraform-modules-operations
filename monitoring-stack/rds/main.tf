resource "aws_security_group" "rds" {
  vpc_id = var.vpc_id
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = var.allow_access_cidr_blocks
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.this.tags
}

module "db" {
  source                      = "terraform-aws-modules/rds/aws"
  version                     = "6.2.0"
  allocated_storage           = var.allocated_storage
  allow_major_version_upgrade = var.allow_major_version_upgrade
  apply_immediately           = var.apply_immediately
  backup_retention_period     = var.backup_retention_period
  backup_window               = var.backup_window
  create_db_option_group      = false
  create_db_subnet_group      = true
  create_monitoring_role      = true
  db_name                     = var.database_name
  deletion_protection         = var.deletion_protection_enabled
  engine                      = "postgres"
  engine_version              = var.engine_version
  family                      = var.family
  identifier                  = module.this.id
  instance_class              = var.instance_class
  kms_key_id                  = var.kms_key_id
  maintenance_window          = var.maintenance_window
  major_engine_version        = var.major_engine_version
  manage_master_user_password = true
  monitoring_interval         = var.monitoring_interval
  monitoring_role_name        = "AllowRDSMonitoringFor-${module.this.id}"
  multi_az                    = false
  password                    = var.database_password
  port                        = "5432"
  skip_final_snapshot         = var.skip_final_snapshot
  snapshot_identifier         = var.snapshot_identifier
  storage_encrypted           = true
  subnet_ids                  = var.subnet_ids
  username                    = var.database_username
  vpc_security_group_ids      = [aws_security_group.rds.id]
  tags                        = module.this.tags
}


module "rds_alarms" {
  source  = "lorenzoaiello/rds-alarms/aws"
  version = "2.2.0"

  db_instance_id    = module.db.db_instance_resource_id
  db_instance_class = var.instance_class
  prefix            = "${module.this.id}-"

  actions_alarm = var.sns_topic_arns_alarms
  actions_ok    = var.sns_topic_arns_alarms
  engine        = "postgres"
}

data "aws_secretsmanager_secret" "db" {
  arn = module.db.db_instance_master_user_secret_arn
}

output "this" {
  value = module.db
}

output "db_instance_master_user_secret_name" {
  value = data.aws_secretsmanager_secret.db.name
}
