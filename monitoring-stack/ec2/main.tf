data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

locals {
  ec2_instance_type             = coalesce(var.ec2_instance_type, (module.this.stage == "prod") ? "t3.large" : "t3.medium")
  ec2_disk_allocation_gb        = tostring(coalesce(var.ebs_volume_disk_allocation_gb, 20))
  ebs_volume_disk_allocation_gb = tostring(coalesce(var.ec2_disk_allocation_gb, (module.this.stage == "prod") ? 100 : 40))
  region                        = data.aws_region.current.name
  account_id                    = data.aws_caller_identity.current.account_id
}

data "aws_subnet" "selected" {
  id = var.subnet_id
}

resource "tls_private_key" "default" {
  count = module.this.enabled ? 1 : 0

  algorithm = "RSA"
}

resource "aws_key_pair" "default" {
  count = module.this.enabled ? 1 : 0

  key_name   = module.this.id
  public_key = tls_private_key.default[0].public_key_openssh
}

data "aws_ami" "default" {
  most_recent = true
  owners      = ["136693071363"]

  filter {
    name   = "name"
    values = ["debian-12-amd64-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

module "instance_role_profile" {
  source  = "sr2c/ec2-conf-log/aws"
  version = "0.0.4"

  count = module.this.enabled ? 1 : 0

  disable_logs_bucket = true

  context = module.this.context
}

data "aws_iam_policy_document" "external_bucket_policy" {
  statement {
    sid = "AllowListBucket"
    actions = [
      "s3:List*",
      "s3:GetBucketLocation"
    ]
    resources = [
      "arn:aws:s3:::${var.bucket_name}"
    ]
  }
  statement {
    sid = "RWInPrefix"
    actions = [
      "s3:List*",
      "s3:Put*",
      "s3:Get*",
      "s3:Delete*"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}

resource "aws_iam_policy" "external_bucket_policy" {
  count       = var.attach_bucket ? 1 : 0
  name        = "${module.this.id}-external-bucket"
  description = "Policy that allows the holder to access the bucket ${var.bucket_name}"
  policy      = data.aws_iam_policy_document.external_bucket_policy.json
  tags        = module.this.tags
}

resource "aws_iam_role_policy_attachment" "external_bucket" {
  count      = var.attach_bucket ? 1 : 0
  role       = module.instance_role_profile[0].iam_role_name
  policy_arn = aws_iam_policy.external_bucket_policy[0].arn
}
data "aws_iam_policy_document" "send_logs_to_cloudwatch" {
  statement {
    sid = "AllowCloudwatchLogsList"
    actions = [
      "logs:DescribeLogGroups",
    ]

    resources = [
      "arn:aws:logs:${local.region}:${local.account_id}:log-group*"
    ]
  }
  statement {
    sid = "AllowCloudwatchLogsAccess"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "arn:aws:logs:${local.region}:${local.account_id}:log-group:${var.log_groups_root}*"
    ]
  }
}

resource "aws_iam_policy" "send_logs_to_cloudwatch" {
  name        = "cloudwatch-logs-${module.this.id}"
  description = "Policy that allows instance to send logs to cloudwatch"
  policy      = data.aws_iam_policy_document.send_logs_to_cloudwatch.json
  tags        = module.this.tags
}

resource "aws_iam_role_policy_attachment" "send_logs_to_cloudwatch" {
  role       = module.instance_role_profile[0].iam_role_name
  policy_arn = aws_iam_policy.send_logs_to_cloudwatch.arn
}

data "cloudinit_config" "this" {
  gzip = true

  part {
    content_type = "text/x-shellscript"
    content      = <<EOT
#!/bin/sh
echo "dash dash/sh boolean false" | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
DEBIAN_FRONTEND=noninteractive apt install -y wget curl
cd /opt
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
dpkg -i amazon-ssm-agent.deb
EOT
  }
}

resource "aws_instance" "default" {
  count = module.this.enabled ? 1 : 0

  ami                     = data.aws_ami.default.id
  instance_type           = local.ec2_instance_type
  subnet_id               = var.subnet_id
  key_name                = aws_key_pair.default[0].key_name
  monitoring              = true
  availability_zone       = data.aws_subnet.selected.availability_zone
  iam_instance_profile    = module.instance_role_profile[0].instance_profile_name
  disable_api_termination = var.deletion_protection_enabled
  vpc_security_group_ids  = var.security_group_ids
  user_data_base64        = data.cloudinit_config.this.rendered

  root_block_device {
    volume_type = "gp2"
    volume_size = local.ec2_disk_allocation_gb
    encrypted   = true
    kms_key_id  = var.kms_key_id
  }

  metadata_options {
    http_tokens            = "required"
    http_endpoint          = "enabled"
    instance_metadata_tags = "enabled"
    # add an extra hop to allow containers to access the metadata instance
    http_put_response_hop_limit = 2
  }

  # this prevents changes to the ami from recreating the whole instance
  lifecycle {
    ignore_changes = [ami]
  }

  tags = module.this.tags
}

resource "aws_ebs_volume" "data" {
  count = module.this.enabled ? 1 : 0

  availability_zone = data.aws_subnet.selected.availability_zone
  size              = var.ebs_volume_disk_allocation_gb
  encrypted         = true
  kms_key_id        = var.kms_key_id
  tags              = module.this.tags
  type              = "gp3"
}

resource "aws_volume_attachment" "default" {
  count = module.this.enabled ? 1 : 0

  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.data[0].id
  instance_id = aws_instance.default[0].id
}
