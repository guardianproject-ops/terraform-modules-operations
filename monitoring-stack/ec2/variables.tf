variable "ec2_disk_allocation_gb" {
  default     = null
  type        = number
  description = <<EOT
    The amount of storage to allocate for EC2 instance root disk. If left
    unset, 20 GB will be allocated.
  EOT
}

variable "ebs_volume_disk_allocation_gb" {
  default     = null
  type        = number
  description = <<EOT
    The amount of storage to allocate for the EBS volume mounted at /var/lib/republisher. If left unset, the amount allocated will depend on the stage
    of the deployment. If the stage variable is set to "prod", 100 GB will be allocated, otherwise only 40 GB will be
    allocated.
  EOT
}

variable "ec2_instance_type" {
  default     = null
  type        = string
  description = <<EOT
    The instance class for the EC2 instance. If left unset, the instance class will depend on the stage
    of the deployment. If the stage variable is set to "prod", t3.large will be use, otherwise only t3.medium.
  EOT
}

variable "bucket_name" {
  default     = ""
  type        = string
  description = <<EOT
The synapse media bucket
EOT
}

variable "kms_key_id" {
  type = string
}


variable "attach_bucket" {
  default     = false
  type        = bool
  description = <<EOT
Whether or not to attach a RW bucket to the instance. Bucket is specified with `bucket_name`
EOT
}

variable "security_group_ids" {
  default     = null
  type        = list(string)
  description = <<EOT
List of security group ids that will be attached to the instance.
EOT
}

variable "subnet_id" {
  default     = null
  type        = string
  description = <<EOT
The subnet id to place the instance in.
EOT
}

variable "deletion_protection_enabled" {
  default = true
  type    = bool
}

variable "log_groups_root" {
  type        = string
  description = <<EOT
The root prefix of log groups this instance is allowed RW access to.
EOT
}
