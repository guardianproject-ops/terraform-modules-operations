resource "tailscale_tailnet_key" "main" {
  reusable      = false
  ephemeral     = false
  preauthorized = true
  expiry        = 3600
  description   = module.this.id
  tags          = var.tailscale_main_tags
}
