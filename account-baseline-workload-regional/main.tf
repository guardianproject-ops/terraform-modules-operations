terraform {
  required_version = ">= 1.5.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.78.0"
    }
  }
}

module "baseline_region" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-account-baseline//modules/region?ref=main"
  #source  = "/home/abel/src/guardianproject-ops/terraform-aws-account-baseline//modules/region"

  context = module.this.context
}
