output "subdomain_zone" {
  value = module.subdomain_zone
}

output "alb" {
  value = module.gm.alb
}

output "db" {
  value = module.db
}

output "gm" {
  value = module.gm
}

output "vpc" {
  value = module.vpc
}

output "subnets" {
  value = module.subnets
}
