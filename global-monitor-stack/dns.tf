module "label_dns" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["dns"]
  context    = module.this.context
  enabled    = true
}

module "subdomain_zone" {
  source           = "git::https://gitlab.com/guardianproject-ops/terraform-aws-route-53-delegated-subdomain?ref=main"
  #source      = "/home/abel/src/guardianproject-ops/terraform-aws-route-53-delegated-subdomain"
  root_domain = var.root_domain
  subdomain   = var.subdomain
  ttl         = var.dns_ttl
  context     = module.label_dns.context
  attributes  = ["zone"]
  providers = {
    aws.subdomain_account   = aws
    aws.root_domain_account = aws.domain_account
  }
}

resource "aws_acm_certificate" "this" {
  domain_name       = module.subdomain_zone.domain
  validation_method = "DNS"
  tags              = module.label_dns.tags
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "this_validation" {
  for_each = {
    for dvo in aws_acm_certificate.this.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = var.dns_ttl
  type            = each.value.type
  zone_id         = module.subdomain_zone.zone_id
}

resource "aws_acm_certificate_validation" "this" {
  certificate_arn         = aws_acm_certificate.this.arn
  validation_record_fqdns = [for record in aws_route53_record.this_validation : record.fqdn]
}

resource "aws_route53_record" "this" {
  zone_id = module.subdomain_zone.zone_id
  name    = module.subdomain_zone.domain
  type    = "A"
  alias {
    name                   = module.gm.alb.alb_dns_name
    zone_id                = module.gm.alb.alb_zone_id
    evaluate_target_health = true
  }
}
