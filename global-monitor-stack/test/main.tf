# edit this file, commit it to git
# put secret vars in terraform.tfvars
terraform {
  required_version = ">= 1.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.47"
    }
  }
}

provider "aws" {
  region  = "eu-west-1"
  profile = "gp-operations"
  alias   = "gp_operations"
}

variable "tailscale_tailnet" { type = string }
variable "tailscale_client_id" { type = string }
variable "tailscale_client_secret" { type = string }
variable "tailscale_tags" { type = list(string) }
variable "cloudflare_edit_token" { type = string }
variable "cloudflare_zone_id" { type = string }
variable "grafana_url" { type = string }
variable "grafana_auth" { type = string }
variable "cloudflare_access_service_token_grafana_client_id" { type = string }
variable "cloudflare_access_service_token_grafana_client_secret" { type = string }

module "this" {
  source                                         = "../"
  namespace                                      = "cgd-bc"
  name                                           = "global-mon"
  stage                                          = "dev"
  cloudflare_edit_token                          = var.cloudflare_edit_token
  cloudflare_zone_id                             = var.cloudflare_zone_id
  task_memory                                    = 4096
  task_cpu                                       = 2048
  vpc_cidr                                       = "10.0.0.0/16"
  subnets_cidr                                   = "10.0.0.0/22"
  deletion_protection_enabled                    = false
  pagerduty_service_name                         = "TODO" # conventionall a domain to the service so its recognizable in alerts like foobarservice.gpcmdln.net
  pagerduty_escalation_policy_name               = "Aegean"
  tailscale_tailnet                              = var.tailscale_tailnet
  tailscale_client_id                            = var.tailscale_client_id
  tailscale_client_secret                        = var.tailscale_client_secret
  tailscale_tags                                 = var.tailscale_tags
  grafana_url                                    = var.grafana_url
  grafana_auth                                   = var.grafana_auth
  grafana_cloudflare_service_token_client_id     = var.cloudflare_access_service_token_grafana_client_id
  grafana_cloudflare_service_token_client_secret = var.cloudflare_access_service_token_grafana_client_secret
  rds_instance_class                             = "db.t4g.small"
  rds_postgres_major_version                     = "17"
  rds_allocated_storage_gb                       = 10
  rds_max_allocated_storage_gb                   = 20
  providers = {
    aws               = aws
    aws.gp_operations = aws.gp_operations # this is needed for monitoring stuff even if your global monitor doesnt use it
  }
}
