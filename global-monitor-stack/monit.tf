module "label_alarms" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["alarms"]
  context    = module.this.context
}

resource "aws_sns_topic" "alarms_pagerduty" {
  name  = module.label_alarms.id
  count = var.pagerduty_enabled ? 1 : 0
  tags  = module.label_alarms.tags
}

locals {
  sns_topics_alarms = {
    "pagerduty" : {
      "topic_arn" : var.pagerduty_enabled ? aws_sns_topic.alarms_pagerduty[0].arn : null
      "topic_name" : var.pagerduty_enabled ? module.label_alarms.id : null
    }
  }
  alarms_sns_topics_arns = [for k, topic in local.sns_topics_alarms : topic.topic_arn if topic.topic_arn != null]
}

module "pagerduty" {
  source                           = "../pagerduty-service"
  enabled                          = var.pagerduty_enabled
  pagerduty_service_name           = var.pagerduty_service_name
  pagerduty_escalation_policy_name = var.pagerduty_escalation_policy_name
  suppress_alerts                  = var.pagerduty_suppress_alerts
  cloudwatch_sns_topics            = local.sns_topics_alarms
  context                          = module.this.context
}

module "grafana" {
  source      = "../grafana-service"
  context     = module.this.context
  enabled     = var.grafana_enabled
  kms_key_arn = var.kms_key_arn
  providers = {
    aws.gp_operations = aws.gp_operations
  }
}
