variable "subnets_cidr" { type = string }
variable "vpc_cidr" { type = string }
variable "tailscale_tailnet" { type = string }
variable "tailscale_client_id" { type = string }
variable "tailscale_client_secret" { type = string }
variable "tailscale_tags_global_monitor" { type = list(string) }
#variable "cloudflare_edit_token" { type = string }
#variable "cloudflare_zone_id" { type = string }


variable "root_domain" {
  type = string
}

variable "subdomain" {
  type = string
}
variable "rds_instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
}

variable "rds_postgres_major_version" {
  description = "The postgres major version you want to run. The specific major version is then calculated by the module. Example: 16, 17, etc"
  type        = string
}

variable "rds_allocated_storage_gb" {
  description = "The allocated storage in gigabytes for the RDS database"
  type        = number
}

variable "rds_max_allocated_storage_gb" {
  description = "The maximum storage the RDS database can autoscale to"
  type        = number
}

variable "task_cpu_global_monitor_api" {
  type = number
}

variable "task_memory_global_monitor_api" {
  type = number
}

variable "task_cpu_global_monitor_frontend" {
  type = number
}

variable "task_memory_global_monitor_frontend" {
  type = number
}

variable "task_cpu_global_monitor_worker" {
  type = number
}

variable "task_memory_global_monitor_worker" {
  type = number
}

variable "tailscale_container_image" {
  type        = string
  default     = "ghcr.io/tailscale/tailscale:stable"
  description = <<EOT
The fully qualified container image for tailscale.
EOT
}

variable "global_monitor_frontend_container_image" {
  type        = string
  description = <<EOT
The fully qualified container image for global monitor frontend.
EOT
}

variable "global_monitor_api_container_image" {
  type        = string
  description = <<EOT
The fully qualified container image for global monitor api.
EOT
}

variable "global_monitor_worker_container_image" {
  type        = string
  description = <<EOT
The fully qualified container image for global monitor worker.
EOT
}

variable "deletion_protection_enabled" {
  type        = bool
  description = "Whether or not to enable deletion protection on things that support it"
  default     = true
}

variable "kms_key_arn" {
  type        = string
  description = "The kms key ARN used for various purposes throughout the deployment"
  default     = null
}

variable "exec_enabled" {
  type        = bool
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
  default     = false
}

variable "dns_ttl" {
  type        = number
  description = "TTL value for NS DNS records"
  default     = 3600
}
