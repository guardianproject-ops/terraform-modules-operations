variable "grafana_url" {
  type        = string
  description = "The root URL of a Grafana server in which to create alerts."
  default     = null
  validation {
    condition     = var.grafana_enabled ? var.grafana_url != null : true
    error_message = "grafana_url must be set when grafana_enabled == true"
  }
}

variable "grafana_auth" {
  type        = string
  description = "API token, basic auth in the `username:password` format"
  default     = null
  validation {
    condition     = var.grafana_enabled ? var.grafana_auth != null : true
    error_message = "grafana_auth must be set when grafana_enabled == true"
  }
}

variable "grafana_cloudflare_service_token_client_secret" {
  type        = string
  description = "The cloudflare access service token client secret to use to access grafana"
  default     = null
  validation {
    condition     = var.grafana_enabled ? var.grafana_cloudflare_service_token_client_secret != null : true
    error_message = "grafana_cloudflare_service_token_client_secret must be set when grafana_enabled == true"
  }
}

variable "grafana_cloudflare_service_token_client_id" {
  type        = string
  default     = null
  description = "The cloudflare access service token client id to use to access grafana"
  validation {
    condition     = var.grafana_enabled ? var.grafana_cloudflare_service_token_client_id != null : true
    error_message = "grafana_cloudflare_service_token_client_id must be set when grafana_enabled == true"
  }
}

variable "grafana_enabled" {
  type    = bool
  default = true
}

variable "pagerduty_enabled" {
  type    = bool
  default = true
}

variable "pagerduty_escalation_policy_name" {
  description = "The name of the pager duty escalation policy to use for this service"
  type        = string
  default     = null
  validation {
    condition     = var.pagerduty_enabled ? var.pagerduty_escalation_policy_name != null : true
    error_message = "pagerduty_escalation_policy_name must be set when pagerduty_enabled == true"
  }
}

variable "pagerduty_service_name" {
  description = "The name of the service. Recomended is the domain name of the service. e.g., neo.keanu.im"
  type        = string
  default     = null
  validation {
    condition     = var.pagerduty_enabled ? var.pagerduty_service_name != null : true
    error_message = "pagerduty_service_name must be set when pagerduty_enabled == true"
  }
}

variable "pagerduty_suppress_alerts" {
  description = "Controls whether all alerts are suppressed or not (does not create an incident nor page anyone)"
  type        = bool
  default     = false
}
