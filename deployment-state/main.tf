terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.replica]
    }
  }
}

module "remote_state" {
  source       = "git::https://gitlab.com/guardianproject-ops/terraform-modules-operations//remote-state?ref=master"
  context      = module.this.context
  is_prod_like = var.is_prod_like
  providers = {
    aws         = aws
    aws.replica = aws.replica
  }
}

locals {
  backend_tf = <<EOF
  terraform {
  backend "s3" {
    bucket         = "${module.remote_state.config.state_bucket.id}"
    key            = "${module.remote_state.state_key_prefix}/terraform.tfstate"
    region         = "${var.region}"
    encrypt        = true
    kms_key_id     = "${module.remote_state.config.kms_key.arn}"
    dynamodb_table = "${module.remote_state.config.dynamodb_table.name}"
    profile        = "${var.profile}"
  }
}
EOF

}

module "kms_key" {
  source                  = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.12.1"
  context                 = module.this.context
  description             = "${var.project} key for encrypting config (sops, etc)"
  deletion_window_in_days = var.is_prod_like ? 30 : 7
  enable_key_rotation     = true
  alias                   = "alias/${module.this.id}${var.twiddle}"
  attributes              = length(var.twiddle) > 0 ? [var.twiddle] : []
}
