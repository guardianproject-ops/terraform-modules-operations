output "remote_state" {
  value = module.remote_state.config
}

output "backend_tf" {
  value = local.backend_tf
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}