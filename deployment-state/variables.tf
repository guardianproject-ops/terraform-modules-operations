variable "region" { type = string }
variable "replica_region" { type = string }
variable "profile" { type = string }
variable "project" { type = string }
variable "is_prod_like" { type = bool }
variable "twiddle" {
  type    = string
  default = ""
}
