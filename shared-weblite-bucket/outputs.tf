output "iam_user_access_key_id" {
  value     = aws_iam_access_key.deploy_user_key_v4.id
}

output "iam_user_secret_access_key" {
  value     = aws_iam_access_key.deploy_user_key_v4.secret
  sensitive = true
}

output "bucket_id" {
  value = aws_s3_bucket.shared_bucket.id
}

output "bucket_arn" {
  value = aws_s3_bucket.shared_bucket.arn
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.shared_bucket.bucket_regional_domain_name
}

output "bucket_id_dev" {
  value = aws_s3_bucket.shared_bucket_dev.id
}

output "bucket_arn_dev" {
  value = aws_s3_bucket.shared_bucket_dev.arn
}

output "bucket_regional_domain_name_dev" {
  value = aws_s3_bucket.shared_bucket_dev.bucket_regional_domain_name
}
