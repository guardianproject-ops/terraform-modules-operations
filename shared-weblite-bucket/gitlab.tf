provider "gitlab" {
   token = var.gitlab_access_token
}

resource "gitlab_project_variable" "aws_region" {
   project   = "keanuapp/keanuapp-weblite"
   key       = "AWS_DEFAULT_REGION"
   value     = "eu-west-1"
   protected = true
}

resource "gitlab_project_variable" "aws_access_key" {
   project   = "keanuapp/keanuapp-weblite"
   key       = "AWS_ACCESS_KEY_ID"
   value     = aws_iam_access_key.deploy_user_key_v4.id
   protected = true
   masked    = true
}

resource "gitlab_project_variable" "aws_access_secret" {
   project   = "keanuapp/keanuapp-weblite"
   key       = "AWS_SECRET_ACCESS_KEY"
   value     = aws_iam_access_key.deploy_user_key_v4.secret
   protected = true
   masked    = true
}
