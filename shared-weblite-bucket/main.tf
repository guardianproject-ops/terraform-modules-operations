resource "aws_s3_bucket" "shared_bucket" {
  bucket = module.this.id
  acl    = "private"
  server_side_encryption_configuration {
    rule {
      bucket_key_enabled = false
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket" "shared_bucket_dev" {
  bucket = "${module.this.id}-dev"
  acl    = "private"
  server_side_encryption_configuration {
    rule {
      bucket_key_enabled = false
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_iam_user" "deploy_user" {
  name = module.this.id
  tags = module.this.tags
}

resource "aws_iam_access_key" "deploy_user_key_v4" {
  user   = aws_iam_user.deploy_user.name
  status = "Active"
}

data "aws_iam_policy_document" "deploy_rw" {
  statement {
    sid = "AllowBucketList"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      aws_s3_bucket.shared_bucket.arn,
      aws_s3_bucket.shared_bucket_dev.arn
    ]
  }
  statement {
    sid = "AllowBucketRW"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${aws_s3_bucket.shared_bucket_dev.arn}/*",
      "${aws_s3_bucket.shared_bucket.arn}/*"
    ]
  }
}

resource "aws_iam_user_policy" "deploy_rw" {
  name   = "AllowIamUserBucketRW-${module.this.id}"
  user   = aws_iam_user.deploy_user.name
  policy = data.aws_iam_policy_document.deploy_rw.json
}
