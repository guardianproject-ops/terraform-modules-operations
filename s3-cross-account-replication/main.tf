data "aws_caller_identity" "source" {
  provider = aws.source
}

data "aws_caller_identity" "dest" {
  provider = aws.destination
}

locals {
  enabled                       = module.this.enabled
  source_bucket_arn             = "arn:aws:s3:::${var.source_bucket_name}"
  destination_bucket_arn        = "arn:aws:s3:::${var.destination_bucket_name}"
  source_bucket_object_arn      = "arn:aws:s3:::${var.source_bucket_name}/${var.replicate_prefix}*"
  destination_bucket_object_arn = "arn:aws:s3:::${var.destination_bucket_name}/${var.replicate_prefix}*"
  source_root_user_arn          = "arn:aws:iam::${data.aws_caller_identity.source.account_id}:root"
}

data "aws_iam_policy_document" "source_replication_role" {
  count = local.enabled ? 1 : 0
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "source_replication_policy" {
  count = local.enabled ? 1 : 0
  statement {
    sid = "SourceBucketPermissions"
    actions = [
      "s3:GetReplicationConfiguration",
      "s3:ListBucket",
    ]

    resources = [local.source_bucket_arn]
  }

  statement {
    sid = "SourceBucketObjectPermissions"
    actions = [
      "s3:GetObjectVersionAcl",
      "s3:GetObjectVersionForReplication",
      "s3:GetObjectVersionTagging",
    ]

    resources = [local.source_bucket_object_arn]
  }

  statement {
    sid = "DestinationBucketObjectPermissions"
    actions = [
      "s3:GetObjectVersionTagging",
      "s3:ObjectOwnerOverrideToBucketOwner",
      "s3:ReplicateDelete",
      "s3:ReplicateObject",
      "s3:ReplicateTags",
    ]

    resources = [local.destination_bucket_object_arn]
  }
  statement {
    sid = "SourceBucketKMSKey"
    actions = [
      "kms:Decrypt",
      "kms:GenerateDataKey"
    ]
    resources = [var.source_kms_key_arn]
  }
  statement {
    sid = "DestinationBucketKMSKey"
    actions = [
      "kms:Encrypt",
      "kms:GenerateDataKey"
    ]
    resources = [var.destination_kms_key_arn]
  }
}

resource "aws_iam_role" "source_replication" {
  count              = local.enabled ? 1 : 0
  provider           = aws.source
  name               = "${module.this.id}-replication-role"
  assume_role_policy = data.aws_iam_policy_document.source_replication_role[0].json
}

resource "aws_iam_policy" "source_replication" {
  count    = local.enabled ? 1 : 0
  provider = aws.source
  name     = "${module.this.id}-replication-policy"
  policy   = data.aws_iam_policy_document.source_replication_policy[0].json
}

resource "aws_iam_role_policy_attachment" "source_replication" {
  count      = local.enabled ? 1 : 0
  provider   = aws.source
  role       = aws_iam_role.source_replication[0].name
  policy_arn = aws_iam_policy.source_replication[0].arn
}

resource "aws_s3_bucket_replication_configuration" "source_replication" {
  count    = local.enabled ? 1 : 0
  provider = aws.source
  bucket   = var.source_bucket_name
  role     = aws_iam_role.source_replication[0].arn

  rule {
    id     = module.this.id
    status = "Enabled"
    filter {
      prefix = var.replicate_prefix
    }
    source_selection_criteria {
      sse_kms_encrypted_objects {
        status = "Enabled"
      }
    }
    destination {
      bucket        = local.destination_bucket_arn
      storage_class = var.destination_storage_class
      account       = data.aws_caller_identity.dest.account_id
      access_control_translation {
        owner = "Destination"
      }
      encryption_configuration {
        replica_kms_key_id = var.destination_kms_key_arn
      }
    }
    delete_marker_replication {
      status = var.delete_marker_replication_enabled ? "Enabled" : "Disabled"
    }
  }
}

data "aws_iam_policy_document" "dest_bucket" {
  statement {
    sid = "ReplicateObjectsFromSourceAccount-${data.aws_caller_identity.source.account_id}"

    actions = [
      "s3:ReplicateObject",
      "s3:ReplicateDelete",
      "s3:ObjectOwnerOverrideToBucketOwner",
    ]

    resources = [
      local.destination_bucket_object_arn
    ]

    principals {
      type = "AWS"

      identifiers = [
        local.source_root_user_arn
      ]
    }
  }
}

resource "aws_s3_bucket_policy" "dest_bucket" {
  bucket   = var.destination_bucket_name
  policy   = data.aws_iam_policy_document.dest_bucket.json
  provider = aws.destination
}
