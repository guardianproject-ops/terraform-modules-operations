variable "source_bucket_name" {
  type        = string
  description = "Name of the source S3 bucket"
}

variable "replicate_prefix" {
  type        = string
  description = "Prefix to replicate, the default is \"\"  for all objects. If specifying it must end in /"
  default     = ""
  validation {
    condition     = var.replicate_prefix != "" ? endswith(var.replicate_prefix, "/") : true
    error_message = "replicate_prefix must end with a /"
  }
}

variable "destination_bucket_name" {
  type        = string
  description = "Name of the destination S3 bucket"
}

variable "source_kms_key_arn" {
  description = "ARN of source bucket KMS key"
  type        = string
  default     = ""
}

variable "destination_kms_key_arn" {
  description = "ARN of destination bucket KMS key"
  type        = string
  default     = ""
}

variable "destination_storage_class" {
  description = "The storage class of the destination"
  type        = string
  default     = "INTELLIGENT_TIERING"
}

variable "delete_marker_replication_enabled" {
  description = "Whether to enable delete marker replication or not"
  default     = false
  type        = bool
}
