variable "is_prod_like" {
  type        = bool
  description = "if false, things will be easier to destroy"
}

variable "instance_type" {
  type        = string
  description = "the ec2 instance type"
}

variable "kms_key_arn" {
  type        = string
  description = "the kms key to use for SSM parameters, boot disk encryption and other tasks"
}

variable "ami" {
  type        = string
  description = "optional. pass an explicit ami id to use. if not provided, the module will use the default debian-base ami."
  default     = ""
}

variable "ami_filter" {
  type        = map(string)
  description = "optional. pass an ami filter to use. if not provided, the module will use the default debian-base ami."
  default     = {}
}

variable "ami_owners" {
  type    = list(string)
  default = ["self"]
}

variable "ssm_prefix" {
  description = "(Optional) The SSM prefix (e.g., /foo/bar) that the instance is allowed to read from. Added to the instance tags as SSMPrefix."
  type = string
  default = null
}

variable "ssm_prefix_policy_arn" {
  description = "(Optional) The ARN of the IAM policy that allows the instance to read a prefix from SSM param store, it is added to the instance role"
  type = string
  default = ""
}

variable "disk_allocation_gb" {
  description = "(Required) the amount in gigabytes the instance root disk will be allocated"
  type        = number
}

#variable "security_group_id_general" {
#  type = string
#}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "availability_zone_1" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "secretsmanager_ssh_key_id" {
  type = string
}

variable "node_exporter_port" {
  type        = number
  description = "The port which node exporter is exposed on"
  default     = 9100
}

variable "cloudflared_metrics_port" {
  type        = number
  description = "The port which the cloudflared exports metrics"
  default     = 9300
}

variable "ingress" {
  type = map(object({
    from_port = number,
    to_port   = number,
    protocol  = string
  }))
  description = "these ports will be added to the security group"
  default     = {}
}

variable "ebs_volume_mount_path" {
  default     = "/var/lib/matrix"
  type        = string
  description = "(Optional) Where to mount the ebs volume, if enabled"
}

variable "ebs_volume_disk_allocation_gb" {
  type        = number
  default     = 10
  description = "how large the persistent ebs volume will be"
}

variable "enable_ebs_volume" {
  type        = bool
  description = "whether to create a persistent ebs volume"
  default     = false
}

variable "extra_iam_policy_arns" {
  type        = list(string)
  description = "a list of iam policy arns that will be added to the instance's profile role"
  default     = []
}

variable "extra_security_group_ids" {
  type        = list(any)
  default     = []
  description = "IDs of extra security groups that the instance will be added to"
}

variable "aws_key_pair_name_override" {
  type        = string
  description = "Override the keypair name to be attached to this instance."
  default     = ""
}

variable "log_groups_root" {
  type = string
}

variable "secure_metadata_endpoint" {
  description = "Whether or not the metadata service should require the session token (IMDSv2). This is dangerous to apply to existing instances."
  type        = bool
  default     = false
}