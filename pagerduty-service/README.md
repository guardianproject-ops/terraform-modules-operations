# pagerduty-service

Creates a PagerDuty service in pagerduty.

This module should only be called once per deployment.

You must define the pagerduty provider in the caller of this module.

It creates:
* The PD service configured with:
  * severity based notifications (critical+error severity = high urgency, warning+info severity = low urgency)
  * event rules to look for `severity:XXX` strings inside the `summary` or `custom_details.AlarmDescription` payload
    * where `XXX` is one of `critical`, `error`, `warning`, or `info`
    * events without this tag are defaulted to `critical`

When given a list of cloudwatch SNS topics, it subscribes the services to those topics.

It's up to you to make sure your cloudwatch alarms contain the appropriate `severity:XXX` strings


## Example Usage
```hcl

provider "aws" {
  alias   = "gp-operations"
  region  = "eu-west-1"
  profile = "gp-operations"
}

data "aws_ssm_parameter" "pagerduty_token" {
  name = "/shared-secrets/pagerduty_token"
  provider = aws.gp-operations
}

provider "pagerduty" {
  token = data.aws_ssm_parameter.pagerduty_token.value
}


module "pagerduty" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-operations//pagerduty-service?ref=CHANGEME"

  pagerduty_service_name           = module.this.tags["Project"] # e.g., neo.keanu.im
  pagerduty_escalation_policy_name = "CCX"  # see gp-operations/pagerduty-global for a list of defined escalation policies (and define a new one there if needed)
  suppress_alerts                  = false # set to true to prevent causing any alerts (events will still go into matrix)
  cloudwatch_sns_topics            = ["my-cw-topic-name"] # this is is only needed if you have cloudwatch alarms
  context                          = module.this.context
}
```

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_pagerduty"></a> [pagerduty](#requirement\_pagerduty) | 2.2.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_pagerduty"></a> [pagerduty](#provider\_pagerduty) | 2.2.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_this"></a> [this](#module\_this) | cloudposse/label/null | 0.24.1 |

## Resources

| Name | Type |
|------|------|
| [aws_sns_topic_subscription.pagerduty_subscription](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription) | resource |
| [pagerduty_service.main](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service) | resource |
| [pagerduty_service_event_rule.catchall](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_event_rule) | resource |
| [pagerduty_service_event_rule.critical](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_event_rule) | resource |
| [pagerduty_service_event_rule.error](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_event_rule) | resource |
| [pagerduty_service_event_rule.info](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_event_rule) | resource |
| [pagerduty_service_event_rule.warning](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_event_rule) | resource |
| [pagerduty_service_integration.cloudwatch](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/resources/service_integration) | resource |
| [aws_sns_topic.cloudwatch_alarm_topics](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/sns_topic) | data source |
| [pagerduty_escalation_policy.escalation_policy](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/data-sources/escalation_policy) | data source |
| [pagerduty_vendor.cloudtrail](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/data-sources/vendor) | data source |
| [pagerduty_vendor.cloudwatch](https://registry.terraform.io/providers/pagerduty/pagerduty/2.2.1/docs/data-sources/vendor) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tag_map"></a> [additional\_tag\_map](#input\_additional\_tag\_map) | Additional tags for appending to tags\_as\_list\_of\_maps. Not added to `tags`. | `map(string)` | `{}` | no |
| <a name="input_attributes"></a> [attributes](#input\_attributes) | Additional attributes (e.g. `1`) | `list(string)` | `[]` | no |
| <a name="input_cloudwatch_sns_topics"></a> [cloudwatch\_sns\_topics](#input\_cloudwatch\_sns\_topics) | List of SNS topic names that pager duty should alert off of | `list(string)` | n/a | yes |
| <a name="input_context"></a> [context](#input\_context) | Single object for setting entire context at once.<br>See description of individual variables for details.<br>Leave string and numeric variables as `null` to use default value.<br>Individual variable settings (non-null) override settings in context object,<br>except for attributes, tags, and additional\_tag\_map, which are merged. | `any` | <pre>{<br>  "additional_tag_map": {},<br>  "attributes": [],<br>  "delimiter": null,<br>  "enabled": true,<br>  "environment": null,<br>  "id_length_limit": null,<br>  "label_key_case": null,<br>  "label_order": [],<br>  "label_value_case": null,<br>  "name": null,<br>  "namespace": null,<br>  "regex_replace_chars": null,<br>  "stage": null,<br>  "tags": {}<br>}</pre> | no |
| <a name="input_delimiter"></a> [delimiter](#input\_delimiter) | Delimiter to be used between `namespace`, `environment`, `stage`, `name` and `attributes`.<br>Defaults to `-` (hyphen). Set to `""` to use no delimiter at all. | `string` | `null` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Set to false to prevent the module from creating any resources | `bool` | `null` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'uw2', 'us-west-2', OR 'prod', 'staging', 'dev', 'UAT' | `string` | `null` | no |
| <a name="input_id_length_limit"></a> [id\_length\_limit](#input\_id\_length\_limit) | Limit `id` to this many characters (minimum 6).<br>Set to `0` for unlimited length.<br>Set to `null` for default, which is `0`.<br>Does not affect `id_full`. | `number` | `null` | no |
| <a name="input_label_key_case"></a> [label\_key\_case](#input\_label\_key\_case) | The letter case of label keys (`tag` names) (i.e. `name`, `namespace`, `environment`, `stage`, `attributes`) to use in `tags`.<br>Possible values: `lower`, `title`, `upper`.<br>Default value: `title`. | `string` | `null` | no |
| <a name="input_label_order"></a> [label\_order](#input\_label\_order) | The naming order of the id output and Name tag.<br>Defaults to ["namespace", "environment", "stage", "name", "attributes"].<br>You can omit any of the 5 elements, but at least one must be present. | `list(string)` | `null` | no |
| <a name="input_label_value_case"></a> [label\_value\_case](#input\_label\_value\_case) | The letter case of output label values (also used in `tags` and `id`).<br>Possible values: `lower`, `title`, `upper` and `none` (no transformation).<br>Default value: `lower`. | `string` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Solution name, e.g. 'app' or 'jenkins' | `string` | `null` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp' | `string` | `null` | no |
| <a name="input_pagerduty_escalation_policy_name"></a> [pagerduty\_escalation\_policy\_name](#input\_pagerduty\_escalation\_policy\_name) | The name of the pager duty escalation policy to use for this service | `string` | n/a | yes |
| <a name="input_pagerduty_service_name"></a> [pagerduty\_service\_name](#input\_pagerduty\_service\_name) | The name of the service. Recomended is the domain name of the service. e.g., neo.keanu.im | `string` | n/a | yes |
| <a name="input_regex_replace_chars"></a> [regex\_replace\_chars](#input\_regex\_replace\_chars) | Regex to replace chars with empty string in `namespace`, `environment`, `stage` and `name`.<br>If not set, `"/[^a-zA-Z0-9-]/"` is used to remove all characters other than hyphens, letters and digits. | `string` | `null` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, e.g. 'prod', 'staging', 'dev', OR 'source', 'build', 'test', 'deploy', 'release' | `string` | `null` | no |
| <a name="input_suppress_alerts"></a> [suppress\_alerts](#input\_suppress\_alerts) | Controls whether all alerts are supressed or not (does not create an incident nor page anyone) | `bool` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags (e.g. `map('BusinessUnit','XYZ')` | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudwatch_integration_key"></a> [cloudwatch\_integration\_key](#output\_cloudwatch\_integration\_key) | The integration key to use in cloudwatch |
| <a name="output_pagerduty_service_id"></a> [pagerduty\_service\_id](#output\_pagerduty\_service\_id) | n/a |
<!-- END_TF_DOCS -->
