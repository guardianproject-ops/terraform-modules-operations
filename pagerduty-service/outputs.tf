output "cloudwatch_integration_key" {
  value       = module.this.enabled && var.cloudwatch_sns_topics != null ? pagerduty_service_integration.cloudwatch[0].integration_key : null
  description = "The integration key to use in cloudwatch"
}

output "pagerduty_service_id" {
  value = try(pagerduty_service.main[0].id, "")
}
