data "pagerduty_vendor" "cloudwatch" {
  count = module.this.enabled ? 1 : 0
  name  = "Amazon CloudWatch"
}

#data "pagerduty_vendor" "cloudtrail" {
#  name = "AWS CloudTrail"
#}

data "pagerduty_escalation_policy" "escalation_policy" {
  count = module.this.enabled ? 1 : 0
  name  = var.pagerduty_escalation_policy_name
}

resource "pagerduty_service" "main" {
  count                = module.this.enabled ? 1 : 0
  name                 = var.pagerduty_service_name
  escalation_policy    = data.pagerduty_escalation_policy.escalation_policy[0].id
  alert_creation       = "create_alerts_and_incidents"
  auto_resolve_timeout = "null"
  incident_urgency_rule {
    type    = "constant"
    urgency = "severity_based"
  }
}

resource "pagerduty_service_integration" "cloudwatch" {
  count   = module.this.enabled && var.cloudwatch_sns_topics != null ? 1 : 0
  name    = data.pagerduty_vendor.cloudwatch[0].name
  service = pagerduty_service.main[0].id
  vendor  = data.pagerduty_vendor.cloudwatch[0].id
}

resource "aws_sns_topic_subscription" "pagerduty_subscription" {
  for_each  = module.this.enabled && var.cloudwatch_sns_topics != null ? var.cloudwatch_sns_topics : {}
  topic_arn = each.value.topic_arn
  protocol  = "https"
  endpoint  = "https://events.eu.pagerduty.com/integration/${pagerduty_service_integration.cloudwatch[0].integration_key}/enqueue"
}

resource "pagerduty_service_event_rule" "critical" {
  count    = module.this.enabled ? 1 : 0
  service  = pagerduty_service.main[0].id
  position = 0
  disabled = false

  conditions {
    operator = "or"
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:critical"
        path  = "summary"
      }
    }
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:critical"
        path  = "custom_details.AlarmDescription"
      }
    }
  }
  actions {
    severity {
      value = "critical"
    }
    suppress {
      value = var.suppress_alerts
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }
}

resource "pagerduty_service_event_rule" "error" {
  count    = module.this.enabled ? 1 : 0
  service  = pagerduty_service.main[0].id
  position = 1
  disabled = false

  conditions {
    operator = "or"
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:error"
        path  = "summary"
      }
    }
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:error"
        path  = "custom_details.AlarmDescription"
      }
    }
  }
  actions {
    severity {
      value = "error"
    }
    suppress {
      value = var.suppress_alerts
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }

  depends_on = [
    pagerduty_service_event_rule.critical
  ]
}

resource "pagerduty_service_event_rule" "warning" {
  count    = module.this.enabled ? 1 : 0
  service  = pagerduty_service.main[0].id
  position = 2
  disabled = false

  conditions {
    operator = "or"
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:warning"
        path  = "summary"
      }
    }
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:warning"
        path  = "custom_details.AlarmDescription"
      }
    }
  }
  actions {
    severity {
      value = "warning"
    }
    suppress {
      value = var.suppress_alerts
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }
  depends_on = [
    pagerduty_service_event_rule.error
  ]
}


resource "pagerduty_service_event_rule" "info" {
  count    = module.this.enabled ? 1 : 0
  service  = pagerduty_service.main[0].id
  position = 3
  disabled = false

  conditions {
    operator = "or"
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:info"
        path  = "summary"
      }
    }
    subconditions {
      operator = "contains"
      parameter {
        value = "severity:info"
        path  = "custom_details.AlarmDescription"
      }
    }
  }
  actions {
    severity {
      value = "info"
    }
    suppress {
      value = var.suppress_alerts
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }

  depends_on = [
    pagerduty_service_event_rule.warning
  ]
}


resource "pagerduty_service_event_rule" "catchall" {
  count    = module.this.enabled ? 1 : 0
  service  = pagerduty_service.main[0].id
  position = 4
  disabled = false

  conditions {
    operator = "and"
    subconditions {
      operator = "matches"
      parameter {
        value = ".*"
        path  = "summary"
      }
    }
  }
  actions {
    severity {
      value = "info"
    }
    suppress {
      value = var.suppress_alerts
    }
  }
  lifecycle {
    ignore_changes = [
      position,
    ]
  }

  depends_on = [
    pagerduty_service_event_rule.info
  ]
}
