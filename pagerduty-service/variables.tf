variable "pagerduty_escalation_policy_name" {
  description = "The name of the pager duty escalation policy to use for this service"
  type        = string
}

variable "pagerduty_service_name" {
  description = "The name of the service. Recomended is the domain name of the service. e.g., neo.keanu.im"
  type        = string
}

variable "cloudwatch_sns_topics" {
  description = "Map of SNS topics that pager duty should alert off of"
  default     = null
  type = map(object({
    topic_name = string
    topic_arn  = string
  }))
}

variable "suppress_alerts" {
  description = "Controls whether all alerts are suppressed or not (does not create an incident nor page anyone)"
  type        = bool
  default     = false
}
