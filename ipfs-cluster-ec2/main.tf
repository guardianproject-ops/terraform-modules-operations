module "label" {
  source  = "cloudposse/label/null"
  version = "0.24.1"
  context = module.this.context
  name    = "ipfs"
  tags    = merge(module.this.tags, { Application = "ipfs" })
}

module "instance" {
  source                        = "../ec2-instance"
  is_prod_like                  = var.is_prod_like
  ssm_prefix                    = var.ssm_prefix
  ssm_prefix_policy_arn         = var.ssm_prefix_policy_arn
  kms_key_arn                   = var.kms_key_arn
  secretsmanager_ssh_key_id     = var.secretsmanager_ssh_key_id
  vpc_id                        = var.vpc_id
  vpc_cidr_block                = var.vpc_cidr_block
  availability_zone_1           = var.availability_zone_1
  subnet_id                     = var.subnet_id
  disk_allocation_gb            = var.disk_allocation_gb
  instance_type                 = var.instance_type
  log_groups_root               = var.log_groups_root
  enable_ebs_volume             = true
  ebs_volume_disk_allocation_gb = var.ebs_volume_disk_allocation_gb
  ebs_volume_mount_path         = "/var/lib/ipfs"
  secure_metadata_endpoint      = var.secure_metadata_endpoint

  ingress = {
    "ipfs-api" = {
      from_port = "5001",
      to_port   = "5001",
      protocol  = "tcp"
    }
    "ipfs-pinning" = {
      from_port = "9097",
      to_port   = "9097",
      protocol  = "tcp"
    }
    "ipfs-swarm" = {
      from_port = "4001",
      to_port   = "4001",
      protocol  = "tcp"
    }
    "ipfs-gateway" = {
      from_port = "8080",
      to_port   = "8080",
      protocol  = "tcp"
    }
  }
  context = module.label.context
}

