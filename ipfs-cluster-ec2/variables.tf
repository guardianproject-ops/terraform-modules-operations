variable "domain_name" {
  type        = string
  description = "The domain name to request a certificate for"
}

variable "alb_public_subnet_ids" {
  type = list(string)
}

variable "expiration_days" {
  type        = number
  description = "The number of days after which logs expire"
  default     = 30
}

variable "log_bucket_policy" {
  type        = string
  description = "A valid bucket policy JSON document."
  default     = ""
}

variable "prefer_well_known_over_dns_srv" {
  type        = bool
  default     = false
  description = "If set to true, this module will not generate DNS SRV records that could interfere/conflict with the well-known lookup. If set to false (default), it will generate those DNS SRV records."
}
