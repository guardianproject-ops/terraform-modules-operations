module "log_bucket_label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.25.0"
  attributes = ["loadbalancer", "logs"]
  context    = module.this.context
}

data "aws_elb_service_account" "main" {}

resource "aws_s3_bucket" "log_bucket" {
  bucket        = module.log_bucket_label.id
  acl           = "log-delivery-write"
  force_destroy = !var.is_prod_like
  policy        = var.log_bucket_policy

  versioning {
    enabled = false
  }

  lifecycle_rule {
    id      = module.log_bucket_label.id
    enabled = true

    expiration {
      days = var.expiration_days
    }
  }

  # https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  # https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  # ALB cannot write logs into the bucket if we use our own KMS key, so we must use the AES256 amazon managed key
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = module.log_bucket_label.tags
}

data "aws_iam_policy_document" "ipfs_cluster_alb" {
  statement {
    sid       = "AllowIPFSClusterALBToPutLoadBalancerLogsToS3Bucket"
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.log_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_elb_service_account.main.id}:root"]
    }
  }
}

resource "aws_s3_bucket_policy" "alb_log_bucket" {
  bucket = aws_s3_bucket.log_bucket.id
  policy = data.aws_iam_policy_document.ipfs_cluster_alb.json
}

resource "aws_s3_bucket_public_access_block" "default" {
  bucket = aws_s3_bucket.log_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
