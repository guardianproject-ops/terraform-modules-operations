locals {

  target_groups_defaults = {
    cookie_duration                  = 86400
    deregistration_delay             = 300
    health_check_interval            = 10
    health_check_healthy_threshold   = 3
    health_check_path                = "/"
    health_check_port                = "traffic-port"
    health_check_timeout             = 5
    health_check_unhealthy_threshold = 3
    health_check_matcher             = "200-299,400"
    stickiness_enabled               = true
    target_type                      = "instance"
    slow_start                       = 0
  }
}

module "label_alb" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.25.0"
  attributes = ["loadbalancer"]
  context    = module.this.context
}

locals {
  instance_id        = module.instance.id
}

resource "aws_security_group" "public_load_balancer" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9097
    to_port     = 9097
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_alb.tags
}

resource "aws_lb" "application" {
  # aws lb names are limited to 32 chars
  name                             = substr(module.label.id, 0, min(length(module.label.id), 32))
  load_balancer_type               = "application"
  internal                         = false
  security_groups                  = [aws_security_group.public_load_balancer.id]
  subnets                          = var.alb_public_subnet_ids
  idle_timeout                     = "60"
  enable_cross_zone_load_balancing = false
  enable_deletion_protection       = false # var.is_prod_like
  enable_http2                     = true
  ip_address_type                  = "ipv4"

  access_logs {
    enabled = true
    bucket  = aws_s3_bucket.log_bucket.id
    prefix  = module.label_alb.id
  }

  timeouts {
    create = "30m"
    delete = "30m"
    update = "30m"
  }

  tags = module.label.tags
  depends_on = [
    aws_s3_bucket_policy.alb_log_bucket
  ]
}

resource "aws_lb" "network" {
  # aws lb names are limited to 32 chars
  name                             = substr(module.label.id, 0, min(length(module.label.id), 32))
  load_balancer_type               = "network"
  internal                         = false
  subnets                          = var.alb_public_subnet_ids
  idle_timeout                     = "60"
  enable_cross_zone_load_balancing = false
  enable_deletion_protection       = false # var.is_prod_like
  ip_address_type                  = "ipv4"

  timeouts {
    create = "30m"
    delete = "30m"
    update = "30m"
  }

  tags = module.label.tags
}

resource "aws_lb_target_group" "ipfs_swarm" {
  name                 = "ipfs-swarm"
  vpc_id               = var.vpc_id
  port                 = "4001"
  protocol             = "TCP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "TCP"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
  }

  depends_on = [aws_lb.network]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "ipfs_swarm")
    },
  )
}

resource "aws_lb_target_group" "ipfs_api" {
  name                 = "ipfs-api"
  vpc_id               = var.vpc_id
  port                 = "5001"
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "ipfs_api")
    },
  )
}
resource "aws_lb_target_group" "ipfs_pinning" {
  name                 = "ipfs-pinning"
  vpc_id               = var.vpc_id
  port                 = "9097"
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = "/pins"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "ipfs_pinning")
    },
  )
}

resource "aws_lb_target_group" "ipfs_gateway" {
  name                 = "ipfs-gateway"
  vpc_id               = var.vpc_id
  port                 = "8080"
  protocol             = "HTTP"
  deregistration_delay = local.target_groups_defaults["deregistration_delay"]
  target_type          = local.target_groups_defaults["target_type"]
  slow_start           = local.target_groups_defaults["slow_start"]

  health_check {
    protocol            = "HTTP"
    path                = "/ipfs/"
    interval            = local.target_groups_defaults["health_check_interval"]
    port                = local.target_groups_defaults["health_check_port"]
    healthy_threshold   = local.target_groups_defaults["health_check_healthy_threshold"]
    unhealthy_threshold = local.target_groups_defaults["health_check_unhealthy_threshold"]
    timeout             = local.target_groups_defaults["health_check_timeout"]
    matcher             = local.target_groups_defaults["health_check_matcher"]
  }

  depends_on = [aws_lb.application]

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, module.this.delimiter, "ipfs_gateway")
    },
  )
}

resource "aws_acm_certificate" "ipfs_certificate" {
  domain_name       = var.domain_name
  validation_method = "DNS"

  tags = module.label.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "network_listener" {
  load_balancer_arn = aws_lb.network.arn
  port              = "4001"
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.ipfs_swarm.id
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "network_listener" {
  target_group_arn = aws_lb_target_group.ipfs_swarm.arn
  target_id        = local.instance_id
}

resource "aws_lb_listener" "application_listener" {
  load_balancer_arn = aws_lb.application.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.ipfs_certificate.arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_lb_target_group.ipfs_api.id
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "ipfs_api" {
  listener_arn = aws_lb_listener.application_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ipfs_api.arn
  }

  condition {
    host_header {
      values = ["api.proofmode.gpfs.link"]
    }
  }
}

resource "aws_lb_target_group_attachment" "ipfs_api" {
  target_group_arn = aws_lb_target_group.ipfs_api.arn
  target_id        = local.instance_id
}

resource "aws_lb_listener_rule" "ipfs_pinning" {
  listener_arn = aws_lb_listener.application_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ipfs_pinning.arn
  }

  condition {
    host_header {
      values = ["pinning.proofmode.gpfs.link"]
    }
  }
}

resource "aws_lb_target_group_attachment" "ipfs_pinning" {
  target_group_arn = aws_lb_target_group.ipfs_pinning.arn
  target_id        = local.instance_id
}

resource "aws_lb_listener_rule" "ipfs_gateway" {
  listener_arn = aws_lb_listener.application_listener.arn
  priority     = 101

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ipfs_gateway.arn
  }

  condition {
    host_header {
      values = ["gateway.proofmode.gpfs.link"]
    }
  }
}

resource "aws_lb_target_group_attachment" "ipfs_gateway" {
  target_group_arn = aws_lb_target_group.ipfs_gateway.arn
  target_id        = local.instance_id
}
