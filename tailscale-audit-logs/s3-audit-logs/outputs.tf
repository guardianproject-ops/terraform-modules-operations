# these match the form at https://login.tailscale.com/admin/logs
output "role_arn" {
  value       = try(aws_iam_role.this[0].arn, "")
  description = "ARN of the IAM role"
}

output "object_key_prefix" {
  value       = var.bucket_prefix
  description = "S3 bucket name prefix for PutObject permissions"
}

output "bucket" {
  value = try(aws_s3_bucket.this[0].id, "")
}

output "region" {
  value = data.aws_region.current.name
}

output "compression" {
  value = "zstd"
}

output "upload_period" {
  value = "1"
}
