locals {
  enabled     = module.this.enabled
  bucket_name = aws_s3_bucket.this[0].id
}

data "aws_region" "current" {}

################
# S3
################

resource "aws_s3_bucket" "this" {
  count  = local.enabled ? 1 : 0
  bucket = module.this.id
  tags   = module.this.tags
}

resource "aws_s3_bucket_versioning" "this" {
  count  = local.enabled ? 1 : 0
  bucket = local.bucket_name

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  count                   = local.enabled ? 1 : 0
  bucket                  = local.bucket_name
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  count  = local.enabled ? 1 : 0
  bucket = local.bucket_name

  rule {
    bucket_key_enabled = true
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_key_arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  count  = local.enabled ? 1 : 0
  bucket = local.bucket_name
  rule {
    id     = "RetentionRule"
    status = "Enabled"
    filter {}
    transition {
      days          = 90
      storage_class = "GLACIER_IR"
    }
    expiration {
      days = 180
    }
  }
}

################
# IAM
################

data "aws_iam_policy_document" "this" {
  count = local.enabled ? 1 : 0
  statement {
    sid = "AllowBucketWrite"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "arn:aws:s3:::${local.bucket_name}/${var.bucket_prefix}*"
    ]
  }
  statement {
    sid = "UseKMSKey"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = [
      var.kms_key_arn
    ]

  }
}
resource "aws_iam_policy" "this" {
  count       = local.enabled ? 1 : 0
  name        = module.this.id
  description = "Policy allowing S3 PutObject to the bucket ${local.bucket_name}"
  policy      = data.aws_iam_policy_document.this[0].json
  tags        = module.this.tags
}

resource "aws_iam_role" "this" {
  count = local.enabled ? 1 : 0
  name  = module.this.id
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          AWS = "arn:aws:iam::${var.account_id}:root"
        }
        Action = "sts:AssumeRole"
        Condition = {
          StringEquals = {
            "sts:ExternalId" = var.external_id
          }
        }
      }
    ]
  })
  tags = module.this.tags
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
  count      = local.enabled ? 1 : 0
  role       = aws_iam_role.this[0].name
  policy_arn = aws_iam_policy.this[0].arn
}

################
# Tailscale
################


# not possible yet
