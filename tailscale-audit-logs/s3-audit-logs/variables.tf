variable "kms_key_arn" { type = string }

variable "account_id" {
  type        = string
  description = "AWS Account ID that can assume this role"
}

variable "external_id" {
  type        = string
  description = "External ID required for assuming this role"
}

variable "bucket_prefix" {
  type        = string
  description = "S3 bucket name prefix for PutObject permissions"
}
