variable "tailscale_tailnet" { type = string }

variable "tailscale_audit_account_id" {
  type        = string
  description = "The tailscale provided AWS Account ID that can assume this role"
}

variable "tailscale_audit_external_id" {
  type        = string
  description = "The tailscale provided external ID required for assuming this role"
}
