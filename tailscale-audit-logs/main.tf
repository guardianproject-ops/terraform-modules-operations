data "aws_ssm_parameter" "ssh_recordings_key_arn" {
  name = "/tailscale/ssh-recordings-key-arn"
}

data "aws_ssm_parameter" "ssh_recordings_bucket_name" {
  name = "/tailscale/ssh-recordings-bucket-name"
}

locals {
  log_archive_audit_log_kms_key_arn = data.aws_ssm_parameter.log_archive_kms_key.value
}

module "s3_audit_logs" {
  source        = "./s3-audit-logs"
  context       = module.this.context
  name          = "audit-logs"
  kms_key_arn   = data.aws_ssm_parameter.ssh_recordings_key_arn.value
  account_id    = var.tailscale_audit_account_id
  external_id   = var.tailscale_audit_external_id
  bucket_prefix = "tailscale-audit/${var.tailscale_tailnet}/"
}

data "aws_ssm_parameter" "log_archive_kms_key" {
  name     = "/log-archive/audit-logs-key-arn"
  provider = aws.log_archive
}

module "log_archive_ssh_recordings_bucket" {
  source      = "../s3-log-archive-bucket"
  context     = module.this.context
  attributes  = ["ssh-recordings", "archive"]
  kms_key_arn = local.log_archive_audit_log_kms_key_arn
  providers = {
    aws = aws.log_archive
  }
}

module "ssh_recordings_logs_replication" {
  source                            = "../s3-cross-account-replication"
  context                           = module.this.context
  attributes                        = ["ssh-recordings", "archive"]
  source_bucket_name                = data.aws_ssm_parameter.ssh_recordings_bucket_name.value
  source_kms_key_arn                = data.aws_ssm_parameter.ssh_recordings_key_arn.value
  destination_bucket_name           = module.log_archive_ssh_recordings_bucket.bucket_name
  destination_kms_key_arn           = local.log_archive_audit_log_kms_key_arn
  delete_marker_replication_enabled = false
  providers = {
    aws.source      = aws
    aws.destination = aws.log_archive
  }
}

module "log_archive_audit_logs_bucket" {
  source      = "../s3-log-archive-bucket"
  context     = module.this.context
  attributes  = ["audit-logs", "archive"]
  kms_key_arn = local.log_archive_audit_log_kms_key_arn
  providers = {
    aws = aws.log_archive
  }
}

module "audit_logs_replication" {
  source                            = "../s3-cross-account-replication"
  context                           = module.this.context
  attributes                        = ["audit-logs", "archive"]
  source_bucket_name                = module.s3_audit_logs.bucket
  replicate_prefix                  = module.s3_audit_logs.object_key_prefix
  source_kms_key_arn                = data.aws_ssm_parameter.ssh_recordings_key_arn.value
  destination_bucket_name           = module.log_archive_audit_logs_bucket.bucket_name
  destination_kms_key_arn           = local.log_archive_audit_log_kms_key_arn
  delete_marker_replication_enabled = false
  providers = {
    aws.source      = aws
    aws.destination = aws.log_archive
  }
}

output "s3_audit_logs" {
  value = module.s3_audit_logs
}
