locals {
  operations_account_id    = data.aws_caller_identity.operations.account_id
  grafana_url              = data.aws_ssm_parameter.grafana_url.value
  grafana_auth_token       = data.aws_ssm_parameter.grafana_service_account_token_terraform_provider.value
  cloudflare_client_id     = data.aws_ssm_parameter.cloudflare_access_service_token_grafana_client_id.value
  cloudflare_client_secret = data.aws_ssm_parameter.cloudflare_access_service_token_grafana_client_secret.value
}

provider "grafana" {
  url  = local.grafana_url
  auth = local.grafana_auth_token
  http_headers = {
    "CF-Access-Client-Id" : local.cloudflare_client_id
    "CF-Access-Client-Secret" : local.cloudflare_client_secret
  }
}

data "aws_region" "current" {}

data "aws_ssm_parameter" "cloudflare_access_service_token_grafana_client_secret" {
  name     = "/shared-secrets/cloudflare_access_service_token_grafana_client_secret"
  provider = aws.gp_operations
}
data "aws_ssm_parameter" "cloudflare_access_service_token_grafana_client_id" {
  name     = "/shared-secrets/cloudflare_access_service_token_grafana_client_id"
  provider = aws.gp_operations
}

data "aws_ssm_parameter" "grafana_service_account_token_terraform_provider" {
  name     = "/shared-secrets/grafana_service_account_token_terraform_provider"
  provider = aws.gp_operations
}
data "aws_ssm_parameter" "grafana_url" {
  name     = "/shared-secrets/grafana_url"
  provider = aws.gp_operations
}

data "aws_caller_identity" "operations" {
  provider = aws.gp_operations
}

data "aws_iam_policy_document" "grafana_cloudwatch_logs_policy" {
  count = module.this.enabled ? 1 : 0
  # ref: https://grafana.com/docs/grafana/latest/datasources/aws-cloudwatch/
  statement {
    sid    = "AllowReadingMetricsFromCloudWatch"
    effect = "Allow"
    actions = [
      "cloudwatch:DescribeAlarmsForMetric",
      "cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms",
      "cloudwatch:ListMetrics",
      "cloudwatch:GetMetricData",
      "cloudwatch:GetInsightRuleReport",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowReadingLogsFromCloudWatch"
    effect = "Allow"
    actions = [
      "logs:DescribeLogGroups",
      "logs:GetLogGroupFields",
      "logs:StartQuery",
      "logs:StopQuery",
      "logs:GetQueryResults",
      "logs:GetLogEvents",
    ]
    resources = ["*"]
  }

  dynamic "statement" {
    for_each = var.kms_key_arn != null ? [1] : []
    content {
      sid = "UseKMSKey"
      actions = [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ]
      resources = [
        var.kms_key_arn
      ]

    }
  }

  statement {
    sid    = "AllowReadingTagsInstancesRegionsFromEC2"
    effect = "Allow"
    actions = [
      "ec2:DescribeTags",
      "ec2:DescribeInstances",
      "ec2:DescribeRegions",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowReadingResourcesForTags"
    effect = "Allow"
    actions = [
      "tag:GetResources",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "cross_account_cloudwatch_logs_policy" {
  count       = module.this.enabled ? 1 : 0
  name        = "${module.this.id}-CrossAccountCloudWatchLogsPolicy"
  description = "Policy to allow access to CloudWatch logs from the operations account"
  policy      = data.aws_iam_policy_document.grafana_cloudwatch_logs_policy[0].json
}

data "aws_iam_policy_document" "cross_account_assume_role_policy" {
  count = module.this.enabled ? 1 : 0
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.operations_account_id}:root"]
    }
    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"
      values   = [random_password.cloudwatch_assume_role_external_id[0].result]
    }
  }
}

resource "random_password" "cloudwatch_assume_role_external_id" {
  count   = module.this.enabled ? 1 : 0
  length  = 32
  special = false
}

resource "aws_iam_role" "cross_account_role_cloudwatch_logs" {
  count              = module.this.enabled ? 1 : 0
  name               = "${module.this.id}-CrossAccountCloudWatchLogsRole"
  assume_role_policy = data.aws_iam_policy_document.cross_account_assume_role_policy[0].json
}

resource "aws_iam_role_policy_attachment" "attach_cloudwatch_logs_policy" {
  count      = module.this.enabled ? 1 : 0
  role       = aws_iam_role.cross_account_role_cloudwatch_logs[0].name
  policy_arn = aws_iam_policy.cross_account_cloudwatch_logs_policy[0].arn
}

resource "aws_ssm_parameter" "gpops_role_arn" {
  count = module.this.enabled ? 1 : 0
  name  = "/cloudwatch-cross-account-roles/${module.this.id}/role-arn"
  type  = "String"
  value = jsonencode({
    "role_arn" : aws_iam_role.cross_account_role_cloudwatch_logs[0].arn,
    "deployment_id" : module.this.id,
    "external_id" : random_password.cloudwatch_assume_role_external_id[0].result
    "default_region" : data.aws_region.current.name
  })
  provider = aws.gp_operations
}

resource "grafana_folder" "folder" {
  count = module.this.enabled ? 1 : 0
  title = module.this.id
}

resource "grafana_data_source" "cloudwatch" {
  count = module.this.enabled ? 1 : 0
  type  = "cloudwatch"
  name  = module.this.id
  json_data_encoded = jsonencode({
    defaultRegion = data.aws_region.current.name
    authType      = "default"
    assumeRoleArn = aws_iam_role.cross_account_role_cloudwatch_logs[0].arn,
    externalId    = random_password.cloudwatch_assume_role_external_id[0].result
  })
}
