output "grafana_folder_uid" {
  value       = try(grafana_folder.folder[0].uid, "")
  description = "The uid of the grafana folder created for this service."
}

output "grafana_cloudwatch_data_source_uid" {
  value       = try(grafana_data_source.cloudwatch[0].uid, "")
  description = "The uid of the cloudwatch datasource created in grafana."
}

output "provider_headers" {
  description = "The headers to pass to the http_headers argument of the grafana provider."
  value = {
    "CF-Access-Client-Id" : local.cloudflare_client_id
    "CF-Access-Client-Secret" : local.cloudflare_client_secret
  }

}

output "url" {
  description = "The base url of the grafana instance, can be used to instantiate a grafana provider"
  value       = local.grafana_url
}

output "auth_token" {
  description = "The api auth token of the grafana instance, can be used to instantiate a grafana provider"
  value       = local.grafana_auth_token
}
