terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.replica]
    }
  }
}

variable "is_prod_like" { type = bool }

module "remote_state" {
  source  = "nozaq/remote-state-s3-backend/aws"
  version = "1.2.0"

  providers = {
    aws         = aws
    aws.replica = aws.replica
  }

  dynamodb_table_name             = module.label.id
  iam_policy_attachment_name      = module.label.id
  iam_policy_name_prefix          = module.label.id
  iam_role_name_prefix            = module.label.id
  override_s3_bucket_name         = true
  enable_replication = var.is_prod_like
  s3_bucket_name                  = module.label.id
  s3_bucket_name_replica          = "${module.label.id}-replica"
  terraform_iam_policy_create     = false
  kms_key_deletion_window_in_days = var.is_prod_like ? 30 : 7
  kms_key_enable_key_rotation = true
  kms_key_description = "KMS key used to encrypt tf state for ${module.this.id}"
  tags                            = module.label.tags
}

module "label" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  context    = module.this.context
  attributes = ["tf-state"]
}

output "config" {
  value = module.remote_state
}

output "state_key_prefix" {
  value = module.label.id
}
